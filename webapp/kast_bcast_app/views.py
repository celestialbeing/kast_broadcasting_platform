"""

This module is responsible for both the trial and live broadcast functionality of Kast.

Models used:
    KastUsers
    UserContactGroups
    KastUserCampaigns
    KastUserBroadcasts
    KastTenants
    KastSIDs
    KastMessageTemplates
    Contacts
    ContactGroups
    UserContactGroups

Classes used:
    utils

"""

import logging

import arrow
import requests
from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponse
from django.views.decorators.http import require_POST
from django.contrib.auth.decorators import login_required
from django.conf import settings

from kast_account_app.models import KastUsers
from kast_bcast_app import utils
from kast_contact_app.models import UserContactGroups
from kast_campaign_app.models import KastUserCampaigns, KastUserBroadcasts
from kast_account_app.models import KastTenants
from kast_sender_id_app.models import KastSIDs
from kast_message_template_app.models import KastMessageTemplates
from kast_contact_app.models import Contacts, ContactGroups,\
    UserContactGroups
from django.db.models import Count

from webapp.util import phonenumbers_ph as phone_ph
from webapp.util.aws import S3


@login_required
@require_POST
def get_broadcast_id(request):
    data = utils.clean_data(request.body, ['campaign_name'])
    try:
        campaign = KastUserCampaigns.objects.get(
            campaign_name = data['campaign_name'],
            user=request.user
        )
        return campaign.kastuserbroadcasts.broadcast_id
    except:
        return None


@login_required
def broadcast_trial(request):

    """
    
    Renders trial broadcast page.

    :type request: obj
    :param request: Request object

    :rtype: obj (HttpResponse)
    :return: Rendered template 
            trial dashboard page
            trial broadcast page
            account processing page
            outside schedule broadcast page

    .. note::
        When dashboard template is rendered, dashboard rendering logic is handled by the campaign app.

    """

    kast_user = KastUsers.objects.get(user=request.user.id)

    if kast_user.is_trial_consumed is True or kast_user.status == 'basic' or kast_user.tenant is not None:
        return redirect('/dashboard')

    now = arrow.utcnow()
    local_time = now.to('Asia/Manila')
    start = arrow.get(local_time).replace(hour=8).replace(minute=01)
    end = arrow.get(start).replace(hour=18).replace(minute=01)

    if not (start.timestamp <= local_time.timestamp <= end.timestamp):
        return render(request,'kast_bcast_app_templates/trial/outside-schedule-broadcast.html')
    else:
        return render(request,'kast_bcast_app_templates/trial/sms-broadcast-free-trial.html')


@login_required
def broadcast_live(request):

    """
    
    Renders live broadcast page.

    :type request: obj
    :param request: Request object

    :rtype: obj (HttpResponse)
    :return: 
        Rendered template 
            dashboard page
            dashboard page (empty)
            broadcast page

    .. note::
        When dashboard template is rendered, dashboard rendering logic is handled by the campaign app.

    """

    kast_user = KastUsers.objects.get(user=request.user.id)
    if kast_user.status == 'trial':
        return redirect('/dashboard')

    #get kast tenant id
    tenant_obj = KastTenants.objects.get(name = kast_user.tenant)
    tenant_id = tenant_obj.id

    sender_ids = KastSIDs.objects.filter(status = 1, tenant_id = tenant_id).order_by('name')
    pre_approved = KastSIDs.objects.filter(tenant_id = 0, status = 1).order_by('name')
    message_templates = KastMessageTemplates.objects.filter(tenant_id = tenant_id).order_by('name')

    #get contact groups
    try:
        contact_groups = ContactGroups.objects.filter(tenant=tenant_id).order_by('-created_at')[:5]

        for group in contact_groups:
            group.base_count = UserContactGroups.objects.filter(contact_group_id = group.id).count()

    except:
        contact_groups = None

    #get campaign from draft
    if (request.method == 'GET' and 'q' in request.GET):
        campaign_uid = request.GET.get('q')
        try:
            campaign = KastUserCampaigns.objects.get(campaign_uid = campaign_uid)
        except:
            campaign = None

        if(campaign is not None):
            #redirect to dashboard if done broadcasting from save as draft
            if(campaign.reference_code):
                return redirect('/dashboard')

            if(campaign.kastuserbroadcasts.sender_id):
                sender_id = int(campaign.kastuserbroadcasts.sender_id)
            else:
                sender_id = None

            campaign_schedule = None
            if(campaign.campaign_schedule is not None):
                campaign_schedule = arrow.get(campaign.campaign_schedule).to('Asia/Manila').timestamp
            campaign_obj = {'campaign_id': campaign.id, 'campaign_name': campaign.campaign_name, 'sender_id': sender_id, 'campaign_schedule': campaign_schedule, 'is_draft': campaign.is_draft, 'message': campaign.kastuserbroadcasts.message}
        else:
            campaign_obj = {}
    else:
        campaign_obj = {}

    return render(request,
        'kast_bcast_app_templates/live/sms-broadcast-live.html', {'sender_ids': sender_ids, 'message_templates': message_templates, 'campaign_draft': campaign_obj, 'contact_groups': contact_groups, 'pre_approved': pre_approved})


@require_POST
def get_franchise(request):

    """
    
    Get franchise details of a MIN.

    :type request: obj
    :param request: Request object

    :rtype: obj or None
    :return: JSON response object containing MIN, network and plan

    """

    data = utils.clean_data(request.body, ['phonenumber'])

    phone = phone_ph.parse(data['phonenumber'])
    if phone is None:
        return JsonResponse({})

    return JsonResponse({
        'number': phone.number,
        'network': phone.network,
        'plan': phone.plan})


@require_POST
@login_required
def save_draft(request):

    """
    
    Saves unfinished campaigin as draft.

    :type request: obj
    :param request: Request object

    :rtype: obj
    :return: JSON response 'cleaned' data

    """

    data = utils.clean_data(request.body)

    campaign_name = utils.clean_campaign_name(
        data.get('campaign_id'),
        data.get('campaign_name'),
        KastUsers.objects.get(pk=request.user.id))

    campaign = KastUserCampaigns.objects.create_broadcast(
        request.user.id,
        **{'c_name': campaign_name,
            'c_type': 'sms_broadcast',
            'c_schedule': data.get('schedule'),
            'is_draft': True,
            'sid': data.get('sender_id'),
            'message': data.get('message'),
            'status': 'Draft'})

    data['campaign_id'] = campaign.id
    return JsonResponse(data)


@login_required
def get_report(request, campaign_id):

    """

    Prepare the report file for download.

    :type request: obj
    :param request: Request object

    :type campaign_id: int 
    :param campaign_id: the campaign ID

    :rtype: obj
    :return: HTTP response object

    """

    # WANT: Exit gracefully if cannot get campaign.
    user_broadcast = KastUserBroadcasts.objects.get(
        broadcast_id=campaign_id,
        campaign__user=KastUsers.objects.get(pk=request.user.id),
        campaign__is_draft=False)
    campaign = user_broadcast.campaign

    report_urls = utils.get_report_urls(user_broadcast.broadcast_id)

    # Download report files to fs.
    members = []
    file = '%s.csv' % campaign.campaign_name

    reports = settings.BROADCASTS['report_url']
    res = requests.get(reports+"files/"+report_urls)
    urls = res.text
    for url in urls.split(","):
        filepath = utils.download_file(url, '%s' % (url.split("/")[1]))
        members.append(filepath)

    zip_file = '%s.zip' % campaign.campaign_name
    zip_filepath = utils.zip_files(zip_file, members)

    # Prep file for download.
    download = open(zip_filepath, 'rb')

    response = HttpResponse(download, content_type='application/zip')
    response['Content-Disposition'] = 'attachment; filename="%s"' % zip_file

    return response

@login_required
def view_failed(request, campaign_id):

    reports = [
        {
            "msisdn": 639173264125,
            "status": "Sent",
            "description": "chuchu"
        },
        {
            "msisdn": 639162360624,
            "status": "Pending",
            "description": "chuchu"
        },
        {
            "msisdn": 639173060606,
            "status": "Rejected",
            "description": "chuchu"
        },
        {
            "msisdn": 639177475869,
            "status": "Pending",
            "description": "chuchu"
        }
    ]

    #reports = []
    #campaign_name = "Campaign Name Here"

    #return render(request,'kast_bcast_app_templates/broadcast-report.html', {"reports": reports, "campaign_name": campaign_name})

@require_POST
@login_required
def do_trial(request):

    """

    Execute trial broadcast

    :type request: obj
    :param request: Request object

    :rtype: obj
    :return: JSON object with the following fields
        list of MINs that were broadcasted
        broadcast SMS message
        default sender ID
        broadcast reference ID

    """

    logger = logging.getLogger('django.server')
    data = utils.clean_data(request.body, ['phonenumbers', 'message'])

    kast_user = KastUsers.objects.get(user=request.user.id)
    if kast_user.is_trial_consumed is True:
        return JsonResponse({'error': 'User already consumed trial.',
            'code': '004'}, status=400)

    # Send trial broadcast only within valid window time.
    utils.clean_schedule()

    # Normalize phone number format.
    phonenumbers = utils.clean_trial_numbers(data['phonenumbers'])

    # Upload phonenumbers csv to S3.
    csv = S3().upload_csv(phonenumbers, 'broadcasts/trial/%s-%s' % (
        request.user.id, arrow.utcnow().timestamp))

    ref_id, broadcast_id = utils.do_broadcast(
        "TRIAL",
        data['message'], csv, request.user.id)

    # Mark user trial as done.
    KastUsers.objects.filter(user=request.user.id)\
        .update(is_trial_consumed=True)

    # Send acknowledgement email.
    # Email sending fails silently.
    utils.trial_send_confirm_email(
        ref_id, len(phonenumbers), data['message'], request.user.email)

    return JsonResponse({
        'phonenumbers': data['phonenumbers'],
        'message': data['message'],
        'sender_id': settings.BROADCASTS['sender_id'],
        'ref_id': ref_id})


@require_POST
@login_required
def do_live(request):

    """

    Execute live broadcast

    :type request: obj
    :param request: Request object

    :rtype: obj
    :return: JSON object with the following fields
        contact list ID
        broadcast SMS message
        approved sender ID
        broadcast reference ID

    """

    logger = logging.getLogger('django.server')
    data = utils.clean_data(request.body, [
        'campaign_name',
        'contact_list_id',
        'sender_id',
        'message',
        'schedule'])

    # Get contact list numbers in db.
    phonenumbers = UserContactGroups.objects\
        .get_contacts(data['contact_list_id'])
    base_count = len(phonenumbers)
    csv = S3().upload_csv(phonenumbers, 'broadcasts/live/%s-%s' % (
        request.user.id, arrow.utcnow().timestamp))

    message = data['message']
    sender_id = utils.clean_sender_id(data['sender_id'])
    schedule = utils.clean_schedule(data['schedule'])
    campaign_name = utils.clean_campaign_name(
        data.get('campaign_id'),
        data['campaign_name'],
        KastUsers.objects.get(pk=request.user.id))

    ref_id, broadcast_id = utils.do_broadcast(
        request.user.kastusers.tenant,
        message, csv,
        request.user.id, schedule, sender_id)

    # Save or update item to db.
    campaign = KastUserCampaigns.objects.create_broadcast(request.user.id, **{
        'c_id': data.get('campaign_id'),
        'c_name': data['campaign_name'],
        'broadcast_id': broadcast_id,
        'c_type': 'sms_broadcast',
        'c_schedule': schedule,
        'is_draft': False,
        'ref_code': ref_id,
        'report_url': None,
        'b_count': base_count,
        'sid': data['sender_id'],
        'message': message,
        'status': 'Scheduled' if schedule else 'Forwarded'})

    # Send acknowledgement email.
    # Email sending fails silently.
    params = [ref_id, base_count, message, request.user.email,
        sender_id, request.user.email, arrow.get(schedule).to('Asia/Manila').format()]
    utils.live_user_send_confirm_email(*params)
    utils.live_admin_send_confirm_email(*params)

    return JsonResponse({
        'contact_list_id': data['contact_list_id'],
        'message': message,
        'sender_id': sender_id,
        'ref_id': ref_id,
        'broadcast_id': campaign.kastuserbroadcasts.broadcast_id
    })


@require_POST
@login_required
def do_resend(request):

    """

    Re-executes live broadcast if live broadcasts failed

    :type request: obj
    :param request: Request object

    :rtype: obj
    :return: JSON object with the following fields
        contact list ID
        broadcast SMS message
        approved sender ID
        broadcast reference ID

    """

    logger = logging.getLogger('django.server')
    
    data = utils.clean_data(request.body, [
        'campaign_name',
        'contact_list',
        'sender_id',
        'message',
        'schedule',
        'campaign_id'])
    
    # Get contact list numbers in db.
    phonenumbers = map(
            str.strip,
            str(data['contact_list']).split(','))

    base_count = len(phonenumbers)
    csv = S3().upload_csv(phonenumbers, 'broadcasts/live/%s-%s' % (
        request.user.id, arrow.utcnow().timestamp))

    message = data['message']
    sender_id = utils.clean_sender_id(data['sender_id'])

    campaign_name = utils.clean_campaign_name(
        data['campaign_id'],
        data['campaign_name'],
        KastUsers.objects.get(pk=request.user.id))

    kast_campaign = KastUserCampaigns.objects.get(
        campaign_name=data['campaign_name'])

    ref_id = utils.do_resend(
        request.user.kastusers.tenant,
        message, csv,
        kast_campaign.kastuserbroadcasts.broadcast_id,
        data['schedule'], sender_id)

    # Send acknowledgement email.
    # Email sending fails silently.
    ref_code = kast_campaign.reference_code
    params = [ref_code, base_count, message,
              request.user.email, sender_id, request.user.email,
              arrow.get(data['schedule']).to('Asia/Manila').format()]

    utils.live_user_send_confirm_email(*params)
    utils.live_admin_send_confirm_email(*params)

    utils.has_record(
        kast_campaign.kastuserbroadcasts.broadcast_id,
        data['contact_list']
    )

    return JsonResponse({
        'base_count': base_count,
        'message': message,
        'sender_id': sender_id,
        'ref_id': ref_code})


# WANT: Optimize this method.
def callback(request):

    """
    
    Checks for broadcast status if either 'Forwarded' or 'Completed'

    :type request: obj
    :param request: Request object

    :rtype: obj
    :return: JSON object containing broadcast id and status

    """

    broadcast_id = request.GET.get('id')
    status = request.GET.get('status')

    # Clean params.
    if not (broadcast_id and status):
        return JsonResponse({'error': 'Missing required params.'}, status=400)
    #if status not in ('1', '2'):
    if status not in ("on going", "cancelled", "completed", "paused", "pending"):
        return JsonResponse({'error': 'Invalid status.'}, status=400)

    user_broadcast = KastUserBroadcasts.objects.filter(
        broadcast_id=broadcast_id)

    if not user_broadcast:
        return JsonResponse({'error': 'Campaign not found.'}, status=404)

    #if status == '1':
    if status == "on going":
        campaign_status = 'Forwarded'
        if utils.has_record(broadcast_id,""):
            utils.retrieve(broadcast_id, True)
    elif status == 'completed':
        campaign_status = 'Completed'
    elif status in ("cancelled","paused"):
        campaign_status = "Paused"

    # Campaign is updated to new status.
    if user_broadcast.update(status=campaign_status):
        return JsonResponse({
            'broadcast_id': broadcast_id,
            'status': campaign_status})

    return JsonResponse({})


# WANT: Optimize this method.
@login_required
def cancel_broadcast(request, campaign_id):

    """
    
    Cancels a broadcast

    :type request: obj
    :param request: Request object

    :type campaign_id: int
    :param campaign_id: the campaign ID

    :rtype: obj
    :return: JSON object containing API hit request 

    """
    
    # WANT: Exit gracefully if cannot get campaign.
    user_broadcast = KastUserBroadcasts.objects.get(
        campaign__pk=campaign_id,
        campaign__user=KastUsers.objects.get(pk=request.user.id),
        campaign__is_draft=False)

    cancelled = utils.update_broadcast(
        user_broadcast.broadcast_id, {'cancelled': True})

    if cancelled.get('status') == "cancelled":
        user_broadcast.status = 'Cancelled'
        user_broadcast.save()
        return JsonResponse(cancelled)

    return JsonResponse({})
