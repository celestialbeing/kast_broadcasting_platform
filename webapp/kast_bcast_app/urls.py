from django.conf.urls import url

from kast_bcast_app import views


# RPC style endpoints.
urlpatterns = [
    url(r'^get_franchise/$', views.get_franchise, name='get_franchise'),
    url(r'^do_trial/$', views.do_trial, name='do_trial'),
    url(r'^do_resend/$', views.do_resend, name='do_resend'),
    url(r'^do_live/$', views.do_live, name='do_live'),
    url(r'^save_draft/$', views.save_draft, name='save_draft'),
    url(r'^get_report/(?P<campaign_id>[0-9]+)/$',
        views.get_report, name='get_report'),
    url(r'^get_bcast_id/$', views.get_broadcast_id, name='get_bcast_id'),
    url(r'^report-failed/(?P<campaign_id>[0-9]+)/$',
        views.view_failed, name='view_failed'),
    url(r'^cancel/(?P<campaign_id>[0-9]+)/$',
        views.cancel_broadcast, name='cancel_broadcast'),
    url(r'^trial/$', views.broadcast_trial, name='broadcast_trial'),
    url(r'^live/$', views.broadcast_live, name='broadcast_live'),
    url(r'^callback/$', views.callback, name='callback'),
]