from django.conf.urls import url

from kast_contact_app import views

# RPC style endpoints.
urlpatterns = [
    url(r'^$', views.contacts, name='contacts'),
    url(r'^add_contacts/', views.add_contacts, name='add_contacts'),
    url(r'^proceed_upload/', views.proceed_upload, name='proceed_upload'),
    url(r'^remove_base/', views.remove_base_file, name='remove_base_file'),
    url(r'^load_more/', views.load_more_contacts, name='load_more_contacts'),
    url(r'^search/', views.search_contacts, name='search_contacts'),

]
