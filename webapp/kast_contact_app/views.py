"""

This module is responsible for rendering the webpage of base management for
    live user accounts

Models used:
    KastUserCampaigns

"""

import os
import re
import csv
import logging
from uuid import uuid4
import json
import random
import string
import subprocess

import pyexcel
from django.shortcuts import render
from django.shortcuts import redirect
from django.http import JsonResponse
from django.views.decorators.http import require_POST
from django.contrib.auth.decorators import login_required
from django.utils.datastructures import MultiValueDictKeyError
from django.db import transaction

from webapp.util import phonenumbers_ph as phone_ph
from webapp.util.aws import S3
from webapp.middleware import KastApiError
from kast_account_app.models import KastUsers
from django.contrib.auth.models import User
from kast_account_app.models import KastTenants
from kast_contact_app.models import Contacts, ContactGroups,\
    UserContactGroups


@login_required
def contacts(request):

    """

    Renders user contacts page if user is not trial account

    :type request: obj
    :param request: Request object

    :rtype: obj (HttpResponse)
    :return: Rendered template contacts with list of contact 
        group under a tenant

    """

    #get user tenant
    user = User.objects.get(email = request.user.email)

    if(user.kastusers.status == 'trial'):
        return redirect('/dashboard')

    tenant = KastUsers.objects.get(user=request.user.id).get_tenant()
    contact_groups = ContactGroups.objects.get_contact_groups(tenant.id)
    return render(request, 'kast_contact_app_templates/contacts.html',
        {'contact_groups': contact_groups})


@require_POST
@login_required
def load_more_contacts(request):
    ## gets next batch of contact groups and total base count for each
    # returns json formatted list of contact group id, name and count

    request_body    = request.body.decode('utf-8')
    body            = json.loads(request_body)

    last_id = body['last_id']
    if 'search_string' in body:
        search_string = body['search_string']
    else:
        search_string = ""
    tenant = KastUsers.objects.get(user=request.user.id).get_tenant()
    contact_groups = ContactGroups.objects.get_contact_groups(
        tenant.id, last_id, search_string)

    return JsonResponse({'contact_groups': contact_groups})


@require_POST
@login_required
def search_contacts(request):
    ## gets next batch of contact groups and total base count for each
    # returns json formatted list of contact group id, name and count

    request_body    = request.body.decode('utf-8')
    body            = json.loads(request_body)

    search_string = body['search_string']

    tenant = KastUsers.objects.get(user=request.user.id).get_tenant()
    contact_groups = ContactGroups.objects.get_contact_groups(
        tenant.id,
        search_str=search_string)

    return JsonResponse({'contact_groups': contact_groups})


@require_POST
@login_required
def add_contacts(request):

    """

    Adds contacts either individually or by batch.
    Save contacts on the database.
    Creates a file for invalid MINs.

    :type request: obj
    :param request: Request object

    :rtype: obj 
    :return: Response of valid and invalid MINs

    :raise KastApiError 002: Missing required parameters

    .. warning:: Documentation not updated. Need to consult with developer.

    """

    logger = logging.getLogger('django.server')
    debug_logger = logging.getLogger('kastlogger')
    debug_logger.debug("starting debug logger")
    try:
        contacts_name = request.POST['contacts_name']
        contacts_file = request.FILES.get('contacts_file')
        contacts_form = request.POST.get('contacts_form')
        if not(contacts_file or contacts_form):
            raise ValueError('No contacts found.')
    except (MultiValueDictKeyError, ValueError) as e:
        print(e)
        logger.exception(e)
        raise KastApiError(
            {'error': 'Missing required parameters.', 'code': '002'}, 400)

    #generate random filename
    file_name=str(request.user.id)+''.join(
        random.choice(string.ascii_uppercase + string.digits)
        for _ in range(5))

    valid_phones_file = open(file_name+".csv", "a+")
    invalid_file = open(file_name+"_invalid.csv", "a+")

    debug_logger.debug("start validating base")

    _clean_contacts(
        request.FILES, request.POST, valid_phones_file, invalid_file)
    valid_phones_file.close()
    invalid_file.close()

    valid_phones, invalid = sort_files(file_name)

    debug_logger.debug("done validating base")

    response = {'valid_count': valid_phones, 'invalid_count': invalid}



    response["valid_contacts"] = file_name

    if invalid:
        debug_logger.debug("done parsing base")

        # Upload phonenumbers csv to S3.
        filename = '%s-%s' % ('KAST_INVALID', _gen_unique_filename())
        response['invalid_contacts'] = S3().upload_from_file(
            file_name+"_invalid", 'contacts/invalid/%s' % filename)
        os.remove(file_name+"_invalid.tmp")

        response['code'] = '008'
        response['error'] = 'File has invalid phonenumbers.'

        return JsonResponse(response)

    del invalid

    # Save numbers to db.
    _save_contacts(request.user, contacts_name, file_name)
    os.remove(file_name+".tmp")

    return JsonResponse(response)


def sort_files(filename):
    try:
        #print("sorting valid")
        valid = subprocess.Popen(
            "sort -u %s.csv > %s.tmp" % (filename, filename),
            shell=True, stdout=subprocess.PIPE)
        valid.wait()
        #print("writing sorted valid")
        os.remove(filename+".csv")
    except:
        print("no valid")

    try:
        #print("sorting invalid")
        invalid = subprocess.Popen(
            "sort -u %s_invalid.csv > %s_invalid.tmp" % (filename, filename),
            shell=True, stdout=subprocess.PIPE)
        invalid.wait()
        #print("writing sorted invalid")

        os.remove(filename+"_invalid.csv")
    except:
        print("no invalid")




    return get_count(filename)


def get_count(filename):
    try:
        #print("getting count for valid")
        valid = subprocess.Popen(["wc", "-l", filename+".tmp"], stdout=subprocess.PIPE)
        valid.wait()
        valid_count = int(valid.stdout.read().split(" ")[0])
        vcount = 0 if valid_count < 0 else valid_count
    except:
        vcount = 0

    try:
        #print("getting count for valid")
        invalid = subprocess.Popen(["wc", "-l", filename+"_invalid.tmp"], stdout=subprocess.PIPE)
        invalid.wait()
        invalid_count = int(invalid.stdout.read().split(" ")[0])
        ivcount = 0 if invalid_count < 0 else invalid_count
    except:
        ivcount = 0

    return vcount, ivcount



@require_POST
@login_required
def remove_base_file(request):
    request_body    = request.body.decode('utf-8')
    body            = json.loads(request_body)

    file_name = body['valid_contacts']
    try:
        os.remove(file_name+".csv")
        response = {'message': 'file removed'}
    except:
        response = {'message': 'file not exist'}

    return JsonResponse(response)


@require_POST
def proceed_upload(request):

    request_body    = request.body.decode('utf-8')
    body            = json.loads(request_body)

    file_name = body['valid_contacts']
    valid_count = body['valid_count']
    contact_name = body['contacts_name']

    _save_contacts(request.user, contact_name, file_name)
    os.remove(file_name+".tmp")

    response = {'valid_count': valid_count, 'invalid_count': 0}

    return JsonResponse(response)



def _handle_uploads(f):

    """

    Creates a filepath for validated uploaded files

    :type f: file
    :param f: The uploaded file

    :rtype: str 
    :return: The path of the uploaded file

    :raise KastApiError 009: File is more than 3MB.

    .. note: Additional validation of file upload
        CSV content-type validation
        If directory for filepath exists

    """

    debug_logger = logging.getLogger('kastlogger')
    debug_logger.debug("checking file size if larger than 30 mb")
    # Size should be < 2.5MB.
    if f.size > 31457280: #2.5*1000000:
        raise KastApiError(
            {'error': 'File is more than 30MB.', 'code': '009'}, 400)

    # TODO: Check if csv content-type.
    # TODO: Check if directory is present.
    filepath = '/tmp/%s' % _gen_unique_filename(f)

    with open(filepath, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)

    return filepath

def _gen_unique_filename(f=None):

    """

    Generates a random UUID for a given file

    :type f: file
    :param f: The uploaded file

    :rtype: str or None
    :return: String appended with the generated UUID or nothing

    """

    unique = uuid4()
    if f is not None:
        unique = '%s-%s' % (uuid4(), f.name)
    return unique


def _is_valid_format(mobile):
    regex = r'^\+?(63|0)?\d{10}$'
    if re.match(regex,mobile):
        return "63"+mobile[-10:]
    else:
        return False

def _clean_contacts(FILES, POST, valid_phones, invalid_phones):

    """

    Processes uploaded file containing list of MINs and return unique, valid 
        MINs from the list

    :type FILES: file
    :param FILES: The uploaded file

    :type POST: obj
    :param POST: Post method objects    

    :rtype: list
    :return: List of unique and valid MINs

    """

    debug_logger = logging.getLogger('kastlogger')
    debug_logger.debug("cleaning base")

    valid = []
    invalid = []
    #phone_ph.reload_franchiser()

    def clean(number):

        """
        
        Checks if number passes franchise.xml before appending as valid MIN

        :type number: str
        :param number: The MIN to be processed

        :rtype: None
        :return: Nothing

        """

        if number:
            #print(number)
            debug_logger.debug(number)
            phone = _is_valid_format(number)
            #print(phone)
            if not phone:
                invalid.append(number)
                if len(invalid)> 1000:
                    #print("write invalid: "+number)
                    invalid_phones.write("\n".join(invalid)+"\n")
                    del invalid[:]
                #invalid.append(number)
                return
            debug_logger.debug(phone)
            valid.append(phone)
            if len(valid)>1000:
                #print("write valid: "+phone)
                valid_phones.write("\n".join(valid)+"\n")
                del valid[:]

        return

    # Process uploaded file.
    if FILES:
        debug_logger.debug("handling file")
        filepath = _handle_uploads(FILES['contacts_file'])
        debug_logger.debug("done file handle")
        debug_logger.debug("franchising each mobile")
        filter(clean, map(lambda x: str(x[0]), pyexcel.get_sheet(file_name=filepath).rows()))

        debug_logger.debug("done franchising mobiles")


    # Process form.
    contacts_form = POST.get('contacts_form')
    if contacts_form:
        filter(clean, map(
            str.strip,
            str(contacts_form).split(',')))

    if len(invalid)> 0:
        invalid_phones.write("\n".join(invalid)+"\n")
        del invalid[:]
    if len(valid)>0:
        valid_phones.write("\n".join(valid)+"\n")
        del valid[:]
    # Return unique valid numbers.
    return invalid, valid_phones


@transaction.atomic
def _save_contacts(user, contacts_name, filename):

    """
    
    Save contacts to contact groups

    :type user: int
    :param user: The user ID

    :type contacts_name: str
    :param contacts_name: The contact group name

    :type valid_phones: list
    :param valid_phones: The list of valid MINs

    .. note::
        Inserts first at contacts model before updating user contact groups and
            contact groups models
        Insertion to database occurs for every 10,000 MINs inserted at a time

    :rtype: None
    :return: Nothing

    """
    
    debug_logger = logging.getLogger('kastlogger')

    msisdn_list = []
    msisdn_fetch_list = []

    tenant = KastUsers.objects.get(user=user.id).get_tenant()
    contact_group, is_created = ContactGroups.objects\
        .get_or_create(user=user, tenant=tenant, name=contacts_name)

    bulk_contacts = []

    valid = open(filename+".tmp", "r")
    for line in valid:
        msisdn_list.append(line.strip("\n"))
        if len(msisdn_list) > 10000:
            contact_id_by_msisdn = {}
            existing_contacts = Contacts.objects.filter(msisdn__in=msisdn_list)
            for contact in existing_contacts:
                contact_id_by_msisdn[contact.msisdn] = contact.id

            for contact in msisdn_list:
                if contact not in contact_id_by_msisdn:
                    msisdn_fetch_list.append(contact)
                    #print(contact)
                    bulk_contacts.append(Contacts(msisdn=contact, network="", franchise=""))
                else:
                    print(contact)

            Contacts.objects.bulk_create(bulk_contacts)
            del bulk_contacts
            bulk_contacts = []

            existing_contacts = Contacts.objects.filter(msisdn__in=msisdn_fetch_list)
            for contact in existing_contacts:
                contact_id_by_msisdn[contact.msisdn] = contact.id

            msisdn_fetch_list = []

            del existing_contacts

            # check if contact already exists in contact group
            contact_id_set = set(contact_id for contact_id in contact_id_by_msisdn.values())
            contact_ids_in_group = UserContactGroups.objects.filter(
                contact_group=contact_group,
                contact_id__in=contact_id_set
            ).values_list('contact_id', flat=True)

            contact_ids_to_insert = contact_id_set - set(contact_ids_in_group)
            del contact_id_set
            del contact_ids_in_group

            # finally insert new contacts into the group
            bulk_contact_groups = []
            for contact_id in contact_ids_to_insert:
                # todo: do not insert into contact groups that does not have contact
                bulk_contact_groups.append(UserContactGroups(contact_id=contact_id, contact_group=contact_group))
                if len(bulk_contact_groups) > 10000:
                    UserContactGroups.objects.bulk_create(bulk_contact_groups)
                    del bulk_contact_groups
                    bulk_contact_groups = []

            UserContactGroups.objects.bulk_create(bulk_contact_groups)

            msisdn_list = []


    contact_id_by_msisdn = {}
    existing_contacts = Contacts.objects.filter(msisdn__in=msisdn_list)
    for contact in existing_contacts:
        contact_id_by_msisdn[contact.msisdn] = contact.id

    for contact in msisdn_list:
        if contact not in contact_id_by_msisdn:
            msisdn_fetch_list.append(contact)
            #print(contact)
            bulk_contacts.append(Contacts(msisdn=contact, network="", franchise=""))
        else:
            print(contact)


    Contacts.objects.bulk_create(bulk_contacts)

    existing_contacts = Contacts.objects.filter(msisdn__in=msisdn_fetch_list)
    for contact in existing_contacts:
        contact_id_by_msisdn[contact.msisdn] = contact.id

    msisdn_fetch_list = []

    del bulk_contacts
    bulk_contacts = []

    # check if contact already exists in contact group
    contact_id_set = set(contact_id for contact_id in contact_id_by_msisdn.values())
    contact_ids_in_group = UserContactGroups.objects.filter(
        contact_group=contact_group,
        contact_id__in=contact_id_set
    ).values_list('contact_id', flat=True)

    contact_ids_to_insert = contact_id_set - set(contact_ids_in_group)
    del contact_id_set
    del contact_ids_in_group

    # finally insert new contacts into the group
    bulk_contact_groups = []
    for contact_id in contact_ids_to_insert:
        # todo: do not insert into contact groups that does not have contact
        bulk_contact_groups.append(UserContactGroups(contact_id=contact_id, contact_group=contact_group))

    UserContactGroups.objects.bulk_create(bulk_contact_groups)
    del bulk_contact_groups
    bulk_contact_groups = []

    return
