"""

This class contains Contact application models. It defines database fields of Contacts,
Contact Groups and User Contact Groups.

Models:
    Django User Model
    KastTenants

"""

import arrow
from django.db import models
from django.contrib.auth.models import User

from kast_account_app.models import KastTenants
from webapp.middleware import KastApiError


class ContactGroupsManager(models.Manager):
    def get_contact_groups(self, tenant, last_contact_group=0, search_str=""):

        """

        Returns a list of Kast contact groups under a given tenant.

        :type self: obj
        :param self: Contact group model instance

        :type tenant: str
        :param tenant: User tenant

        :rtype: list
        :return: List of contact groups

        .. warning:: Documentation not updated

        """

        format_dict = lambda x: {
            'id': x['id'],
            'name': x['name'],
            'base_count': x['base_count']}

        #if search_str:
        #    search_str += "%"

        if last_contact_group:
            return map(format_dict, self.filter(
                tenant=tenant,
                id__lt=last_contact_group,
                name__istartswith=search_str)\
            .order_by('-created_at')\
            .values('id', 'name')\
            .annotate(base_count=models.Count('usercontactgroups'))[:5])

        return map(format_dict, self.filter(
            tenant=tenant,
            name__istartswith=search_str)\
            .order_by('-created_at')\
            .values('id', 'name')\
            .annotate(base_count=models.Count('usercontactgroups'))[:5])


class UserContactGroupsManager(models.Manager):
    # WANT: Refactor, don't put all numbers in one big list.
    def get_contacts(self, groups):

        """

        Returns a list of contacts under a given contact group.

        :type self: obj
        :param self: Contact group model instance

        :type groups: str
        :param groups: Contact group name

        :rtype: list
        :return: List of contacts

        :raise KastApiError 011: No contacts found.

        """
        
        query = self.filter(contact_group__in=groups, status=1)

        if not query.exists():
            raise KastApiError(
                {'error': 'No contacts found.', 'code': '011'}, 404)

        return set(query.values_list('contact__msisdn', flat=True))


class Contacts(models.Model):
    msisdn = models.CharField(max_length=12, unique=True)
    network = models.CharField(max_length=5)
    franchise = models.CharField(max_length=20)
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'contacts'
        index_together = ['network', 'franchise']


class ContactGroups(models.Model):
    user = models.ForeignKey(User)
    tenant = models.ForeignKey(KastTenants, to_field='id')
    name = models.CharField(max_length=50)
    status = models.SmallIntegerField(default=1, db_index=True)
    created_at = models.DateTimeField(auto_now_add=True, db_index=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = ContactGroupsManager()

    class Meta:
        db_table = 'contact_groups'
        unique_together = ['name', 'tenant']


class UserContactGroups(models.Model):
    contact = models.ForeignKey(Contacts)
    contact_group = models.ForeignKey(ContactGroups)
    status = models.SmallIntegerField(default=1, db_index=True)
    created_at = models.DateTimeField(auto_now_add=True, db_index=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = UserContactGroupsManager()

    class Meta:
        db_table = 'user_contact_groups'
