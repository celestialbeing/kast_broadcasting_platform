from django.conf.urls import url
from . import views

urlpatterns = [

    # Route to KAST dashboard (actually my campaigns SMS broadcast page)
    url(r'^dashboard/$', views.dashboard, name='dashboard'),
    url(r'^dashboard/datatable/$', views.datatable, name='datatable'),
    url(r'^broadcast-guide/$', views.sms_broadcast_guide, name='sms_broadcast_guide'),
]

