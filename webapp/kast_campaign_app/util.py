import redis


util_redis = redis.StrictRedis(
    host='localhost',
    port=6379,
    socket_timeout=5
)


def has_record(transaction_id, numbers):
    existing = retrieve(transaction_id, False)
    if existing:
        return False
    else:
        record(transaction_id, numbers)
        return True


def retrieve(transaction_id, delete):
    name = _create_name(transaction_id)
    wdoc = util_redis.get(name)
    if delete:
        self._redis.delete(name)

    return wdoc


def record(transaction_id, wdoc):
    name = _create_name(transaction_id)
    result = util_redis.set(name, wdoc)
    # 3 days
    util_redis.expire(name, 259200)
    return result


def _create_name(transaction_id):
    return 'bcast:%s' % transaction_id

