import json
import urlparse
from datetime import timedelta

from django.db import transaction
from django.shortcuts import render
from django.http import JsonResponse
from django.shortcuts import redirect
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required

from .models import KastUserCampaigns
from kast_bcast_app.views import cancel_broadcast
from operator import itemgetter

from pprint import pprint


@csrf_exempt
def dashboard(request):

    """
    
    Renders and populates the Kast dashboard

    .. note:: Code shown is KAST dashboard phase 2. Code will be changed soon.

    :type request: obj
    :param request: Request object

    :rtype: obj (HttpResponse)
    :return: Rendered template
        dashboard live
        dashboard live (empty)
        dashboard trial   

    """

    if (request.user.is_authenticated()):
        # User is trying to render the dashboard page
        if (request.session['login_type'] == 'trial'):
            # User is a trial user. Check for tenant.
            if (request.user.kastusers.tenant is not None):
                # user has tenant already. Since user is still on trial, user status is processing
                return redirect('/processing')
            else:
                # user has no tenant. Proceed to trial logic
                if (request.user.kastusers.is_trial_consumed == True):
                    return render(request, 'kast_campaign_app_templates/dashboard-trial.html')
                else:
                    # user is not using his/her trial yet. Route to trial broadcast
                    return redirect('/broadcasts/trial')
        else:
            context = {}
            context['title'] = 'Dashboard'
            # Obviously user is live already
            if (len(KastUserCampaigns.objects.filter(user=request.user.kastusers)) > 0):
                # User has existing campaigns
                return render(request, 'kast_campaign_app_templates/live/dashboard.html', 
                           {'context': context, 'Campaigns': KastUserCampaigns.objects.filter(user=request.user.kastusers)})
            else:
                # User has no existing campaigns
                #return render(request, 'kast_campaign_app_templates/dashboard-live-empty.html')
                return redirect('/broadcast-guide')
    else:
       return redirect('/home')


@login_required
def sms_broadcast_guide(request):

    """
    
    Renders broadcast guide page or redirects to dashboard

    :type request: obj
    :param request: Request object

    :rtype: obj (HttpResponse)
    :return: Rendered broadcast guide page   

    """

    if (request.session['login_type'] == 'live'):
        # Render the how it works page for own base sms broadcast
        return render(request, 'kast_campaign_app_templates/sms-broadcast-guide.html')
    else:
        return redirect('/dashboard')

@login_required
def broadcast_lists(request):

    """
    
    Renders and populates KAST campaign table.

    .. note:: KAST dashboard phase 2 is rendered here

    :type request: obj
    :param request: Request object

    :rtype: obj (HttpResponse)
    :return: Rendered template
        dashboard live
        dashboard live (empty)
        dashboard trial   

    """

    context = {}
    context['title'] = 'Broadcasts'
    if (request.method == "POST"):
            # User has sent a request from dashboard. Handling request
            if (request.POST.get('request_type') == 'delete_campaign'):
            
                campaign_uid      = request.POST.get('campaign_id')
                campaign          = KastUserCampaigns.objects.get(campaign_uid=campaign_uid)
                campaign_id       = campaign.id
                campaign_is_draft = campaign.is_draft

                if (campaign_is_draft == 0):
                    res = cancel_broadcast(request, campaign_id)
                    # Verify JSON response
                    response = json.loads(res.content.decode('utf-8'))
                    if (response['status'] == "cancelled"):
                        campaign.delete()

                    transaction.rollback()

                else:
                    campaign.delete()
                    transaction.rollback()

                # Obviously user is live already
                if (len(KastUserCampaigns.objects.filter(user=request.user.kastusers)) > 0):
                    # User has existing campaigns
                    return render(request, 'kast_bcast_app_templates/live/sms-broadcast-lists.html', 
                               {'context': context, 'Campaigns': KastUserCampaigns.objects.filter(user=request.user.kastusers)})
                else:
                    # User has no existing campaigns
                    #return render(request, 'kast_campaign_app_templates/dashboard-live-empty.html')
                    return redirect('/broadcast-guide')
    else:
       return redirect('/home')


@login_required
@csrf_exempt
def datatable(request):
    data_columns = [
        "kastuserbroadcasts__campaign_id",
        "campaign_name",
        "campaign_schedule",
        "created",
        "kastuserbroadcasts__get_onqueue.count",
        "kastuserbroadcasts__get_forwarded.count",
        "kastuserbroadcasts__get_success.count",
        "kastuserbroadcasts__get_failed.count",
        "kastuserbroadcasts__base_count",
        "message_part",
        "reference_code",
        "",
        "kastuserbroadcasts__message",
        "kastuserbroadcasts__sender_id",
        "kastuserbroadcasts__sender_name",
        "campaign_uid",
        "is_draft",
        "kastuserbroadcasts__broadcast_id"
    ]

    try:
        body = json.loads(request.body)
    except:
        print "POST body empty!"

    search = body["search"]

    search_dict = {}

    columns = body['columns']

    if search.get("value") is not None and search.get("value") != "":
        for index, column in enumerate(columns):

            if column['searchable'] and (index < 4 or index in [8]):
                column_name = data_columns[index]+'__icontains'
                search_dict[column_name] = search['value']

    order_column = body["order"]
    start = int(body["start"])
    end = start + int(body["length"])

    kast_user_campaigns = KastUserCampaigns.objects.filter(
        user=request.user.kastusers)

    if search.get("value") is not None and search.get("value") != "":
        campaigns = KastUserCampaigns.objects.none()

        for key,value in search_dict.iteritems():
            dictionary = {key:value}

            campaigns = campaigns | kast_user_campaigns.filter(**dictionary)

        kast_user_campaigns = campaigns

    column_name = ""
    if order_column[0]['column'] is not None and \
      (order_column[0]['column'] < 4 or order_column[0]['column'] in [8, 9, 10]):
        for order in order_column:
            column_name = data_columns[int(order['column'])]
            if order['dir'] == 'desc':
                column_name = '-' + column_name

            kast_user_campaigns = kast_user_campaigns.order_by(column_name)

    record_count = KastUserCampaigns.objects.filter(user=request.user.kastusers).count()
    filter_count = kast_user_campaigns.count()

    response_dict = {
        "draw": body['draw'],
        "recordsTotal": record_count,
        "recordsFiltered": filter_count,
        "data": []
    }


    for campaign in kast_user_campaigns[start:end]:
        if campaign.campaign_schedule:
            schedule = campaign.campaign_schedule + timedelta(hours=8)
        else:
            schedule = campaign.created + timedelta(hours=8)

        date_created = campaign.created + timedelta(hours=8)
        message_part = get_message_count(campaign.kastuserbroadcasts.message)

        onqueue = campaign.kastuserbroadcasts.get_onqueue()
        forwarded = campaign.kastuserbroadcasts.get_forwarded()
        sent = campaign.kastuserbroadcasts.get_success()
        failed = campaign.kastuserbroadcasts.get_failed()

        response_dict["data"].append({
            "0":campaign.kastuserbroadcasts.campaign_id,
            "1":campaign.campaign_name,
            "2":schedule.strftime("%Y-%m-%d %H:%M:%S"),
            "3":date_created.strftime("%Y-%m-%d %H:%M:%S"),
            "4":onqueue['count'],
            "5":forwarded['count'],
            "6":sent['count'],
            "7":failed,
            "8":campaign.kastuserbroadcasts.base_count,
            "9":message_part,
            "10":campaign.reference_code,
            "11":"",
            "12":campaign.kastuserbroadcasts.message,
            "13":campaign.kastuserbroadcasts.sender_id,
            "14":campaign.kastuserbroadcasts.sender_name(),
            "15":campaign.campaign_uid,
            "16":campaign.is_draft,
            "DT_RowId": campaign.kastuserbroadcasts.broadcast_id
        })

    return JsonResponse(response_dict)


def get_message_count(message):
    if len(message) >= 1 and  len(message) <= 160:
        return 1
    elif len(message) >= 161 and  len(message) <= 306:
        return 2
    else:
        return 3


def clean_data(data):
    result = {}

    for index, value in data.items():
        tmp_index = index.replace("][",",").\
            replace("[",",").replace("]","").\
            split(",")

        if len(tmp_index) == 1:
            result[index] = value
        else:
            print "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
            print "index:",tmp_index
            print "result:", index
            print "value:", value
            result = decode_nest(0, tmp_index, result, value)
            print "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"

    return result



def decode_nest(index, data, result, value):
    if "search" in  data and "value" in  data:
        print(data, value)
        print(result)
    if result:
        start = len(result)
    else:
        start = 0

    if index > (len(data)-1):
        return result

    try:
        if start == 0:
            result = []

        tmp_data = int(data[index])

        for i in range(start,tmp_data):
            if i == (tmp_data-1) and index == (len(data)-1):
                result.append(value)
            else:
                result.append(None)

        result[tmp_data-1] = decode_nest(index+1, data, result[tmp_data-1], value)

    except:
        if start == 0:
            result = {}

        tmp_data = data[index]
        pprint(tmp_data)
        if tmp_data not in result:
            result[tmp_data] = None
        result[tmp_data] = decode_nest(index+1, data, result[tmp_data], value)

    return result








@login_required
def sms_broadcast_guide(request):

    if (request.session['login_type'] == 'live'):
        # Render the how it works page for own base sms broadcast
        return render(request, 'kast_campaign_app_templates/sms-broadcast-guide.html')
    else:
        return redirect('/dashboard')

