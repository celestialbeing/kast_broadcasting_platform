import json
import requests

from channels import Group
from django.conf import settings
from kast_bcast_app import utils
from channels.auth import channel_session_user, channel_session_user_from_http, channel_session


#@channel_session_user_from_http
@channel_session
def ws_connect(message):
    Group('statuses').add(message.reply_channel)
    Group('statuses').send({"text":json.dumps({"message":"connected"})})


@channel_session
def ws_receive(message):
    url = settings.BROADCASTS['status_url']

    onqueue = get_value(url.format(bcast=message["text"], status="onqueue"))
    forwarded = get_value(url.format(bcast=message["text"], status="forwarded"))
    sent = get_value(url.format(bcast=message["text"], status="sent"))
    failed_url = url.format(bcast=message["text"], status="failed")

    if utils.has_record(broadcast_id, ""):
        numbers = utils.retrieve(broadcast_id, False)
    else:
        numbers = ""

    failed = get_value(failed_url+"&numbers=%s" % numbers)

    Group('statuses').send({"text":json.dumps({
        "onqueue": onqueue,
        "forwarded": forwarded,
        "sent": sent,
        "failed": failed,
        "broadcast_id": message["text"]
    })})


def get_value(url):
    try:
        status_code = 400
        while(status_code != 200):
            result = requests.get(url)
            status_code = result.status_code
            if status_code == 200:
                r = json.loads(result.text.decode('utf-8'))
        return r
    except:
        result = {"count":0, "base":[]}
        return json.dumps(result)


@channel_session_user
def ws_disconnect(message):
    Group('statuses').send({"text":json.dumps({})})