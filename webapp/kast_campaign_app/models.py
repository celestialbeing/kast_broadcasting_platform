import json
import uuid
import arrow
import requests

from django.db import models
from django.conf import settings
from . import util
from kast_account_app.models import KastUsers
from kast_sender_id_app.models import KastSIDs

# Main Campaign Manager
class KastCampaignManager(models.Manager):

    """ Kast Campaign Manager Model """

    def create_broadcast(self, user_id, **data):

        """

        Saves/updates a new campaign model

        :type self: obj
        :param self: Class instance of itself

        :type user_id: int
        :param user_id: the user's ID 

        :type **data: dict
        :param **data: Dictionary of campaign data with the following dictionary keys
            c_name
            c_type
            c_schedule
            ref_code
            is_draft
            b_count
            broadcast_id
            status
            sid
            message

        .. note:: 
            updating campaign info such as changing it from draft to live happens here

        :rtype: int
        :return: ID of the newly created/updated campaign

        """

        # required fields
        c_uid   = str(uuid.uuid4().hex)[:15]
        c_name  = data.get('c_name')
        c_type  = data.get('c_type') 

        # optional fields
        c_schedule          = data.get('c_schedule')
        ref_code            = data.get('ref_code')
        is_draft            = data.get('is_draft',False)
        b_count             = data.get('b_count')
        broadcast_id        = data.get('broadcast_id')
        status              = data.get('status')
        sid                 = data.get('sid')
        message             = data.get('message')

        # Add epoch time support on broadcast schedule
        formatted_schedule = None
        if c_schedule:
            formatted_schedule = arrow.get(c_schedule).to('utc').format('YYYY-MM-DD HH:mm:ss')
        else:
            formatted_schedule = c_schedule

        # Check if record exists already for this campaign name
        user = KastUsers.objects.get(id=user_id)
        try:
            campaign  = KastUserCampaigns.objects.get(user=user,campaign_name=c_name,is_draft=True)

            campaign.campaign_type       = c_type
            campaign.campaign_schedule   = formatted_schedule
            campaign.is_draft            = is_draft
            campaign.reference_code      = ref_code
            campaign.save(force_update=True)

            campaign.kastuserbroadcasts.base_count   = b_count
            campaign.kastuserbroadcasts.broadcast_id = broadcast_id
            campaign.kastuserbroadcasts.status       = status
            campaign.kastuserbroadcasts.sender_id    = sid
            campaign.kastuserbroadcasts.message      = message
            campaign.kastuserbroadcasts.save(force_update=True)

            return campaign

        except:
            # This is a new entry
            main_data  = KastUserCampaigns.objects.create(user=user,campaign_uid=c_uid,campaign_name=c_name,campaign_type=c_type,
                         campaign_schedule=formatted_schedule,is_draft=is_draft,reference_code=ref_code)
            campaign   = KastUserCampaigns.objects.get(campaign_name=c_name, user=user)
            sub_data   = KastUserBroadcasts.objects.create(campaign=campaign, base_count=b_count,
                         status=status,broadcast_id=broadcast_id,sender_id=sid,message=message)
            complete_campaign = KastUserCampaigns.objects.get(campaign_name=c_name, user=user)

            return complete_campaign

# Main Campaign Model
class KastUserCampaigns(models.Model):

    """ Kast User Campaign Models """

    user                = models.ForeignKey(KastUsers, on_delete=models.CASCADE)
    campaign_uid        = models.CharField(null=True, max_length=90)
    campaign_name       = models.CharField(null=True, max_length=60)
    campaign_type       = models.CharField(null=True, max_length=60)
    campaign_schedule   = models.DateTimeField(null=True)
    reference_code      = models.CharField(null=True, max_length=90)
    is_draft            = models.BooleanField(default=False)
    created             = models.DateTimeField(auto_now_add=True)
    modified            = models.DateTimeField(auto_now=True)
    objects             = KastCampaignManager()

    class Meta:
        db_table = "kast_user_campaigns"
        unique_together = ('user', 'campaign_name', 'campaign_type')

    def __unicode__(self):

        """ The campaign name will be used to represent a campaign on the Django admin """

        return self.campaign_name


# KAST Own Base Broadcast Model
class KastUserBroadcasts(models.Model):

    """ Kast User Broadcast Models """

    campaign     = models.OneToOneField(KastUserCampaigns, on_delete=models.CASCADE)
    base_count   = models.CharField(null=True, max_length=60)
    status       = models.CharField(null=True, max_length=60)
    broadcast_id = models.CharField(null=True, max_length=60)
    sender_id    = models.CharField(null=True, max_length=30)
    message      = models.TextField(null=True, max_length=480)

    class Meta:
        db_table = "kast_user_broadcasts"

    def __unicode__(self):

        """ The campaign name will be used to represent a campaign on the Django admin """

        return self.campaign.campaign_name

    def sender_name(self):
        try:
            kast_sender = KastSIDs.objects.get(id=self.sender_id)
            return kast_sender.name
        except:
            return ""

    def get_onqueue(self):
        url = settings.BROADCASTS['status_url'].format(
            bcast=self.broadcast_id,
            status="onqueue"
        )
        result = self.get_value(url)
        return result

    def get_forwarded(self):
        url = settings.BROADCASTS['status_url'].format(
            bcast=self.broadcast_id,
            status="forwarded"
        )
        result = self.get_value(url)
        return result

    def get_success(self):
        url = settings.BROADCASTS['status_url'].format(
            bcast=self.broadcast_id,
            status="sent"
        )
        result = self.get_value(url)
        return result

    def get_failed(self):
        if util.has_record(broadcast_id,""):
            numbers = util.retrieve(broadcast_id, False)
        else:
            numbers = ""

        url = settings.BROADCASTS['status_url'].format(
            bcast=self.broadcast_id,
            status="failed",
            numbers=numbers
        )
        result = self.get_value(url+"&numbers=%s" % numbers)
        return result

    def get_value(self, url):
        try:
            # status_code = 400
            # while(status_code != 200):
            result = requests.get(url, timeout=1)
                #status_code = result.status_code
            #if status_code == 200:
            r = json.loads(result.text)
            return r
        except:
            result = {"count":0, "base":[]}
            return result
