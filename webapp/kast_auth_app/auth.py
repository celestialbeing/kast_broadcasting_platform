"""

This module handles user authentication both with and without account password.

Models
    User

"""

from django.contrib.auth.models import User
from django.contrib.auth.hashers import check_password

class AuthBackend(object):    
    def authenticate(self, username=None, password=None):
   
        """
    
        Checks if user account exists through username and password

        :type username: str
        :param username: Account username to be checked

        :type password: str
        :param password: Account password to be checked

        :rtype: obj or None
        :return: User object or nothing

        """

        try:
            user = User.objects.get(email=username)
            if check_password(password, user.password):
                return user
            else:
                return None
        except User.DoesNotExist:
            return None 

    def get_user(self, user_id):

        """

        Checks if user account exists through user ID

        :type user_id: int
        :param user_id: User ID

        :rtype: obj or None
        :return: User object or nothing

        """

        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None

class AuthBackendNoPassword(object):    
    def authenticate(self, username=None):

        """

        Checks if user account exists through username

        :type username: str
        :param username: Account username

        :rtype: obj or None
        :return: User object or nothing

        """

        try:
            user = User.objects.get(email=username)
            if user:
                return user
            else:
                return None
        except User.DoesNotExist:
            return None 

    def get_user(self, user_id):

        """

        Checks if user account exists through user ID

        :type user_id: int
        :param user_id: User ID

        :rtype: obj or None
        :return: User object or nothing

        """
        
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
