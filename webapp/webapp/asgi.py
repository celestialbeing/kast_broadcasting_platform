import os
import sys
from channels.asgi import get_channel_layer

sys.path.append('/home/developer/kast_web_application/webapp')
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'webapp.settings')

application = get_channel_layer()