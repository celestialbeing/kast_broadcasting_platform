from django.conf.urls import include
from django.conf.urls import url
from django.contrib import admin


from django.conf.urls.static import static
from django.conf import settings

# Kast Web Application URL routing
urlpatterns = [

    # Route to admin page (main django project admin)
    url(r'^admin/', include(admin.site.urls)),

    # Route to main login page (kast_home_app)
    url(r'', include('kast_home_app.urls')),

    # Route to account, authorization and authentication
    # related functionalities (kast_account_app)
    url(r'', include('kast_account_app.urls')),

    # Route to broadcast functionalities (kast_bcast_app)
    url(r'^broadcasts/', include('kast_bcast_app.urls')),

    # Route to contacts functionalities (kast_contact_app)
    url(r'^contacts/', include('kast_contact_app.urls')),

    # Route to landing(dashboard) page. Serves as a bridge to other campaign apps
    url(r'', include('kast_campaign_app.urls')),

    # Route to message template (kast_message_template_app)
    url(r'', include('kast_message_template_app.urls')),

    # Route to user management (kast_user_management_app)
    url(r'', include('kast_user_management_app.urls')),

    # Route to sender id page (kast_sender_id_app)
    url(r'', include('kast_sender_id_app.urls')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
