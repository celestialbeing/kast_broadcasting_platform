import os
import sys
from django.core.wsgi import get_wsgi_application

sys.path.append('/home/developer/kast_web_application/webapp')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "webapp.settings")
application = get_wsgi_application()
