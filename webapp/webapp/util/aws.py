"""

Uses BOTO3 to generate a public URL of a file on a site running with an AWS instance

"""

import boto3

from django.conf import settings


conf = settings.AWS
access_key_id = conf['access_key_id']
secret_access_key = conf['secret_access_key']


class S3(object):
    def upload_csv(self, phonenumbers, key):

        """

        Uploads file to AWS server and returns an accessible URL to the uploaded file

    
        :type phonenumbers: list
        :param phonenumbers: List of MINs

        :type key: str
        :param key: CSV filename containing MINs

        :rtype: str
        :return: Accessible file URL from AWS

        """
        
        bucket = conf['s3']['bucket']
        file = '%s%s' % (key, '.csv')

        # WANT: Better csv encoder.
        content = '\n'.join(phonenumbers)

        # Do upload.
        s3 = boto3.client('s3',
            aws_access_key_id=access_key_id,
            aws_secret_access_key=secret_access_key)
        s3.put_object(Body=content, Bucket=bucket,
            Key=file, ContentType='text/csv')

        return s3.generate_presigned_url(
            'get_object',
            {'Bucket': bucket, 'Key': file},
            ExpiresIn=conf['s3']['url_expires_in'])

    def upload_from_file(self, filename, key):
        bucket = conf['s3']['bucket']
        file = '%s%s' % (key, '.csv')

        # Do upload.
        s3 = boto3.client('s3',
            aws_access_key_id=access_key_id,
            aws_secret_access_key=secret_access_key)

        s3.upload_file(Filename=filename+".tmp",
                       Bucket=bucket, Key=file)

        return s3.generate_presigned_url(
            'get_object',
            {'Bucket': bucket, 'Key': file},
            ExpiresIn=conf['s3']['url_expires_in'])
