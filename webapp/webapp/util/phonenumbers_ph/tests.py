import unittest

import phonenumbers_ph as phone_ph


class TestPhoneNumbers(unittest.TestCase):
    def test_parse(self):
        phone = phone_ph.parse('09178824474')

        self.assertEqual(phone.number, '+639178824474')
        self.assertEqual(phone.network, 'Globe')
        self.assertEqual(phone.plan, 'Postpaid')
        self.assertEqual(phone.normalize('local'), '0917 882 4474')

    def test_fail_parse(self):
        phone = phone_ph.parse('bae bae bae')
        self.assertEqual(phone, None)

    def test_set_attribute(self):
        phone = phone_ph.parse('09055169553')

        with self.assertRaises(AttributeError):
            phone.number = '0905 516 9553'

        with self.assertRaises(AttributeError):
            phone.network = 'Globe'

        with self.assertRaises(AttributeError):
            phone.plan = 'Postpaid'

    def test_multiple_parse(self):
        phone1 = phone_ph.parse('0905-516-9553')
        phone2 = phone_ph.parse('Tel. +63917 882 4474')

        self.assertEqual(phone1.number, '+639055169553')
        self.assertEqual(phone2.number, '+639178824474')
