"""

.. warning:: THIS MODULE IS NOW DEPRECATED

Philippine Phone Numbers validation python module.

Checks franchise via xml file (needs to update manually).
Uses phonenumberslite module for phone number formatting
and other features.

SYNOPSIS:
import phonenumbers_ph as phone_ph

phone = phone_ph.parse('09178824474')
phone.number    # '+639178824474'
phone.network   # 'Globe'
phone.plan      # 'Postpaid'

# Use other formatting.
phone.normalize('local') # '0917 882 4474'

"""

import re
import os
from xml.etree import ElementTree

import phonenumbers
from phonenumbers.phonenumberutil import NumberParseException


class Franchise(object):
    __definitions = None

    def __init__(self, franchise_file):

        """

        Sorts franchise XML file in a more readable format

        :type franchise_file: str
        :param franchise_file: file path of the franchise XML file

        :rtype: None
        :return: Nothing

        """

        if Franchise.__definitions is None:
            tree = ElementTree.parse(franchise_file)
            Franchise.__definitions = [{
                'desc':     _.find('desc').text,
                'type':     _.find('type').text,
                'network':  _.find('network').text,
                'regex':    re.compile(_.find('range').text),
            } for _ in tree.findall('number_range')]

    def get_info(self, PN):
        info = {}
        number = phonenumbers.format_number(
            PN, phonenumbers.PhoneNumberFormat.E164)

        for def_ in Franchise.__definitions:
            pattern = def_.get('regex')

            # WANT: Splice plus sign (+) in number efficiently.
            if pattern.match(number[1:]):
                info = def_
                break

        return PhoneNumber(PN, info) if info else None

franchise_file = os.path.join(os.path.dirname(__file__), 'franchise.xml')
franchiser = Franchise(franchise_file)


def reload_franchiser():
    global franchiser
    franchiser = Franchise(franchise_file)


def parse(number):

    """

    Checks if number is valid PH phone number.
    Returns franchise details about it.

    :type number: int
    :param number: The phone number to parse

    :rtype: obj
    :return: PhoneNumber instance if valid, otherwise None.
    
    """

    try:
        PN = phonenumbers.parse(number, 'PH')
        if not (phonenumbers.is_possible_number(PN)\
            and phonenumbers.is_valid_number(PN)):

            raise NumberParseException(
                1, 'Failed is_possible_number() or is_valid_number().')
    except NumberParseException:
        return None

    return franchiser.get_info(PN)


class PhoneNumber(object):
    def __init__(self, PN, info):

        """
        
        Sets franchise details of a MIN such as network and type

        :type PN: int
        :param PN: MIN 

        :type info: obj
        :param info: The franchise parser object maybe

        :rtype: None
        :return: Nothing

        """

        self._PN = PN

        self._number = self.normalize()
        self._network = info.get('network').capitalize()
        self._plan = info.get('type').capitalize()

    @property
    def number(self):
        return self._number

    @property
    def network(self):
        return self._network

    @property
    def plan(self):
        return self._plan

    @property
    def get_dict(self):
        return {
            "number": self._number,
            "network": self._network,
            "plan": self._plan
        }

    def normalize(self, format='e164'):

        """

        Returns different MINs formats

        :type format: str
        :param format: E164 format (e.g. +111222333)

        :rtype: dict
        :return: International, local and E164 format of MINs

        .. note::
            - International format example +1 111-222-333
            - Local format example (111) 222-333
            - E164 format example +111222333

        """

        formats = {
            'intl': phonenumbers.PhoneNumberFormat.INTERNATIONAL,
            'local': phonenumbers.PhoneNumberFormat.NATIONAL,
            'e164': phonenumbers.PhoneNumberFormat.E164}

        number_format = formats.get(format)
        return phonenumbers.format_number(self._PN, number_format)
