"""

This module initializes KAST API error exception used by other modules
	if API hit fails.

"""

import logging

from django.http import JsonResponse


class KastApiError(Exception):

	"""

	Handles KAST API error exception

	"""

    def __init__(self, data, status):

		"""

		Raises KAST API exception if conditions are not met.

		:type data: dict
		:param data: JSON data indicating error description and error code

		:type status: int
		:param status: HTTP response code

		:rtype: None
		:return: Nothing

		"""

        self.data = data
        self.status = status


class KastApiExceptionMiddleware(object):
    def process_exception(self, request, ex):

		"""
	
		Raises KAST API exception if conditions are not met.

		:type request: dict
		:param request: JSON data indicating error description and error code

		:type ex: int
		:param ex: HTTP response code

		:rtype: None
		:return: Nothing

    	"""
    	
        if not isinstance(ex, KastApiError):
            return None

        logger = logging.getLogger('django.server')
        logger.exception(ex)
        return JsonResponse(ex.data, status=ex.status)
