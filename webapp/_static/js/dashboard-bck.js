$("#header-navigation").sticky({ topSpacing: 0 });

var grid = $("#grid-data").bootgrid({
    formatters: {
        "summary": function(column, row) {
        var schedule = row.schedule.split(" ");
        var date = schedule[0];
        var time = schedule[1];
            return "<a data-toggle=\"modal\" href=\"javascript:void(0)\" class=\"btn-dashboard btn-summary\" data-row-campaign=\"" + row.campaign + "\" data-row-audience=\"" + row.basecount + "\" data-row-message=\"" + row.message + "\" data-row-senderid=\"" + row.sid + "\" data-row-date=\"" + date + "\" data-row-time=\"" + time + "\" title=\"View Summary\"><span class=\"fa fa-eye\"></span></a> <a href=\"/broadcasts/live/?q=" + row.id + "\" class=\"btn-dashboard btn-edit\" data-row-id=\"" + row.id + "\" title=\"Edit\"><span class=\"fa fa-edit\"></span></a> <a data-toggle=\"modal\" href=\"javascript:void(0)\" class=\"btn-dashboard btn-delete\" data-row-id=\"" + row.id + "\" data-row-campaign=\"" + row.campaign +"\" title=\"Delete\"><span class=\"fa fa-trash-o\"></span></a> <a href=\"/broadcasts/get_report/" + row.report_id + "\" class=\"btn-dashboard btn-request\" title=\"Request for a Report\"><span class=\"fa fa-bar-chart\"></span></a>"
        },
        "campaign_name": function(column, row){

            return '<a href="#" data-toggle="tooltip" data-placement="bottom" title="'+row.campaign+'">'+row.campaign+'</a>';
        },
        "campaign_delivered": function(column, row){
            return '<span style="color:#15ce67;">'+row.delivered+'</span>';
        },
        "campaign_failed": function(column, row){
            
            if(row.failed != '-'){
                return '<a href="javascript:void(0)" data-id="'+row.report_id+'" style="color: #cf6162;text-decoration: underline;" class="btn-failed"><span>'+row.failed+'</span></a>';
            }
            else{
                return '-';
            }
        }
    }
}).on("loaded.rs.jquery.bootgrid", function() {
    grid.find(".btn-summary").on("click", function() {
    // if(!$(this).hasClass("btn-disabled")) {
        $("#broadcast-summary").modal("show");
    // }
    });

    grid.find(".btn-delete").on("click", function(){
    if(!$(this).hasClass("btn-disabled")) {
        $("#deleteModal").modal({backdrop: 'static'});
        $("#deleteModal").modal("show"); 
    }
    });

    grid.find("a.btn-failed").on("click", function(){
        var report_id = $(this).data("id");
        console.log(report_id);
        $("#modalFailed").modal("show");
    });

    /*$('.btn-failed').click(function(){

        var report_id = $(this).data("id");
        console.log(report_id);
        $("#modalFailed").modal("show");
    });*/

    /*auto update statuses*/
    setInterval(function(){

        //call to api ajax
        //remove class btn-disabled to report if on_forwarded is not equal to none

        $("#grid-data  tbody tr").each(function(){
            var on_queue = $(this).find("td:nth-child(4)").html(600);
            var on_forwarded = $(this).find("td:nth-child(5)").html(500);
            var on_sent = $(this).find("td:nth-child(6)").html('<span style="color:#15ce67;">300</span>');
            var on_failed = $(this).find("td:nth-child(7)").html('<a href="javascript:void(0)" data-id="2" style="color: #cf6162;text-decoration: underline;" class="btn-failed"><span>50</span></a>');
        });

    }, 30000);

    var $bigList = $('#modalFailed .list-failed'), group;

    while((group = $bigList.find('li:lt(2)').remove()).length){
        ul = $('<ul/>').addClass('sub-list list-group').append(group);
        $("<div/>").addClass("col-md-3 col-sm-3").append(ul).appendTo("#modalFailed .panel-body");
        //$('<ul/>').addClass('sub-list list-group').append(group).appendTo('.right');
    }

    /** HIDE PAGINATION ITEMS BASED ON ROW COUNT **/
    $(".pagination").hide();
    var total = $("#grid-data").bootgrid("getTotalRowCount");
    var countDD = $(".dropdown > .dropdown-toggle > .dropdown-text").text();

    if (countDD < total) {
    $(".pagination").show();
    }

    /** ACTION ICONS AVAILABILITY BASED ON STATUS VALUE **/
    $("#grid-data  tbody tr").each(function(){
    var on_forwarded = $(this).find("td:nth-child(5)").html();

    if(on_forwarded != "-"){

        $(this).find("td:nth-child(10)").find("a.btn-request").removeClass("btn-disabled");
        $(this).find("td:nth-child(10)").find("a.btn-edit").addClass("btn-disabled");
        $(this).find("td:nth-child(10)").find("a.btn-delete").addClass("btn-disabled");
    }
    else{

        $(this).find("td:nth-child(10)").find("a.btn-request").addClass("btn-disabled");
    }

        /*if (stat === "COMPLETED") {
            $(this).find("td:nth-child(7)").find("a.btn-edit").addClass("btn-disabled");
            $(this).find("td:nth-child(7)").find("a.btn-delete").addClass("btn-disabled");
        } else 
        if (stat === "DRAFT") {
            $(this).find("td:nth-child(7)").find("a.btn-request").addClass("btn-disabled");
        } else if (stat === "FORWARDED") {
            $(this).find("td:nth-child(7)").find("a.btn-edit").addClass("btn-disabled");
            $(this).find("td:nth-child(7)").find("a.btn-delete").addClass("btn-disabled");
            $(this).find("td:nth-child(7)").find("a.btn-request").removeClass("btn-disabled");
        } else if (stat === "SCHEDULED") {
            $(this).find("td:nth-child(7)").find("a.btn-edit").addClass("btn-disabled");
            $(this).find("td:nth-child(7)").find("a.btn-request").addClass("btn-disabled");
        }*/
    });

    /** DISABLE LINK IF ICON IS DISABLED **/
    $("#grid-data tr td:nth-child(10) a").each(function(e){
    if($(this).hasClass("btn-disabled")) {
        $(this).attr("href", "javascript:void(0)");    
    }
    });

    /** STATUS COLORING
    $(".table-condensed>tbody>tr>td:nth-child(4)").each(function(){
        var status = $(this).text().toLowerCase();

        if (status == "completed") {
            $(this).addClass("status-green");
        } else if (status == "draft") {
            $(this).addClass("status-black");
        }
    });
    **/

    /*$('#deleteModal').on('shown.bs.modal', function () {
      localStorage.removeItem('successDelete');
      var successDelete = localStorage.getItem('successDelete');
    });*/

    /*$('#successModalDashboard').on('shown.bs.modal', function () {
      localStorage.removeItem('successDelete');
      var successDelete = localStorage.getItem('successDelete');
    });*/

    $('#deleteModal').on('hidden.bs.modal', function (e) {

      localStorage.removeItem('successDelete');
      var successDelete = localStorage.getItem('successDelete');

    });

    $('#successModalDashboard').on('hidden.bs.modal', function (e) {

      localStorage.removeItem('successDelete');
      var successDelete = localStorage.getItem('successDelete');

      window.location = '/dashboard';

    });

    $('.datepicker').datepicker({
        format: 'mm-dd-yyyy'
    });

    var theadCtr = 0;
    $(".table-condensed>thead>tr>th").each(function(){
        theadCtr ++;

        if (theadCtr < 9) {
            $(this).children("a").children("span.fa").addClass("fa-sort");
        }
    });

    $(".btn-summary").click(function(){
        var rowCampaignName         = $(this).data('row-campaign');
        var rowAudience         = $(this).data('row-audience');
        var rowMessage          = $(this).data('row-message');
        var rowSID          = $(this).data('row-senderid');
        var rowScheduleDate         = $(this).data('row-date');
        var rowScheduleTime         = $(this).data('row-time');

        $(".modal-summary #cname").html(rowCampaignName);
        $(".modal-summary #caudience").html(rowAudience);
        $(".modal-summary #cmessage").html(rowMessage);
        $(".modal-summary #csenderid").html(rowSID);
        $(".modal-summary #cdate").html(rowScheduleDate);
        $(".modal-summary #ctime").html(rowScheduleTime);
    });

    $(".btn-delete").click(function(){
    var rowCampaignID   = $(this).data('row-id');
    var rowCampaignName     = $(this).data('row-campaign');

    $("#deleteModal #modal-campaign-name").html(rowCampaignName);
    $("#deleteModal #delete-confirm").val(rowCampaignID);

    /* DO NOT DELETE FOR NOW 
     ************************
    var result = "false";

    $("#delete-form #delete-yes").click(function(){
        var cdata = $("#delete-form").serialize();

        $.ajax({
            type: "POST",
            dataType: "JSON",
            data: cdata,
        async: false,
            success: function(data) {
            var status = data.status;
            doThis(status);
                    $("#grid-data").bootgrid("reload"); 
            },
        error: function() {
            console.log("ERROR!");
            return false;
        }
        });

        console.log(result);
        console.log("bootgrid: reload");
            $("#grid-data").bootgrid("reload"); 

        if(result === "true" ) {
            $("#deleteModal").modal("hide");
            $("#successModalDashboard").modal({backdrop: 'static'});
            $("#successModalDashboard").modal("show");
        }
    });
    */

    localStorage.setItem('successDelete', true);
    });
});/*.on("click.rs.jquery.bootgrid", function(e, columns, rows)
{

    //get column index

    console.log(e);
    console.log(columns);
    console.log(rows);
});*/

function doThis(data) {
    if(data === "ok"){
        result = "true";
    } else {
        result = "false";
    }
}

/** FILTER DROPDOWN **/
$(document).ready(function(){
    var filter_field = '<div class="filter-container form-group"><div class="input-group"><label for="filter" class="filter-label">Filter by: </label><br /><select class="filter form-control"><option value="">All</option><option value="4">Campaign Name</option><option value="5">Schedule</option><option value="6">Date Created</option><option value="8">Campaign Base</option></select></div></div>';
    var status_field = '<div id="status-container" class="filter-container form-group"><div class="input-group"><label for="filter" class="filter-label">Status: </label><br /><select id="status" class="filter form-control"><option value="">All</option><option value="Draft">Draft</option><option value="Forwarded">Forwarded</value><option value="Scheduled">Scheduled</option></select></div></div>';
    var date_field = '<div id="date-container" class="filter-container form-group"><div class="input-group"><label for="filter" class="filter-label">Date: </label><br /><input id="date" class="datepicker form-control" /></div></div>';
    var items_per_page_label = '<label for="items-per-page" class="items-per-page-label">Rows:</label><br />';

    $(".search").before($(filter_field));
    $(".actions").before($(status_field));
    $(".actions").before($(date_field));
    $(".actions .btn-group:first-child:not(last-child)").before($(items_per_page_label));

    $(".search, #status-container, #date-container").hide();

    var successDelete = localStorage.getItem('successDelete');

    if (successDelete !== null) {
    localStorage.removeItem('successDelete');
        $("#successModalDashboard").modal({backdrop: 'static'});
        $("#successModalDashboard").modal("show");
    }
});

/** SEARCH BY FILTER **/
$(document.body).on('change', '.filter, #date, #status', function(){
    var filter = $(".filter").val();
    var search = $(".search-field").val();
    var date = $("#date").val();
    var cstatus = $("#status").val();

    if(!(filter == 5 || filter == 6 || filter == 7)) {
    $(".search").show();
    $("#date-container, #status-container").hide();

        $("#grid-data").bootgrid("clearParams");
        $("#grid-data").bootgrid("addParams", search, filter);
    } else {
    if(filter == 5 || filter == 6) {
        $("#date-container").show();
        $(".search, #status-container").hide();

            $("#grid-data").bootgrid("clearParams");
            $("#grid-data").bootgrid("addParams", date, filter);
    } else if(filter == 7) {
        $("#status-container").show();
        $(".search, #date-container").hide();

            $("#grid-data").bootgrid("clearParams");
            $("#grid-data").bootgrid("addParams", cstatus, filter);
    } else if(filter == "") {
        $("#status-container, #date-container").hide();
        $("#grid-data").bootgrid("clearParams");

            $("#grid-data").bootgrid("removeParams", search, filter);
            $("#grid-data").bootgrid("removeParams", date, filter);
            $("#grid-data").bootgrid("removeParams", cstatus, filter);
    }
    } 
});

$(".search-field").on("change input keypress", function() {
    var filter = $(".filter").val();
    var search = $(this).val();

    $("#grid-data").bootgrid("clearParams");

    if(search.length !== 0) {
        $("#grid-data").bootgrid("addParams", search, filter);
    } else {
        $("#grid-data").bootgrid("removeParams", search, filter);
    } 
});

/** RELOAD BOOTGRID ASYNCHRONIOUS ON SUCCESSFUL ROW DELETE **/
/*
$("#successModalDashboard").on("shown.bs.modal", function(){
    console.log("bootgrid: reload");
    $("#grid-data").bootgrid("reload");     
});
*/

/** KILL DEBUGGER
$(document).on("contextmenu",function(e){        
   e.preventDefault();
});

$(document).keydown(function(event){
    if(event.keyCode==123) {
        return false;
    } else if(event.ctrlKey && event.shiftKey && event.keyCode==73) {        
      return false;  //Prevent from ctrl+shift+i
    }
});
**/
