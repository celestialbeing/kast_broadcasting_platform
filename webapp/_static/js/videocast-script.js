app.directive('wizard', [function() {
    return {
        restrict: 'EA',
        scope: {
            stepChanging: '='
        },
        compile: function(element, attr) {
            element.steps({
                bodyTag: attr.bodyTag,
                transitionEffect: "slideLeft",
                autoFocus: true,
                titleTemplate: '<div style="width:100%;"><span class="steps-index">#index#</span></div><span class="steps-text">#title#</span>',
                labels: {
                    next: "Next",
                    previous: "Back",
                    finish: 'Next',
                    loading: "Loading ..."
                },
                autoFocus: true,
                enableContentCache: false,
                preloadContent: true,
                saveState: true,
                startIndex: 0,
                onInit: function (event, currentIndex) {

                    $("#videocast-container").show();

                    $(".btn-back").click(function(){
                         parent.$.fancybox.close();
                    });

                    $("body").tooltip({ selector: '[data-toggle=tooltip]', container: '#videocast-container', trigger: 'hover'});
                    $(".fancybox-upload").fancybox({
                        width:350,
                        height:270,
                        autoHeight: false,
                        autoResize: false,
                        fitToView: false,
                        autoSize: false,
                        padding: 0,
                        margin: 0
                    });

                    $("#uploadBtnContact").change(function(){
                        var file = this.files[0];
                        console.log(file);

                        $("#uploadFileContact").val(file.name);

                        //redirect to upload warning if error
                        $(".upload-contacts-warning").show();
                        $(".modal-upload-contacts").hide();
                        $(".save-contact-list").hide();

                    });

                    $('#videocast-container').on('change', '#uploadBtnLogo', function(){
                        var file = this.files[0];
                        console.log(file);

                        $("#uploadFileLogo").val(file.name);

                    });

                    $('#videocast-container').on('change', '#uploadBtnVideo', function(){
                        var file = this.files[0];
                        console.log(file);

                        $("#uploadFileVideo").val(file.name);

                    });

                    return true;
                },
                onStepChanged: function(event, currentIndex, priorIndex){

                    //for step1
                    if(currentIndex == 0 || currentIndex == 2){

                        $("#videocast-container .content").css('min-height', '30em');
                    }
                    else if(currentIndex == 1){

                        $("#videocast-container .content").css('min-height', '57em');
                    }
                },
                onFinishing: function (event, currentIndex) 
                { 
                    console.log(currentIndex);
                    $.fancybox( '#broadcast-summary', {

                        width: 500,
                        height: 500,
                        autoHeight: false,
                        autoResize: false,
                        fitToView: false,
                        autoSize: false, 
                        closeBtn: false
                    } );
                    return false; 
                }
            });

            return {
                //pre-link
                pre:function() {},
                //post-link
                post: function(scope, element) {
                    element.on('stepChanging', scope.stepChanging);
                }
            }
        }
    };
}]);
  
  
app.controller('Ctrl', ['$scope', function($scope) {
    $scope.stepChanging = function() {
  
    };

    $scope.save = function(){

        console.log($scope.model);
    }
}]);