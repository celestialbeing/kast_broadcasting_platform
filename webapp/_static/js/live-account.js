$("#header-navigation").sticky({ topSpacing: 0 });   

var form = $("#live-account-form");

/** COMPANY NAME CONVERT TO UPPERCASE **/
$(function() {
    $('#live-account-form #name, #live-account-form #company').keyup(function() {
        this.value = this.value.toLocaleUpperCase();
    });
});

$("#company").keyup(function(event){

    if(event.keyCode == 13){

        $("#live-account-form").submit();
    }

    $("#company-error").html('');
    $(this).removeClass('error');
});

/** VALIDATION RULES **/
$.validator.addMethod("notwhitespace", function(value, element) {
    var newVal = value.trim();
    return /^[^\s].*/.test(newVal);
});

$.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[a-zA-Z ]+$/i.test(value);
}, "Please input a valid name");

$.validator.addMethod("alphanumeric", function(value, element) {
    return this.optional(element) || /^[a-zA-Z0-9 ]+$/i.test(value);
}, "Please input a valid company name");

form.validate({
    highlight: function(element, errorClass, validClass) {
        $(element).addClass(errorClass).removeClass(validClass);
        $(element).siblings("span").removeClass("fa-check-circle").addClass("fa-times-circle");
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass(errorClass).addClass(validClass);
        if ($(element).val() !== '') {
            $(element).siblings("span").removeClass("fa-times-circle").addClass("fa-check-circle");
        } else {
            $(element).siblings("span").removeClass("fa-check-circle fa-times-circle");
        }
    },
    onkeyup: false,
    errorPlacement: function errorPlacement(error, element) { element.before(error); },
    rules: {
        name: {
            lettersonly: true,
            maxlength: 50,
        },
	company: {
	    alphanumeric: true,
        notwhitespace: true,
        maxlength: 50,
	},
        mobile: {
            number: true,
            minlength: 11,
	    maxlength: 12
        },
        email: {
            email: true
        },
        password: {
            minlength: 5
        },
        cpassword: {
            minlength: 5,
            equalTo: "#password"
        }
    },
    messages: {
        cpassword: {
            equalTo: "Your passwords do not match"
        },
        company: {

            notwhitespace: "Please input a valid email address",
        }
    },
    submitHandler: function(form) {
        $.ajax({
            type: "POST",
	    data: $("#live-account-form").serialize(),
            dataType: 'json',
            success: function (data) {
            }
        });

        $('#liveAccountModal').modal({backdrop: "static"});
        $('#liveAccountModal').modal('show');
    }
});

/** DISABLE SUBMIT BUTTON IF INPUT IS INCOMPLETE **/
$(document).ready(function() {
    var empty = false;
    $('input:not([type="submit"])').each(function() {
        if ($(this).val().length === 0 && $.trim($(this).val()) === '') {
            empty = true;
        }
    });

    if (empty) {
        $('.btn-submit').attr('disabled', 'disabled');
    } else {
        $('.btn-submit').removeAttr('disabled');
    }    
});

(function() {
    $('input:not([type="submit"])').on("change keyup blur mouseenter", function() {

        var empty = false;
        $('input:not([type="submit"])').each(function() {
            if ($(this).val().length === 0 && $.trim($(this).val()) === '') {
                empty = true;
            }
        });

        if (empty) {
            $('.btn-submit').attr('disabled', 'disabled');
        } else {
            $('.btn-submit').removeAttr('disabled');
        }
    });
})()
