$("#header-navigation").sticky({ topSpacing: 0 });   

/** NAME CONVERT TO UPPERCASE **/
$(function() {
    $('#registration-form #name').keyup(function() {
        this.value = this.value.toLocaleUpperCase();
    });
});

$("#email").focusout(function(event){

    $(".email-error").html("");
    $("#email-error").remove();
    $(this).removeClass("error");
    $(this).siblings("span").removeClass("fa-check-circle fa-times-circle");
    if(/^[^\s].*/.test($("#email").val()) === false){

        $(".email-error").html("Please input a valid email address");
        $(this).addClass('error');
        $(this).siblings("span").removeClass("fa-check-circle").addClass("fa-times-circle");
    }

});

/** RESEND VERIFICATION CODE **/
$("#resend").click(function(){
    if (!$(this).hasClass("link-disabled")) {
	$(this).addClass("link-disabled").attr("href", "javascript:void(0)");
        $("#registration-form #request_type").val("registration_verif_resend");
        $("#registration-form").submit();
	
	$(".verification-confirm").show(500);
	setInterval('$(".verification-confirm").hide(500)', 6000);

        /** ENABLE RESEND VERIFICATION CODE AFTER 1 MINUTE **/
        setInterval(function(){
	    $("#resend").removeClass("link-disabled").attr("href", "#)");
	}, 60000);
    }
});

/** REGISTRATION FORM VALIDATION **/
var form = $("#registration-form");

/** VALIDATION RULES **/
$.validator.addMethod("notwhitespace", function(value, element) {
    var newVal = value.trim();
    return /^[^\s].*/.test(newVal);
});

$.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[a-zA-Z ]+$/i.test(value);
}, "Please input a valid name"); 

$.validator.addMethod("alphanumeric", function(value, element) {
    return this.optional(element) || /^[a-zA-Z0-9]+$/i.test(value);
}, "Please input a valid name"); 

$.validator.addMethod("kastverif", function(value, element) {
    return this.optional(element) || /^[A-Z0-9]+$/.test(value);
}, "Please input the correct verification code");

var result = "false";

$.validator.addMethod("checkExists", function(value, element) {

    //remove double error view
    $("#email-error").remove();

    var inputElem = $('#registration-form :input[name="email"]'),
        data = { "request_type":"registration_check_email", "email": inputElem.val() },
        eReport = '';

    $.ajax({
        type: "POST",
        dataType: "json",
        data: data,
	async: false, 
        success: function(returnData) {
	    var status = returnData.status;
	    doThis(status);
        },
        error: function(xhr, textStatus, errorThrown) {
	    console.log("URL issue...");
            return false;
        }
    });

    console.log(result);

    if (result === "true") {
       return true;
    } else {
       return false;
    }
}, '');

function doThis(data) {
    if(data === "ok"){
 	result = "true";
    } else {
	result = "false";
    }
}

form.validate({
    highlight: function(element, errorClass, validClass) {
        $(element).addClass(errorClass).removeClass(validClass);
        $(element).siblings("span").removeClass("fa-check-circle").addClass("fa-times-circle");
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass(errorClass).addClass(validClass);
        if ($(element).val() !== '') {
            $(element).siblings("span").removeClass("fa-times-circle").addClass("fa-check-circle");
        } else {
            $(element).siblings("span").removeClass("fa-check-circle fa-times-circle");
        }
    },
    errorPlacement: function errorPlacement(error, element) { element.before(error); },
    rules: {
	   name: {
	       lettersonly: true,
           maxlength: 50,
	   },
	   mobile: {
    	    number: true,
    	    minlength: 11,
    	    maxlength: 12,
	   },
        email: {
            email: true,
            notwhitespace: true,
	        checkExists: true	    
        },
        password: {
            minlength: 5,
            maxlength: 128,
        },
        cpassword: {
            minlength: 5,
            maxlength: 128,
            equalTo: "#password"
        }
    },
    messages: {
	mobile: {
	    number: "Please input a valid mobile number"
	},
	email: {
	    email: "Please input a valid email address",
	    checkExists: "This email is already registered",
        notwhitespace: "Please input a valid email address",
	},
        cpassword: {
            equalTo: "Your passwords do not match"
        }
    },
    submitHandler: function(form) {
	$.ajax({
            type: "POST",
	    encoding:"UTF-8",
	    data: $('#registration-form').serialize(), 
            success: function () {
		var reg = $('#registration-form').serialize();
            }
        });

	$('#registrationModal').modal({backdrop: 'static'});
	$('#registrationModal').modal('show');
    }
});

var verif = $("#verification-form");

/** VERIFICATION CODE CONVERT TO UPPERCASE **/
$(function() {
    $('#verification-form #code').keyup(function() {
       	this.value = this.value.toLocaleUpperCase();
    });
});

verif.validate({
    highlight: function(element, errorClass, validClass) {
        $(element).addClass(errorClass).removeClass(validClass);
        $(element).siblings("span").removeClass("fa-check-circle").addClass("fa-times-circle");
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass(errorClass).addClass(validClass);
        if ($(element).val() !== '') {
            $(element).siblings("span").removeClass("fa-times-circle").addClass("fa-check-circle");
        } else {
            $(element).siblings("span").removeClass("fa-check-circle fa-times-circle");
        }
    },
    errorPlacement: function errorPlacement(error, element) { element.before(error); },
    rules: {
	code: {
	    minlength: 6,
	    maxlength: 6,
	    kastverif: true
	}
    },
    submitHandler: function(verif) {

	$.ajax({
            type: "POST",
	    url: "/register/trial/",
	    encoding:"UTF-8",
	    data: $('#verification-form').serialize(),
            dataType: 'json', 
            success: function (data) {
		if (data.status !== "ok") {
		   console.log("data");
                   var label = '<label class="error" style="margin-top:1em">Invalid verification code</label>';
                   $("#verification-form .form-group:first-of-type").append(label);
                   $("#verification-form #code").addClass("error");
                   $("#verification-form .form-group:first-of-type span").removeClass("fa-check-circle").addClass("fa-times-circle");
		} else {

        	   $('#registrationModal').modal('hide');
	           $('#successModal').modal({backdrop: 'static'});
	           $('#successModal').modal('show');      
		} 
            }
        });

    }
});

/** VERIFICATION CODE EMAIL INTERPOLATION **/
var formEmail = $("#registration-form input[type='email'").val();
$("#registrationModal #modal-email").html(formEmail);

/** DISABLE SUBMIT BUTTON IF INPUT IS INCOMPLETE **/
$(document).ready(function() {
    var empty = false;
    $('#registration-form input:not([type="submit"])').each(function() {
        if ($(this).val().length === 0 && $.trim($(this).val()) === '') {
            empty = true;
        }
    });

    if(!$("#registration-form #tupp:checked").length > 0) {
	empty = true;
    } 

    if (empty) {
        $('.btn-submit').attr('disabled', 'disabled');
    } else {
        $('.btn-submit').removeAttr('disabled');
    }   
});

$('#registration-form input:not([type="submit"])').on("change keyup blur mouseenter", function() {
    var empty = false;
    $('#registration-form input:not([type="submit"])').each(function() {
        if ($(this).val().length === 0 && $.trim($(this).val()) === '') {
	    empty = true;
        }
    });

    if(!$("#registration-form #tupp:checked").length > 0) {
	empty = true;
    } 

    if (empty) {
        $('.btn-submit').attr('disabled', 'disabled');
    } else {
        $('.btn-submit').removeAttr('disabled');
    }
});

$('#verification-form input:not([type="submit"])').on("change keyup blur mouseenter", function() {
    var empty = false;
    $('#verification-form input:not([type="submit"])').each(function() {
        if ($(this).val().length === 0 && $.trim($(this).val()) === '') {
	    empty = true;
        }
    });

    if (empty) {
        $('.btn-submit').attr('disabled', 'disabled');
    } else {
        $('.btn-submit').removeAttr('disabled');
    }
});
