$("#header-navigation").sticky({ topSpacing: 0 });   
                            
var form = $("#forgot-password-form");

/** VALIDATION RULES **/
form.validate({
    highlight: function(element, errorClass, validClass) {
        $(element).addClass(errorClass).removeClass(validClass);
        $(element).siblings("span").removeClass("fa-check-circle").addClass("fa-times-circle");
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass(errorClass).addClass(validClass);
        if ($(element).val() !== '') {
            $(element).siblings("span").removeClass("fa-times-circle").addClass("fa-check-circle");
        } else {
            $(element).siblings("span").removeClass("fa-check-circle fa-times-circle");
        }
    },         
    errorPlacement: function errorPlacement(error, element) { element.before(error); },
    rules: {                
        npassword: {
            minlength: 5,
        },
        cpassword: {
            minlength: 5,
            equalTo: "#npassword"
        }
    },
    submitHandler: function(form) {
        $.ajax({
            type: "POST",
            data: $("#forgot-password-form").serialize(),
            dataType: 'json',
            success: function (data) {
                //window.location.replace(data.url);
            }
        });

        $('#successModal').modal({backdrop: 'static'});
        $('#successModal').modal('show');
    }
});         

/** DISABLE SUBMIT BUTTON IF INPUT IS INCOMPLETE **/
$(document).ready(function() {
    var empty = false;
    $('input:not([type="submit"])').each(function() {
        if ($(this).val().length === 0 && $.trim($(this).val()) === '') {
            empty = true;
        }
    });

    if (empty) {
        $('.btn-submit').attr('disabled', 'disabled');
    } else {
        $('.btn-submit').removeAttr('disabled');
    }    
});

(function() {
    $('input:not([type="submit"])').on("change keyup blur mouseenter", function() {

        var empty = false;
        $('input:not([type="submit"])').each(function() {
            if ($(this).val().length === 0 && $.trim($(this).val()) === '') {
                empty = true;
            }
        });

        if (empty) {
            $('.btn-submit').attr('disabled', 'disabled');
        } else {
            $('.btn-submit').removeAttr('disabled');
        }
    });
})()
