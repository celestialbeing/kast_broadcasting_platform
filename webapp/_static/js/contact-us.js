var cform = $("#contact-form");

/** VALIDATION RULES **/
$.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[a-zA-Z]+(\s{0,1}[a-zA-Z-,. ])*$/i.test(value);
}, "Please put a valid name");

$.validator.addMethod("checksubject", function(value, element) {
    return this.optional(element) || /^[a-zA-Z0-9]+(\s{0,1}[a-zA-Z-,. ])*$/i.test(value);
}, "Please put a valid subject");

$.validator.addMethod("checkmessage", function(value, element) {
    return this.optional(element) || /^[a-zA-Z0-9]+(\s{0,1}[a-zA-Z-,. ])*$/i.test(value);
}, "Please put a valid message");

cform.validate({
    errorPlacement: function errorPlacement(error, element) { element.before(error); },
    rules: {
        name: {
            lettersonly: true
        },
	email: {
	    email: true
	},
	subject: {
	    checksubject: true
	},
	message: {
	    checkmessage: true
	},
    },
    messages: {
        email: {
            email: "Please put a valid email address"
        }
    },
    submitHandler: function(cform) {
        $.ajax({
            type: "POST",
            data: $("#contact-form").serialize(),
            dataType: 'json',
            success: function (data) {
            }
        });

        $('#successModal').modal({backdrop: "static"});
        $('#successModal').modal('show');
    }
});

/** DISABLE SUBMIT BUTTON IF INPUT IS INCOMPLETE **/
$(document).ready(function() {
    var empty = false;
    $('#contact-form input:not([type="submit"])').each(function() {
        if ($(this).val().length === 0 && $.trim($(this).val()) === '') {
            empty = true;
        }
    });

    var txt = $('#contact-form textarea').val();
    if (txt.length === 0 && $.trim(txt) === '') {
	empty = true;
    }

    if (empty) {
        $('#contact-form .btn-submit').attr('disabled', 'disabled');
    } else {
        $('#contact-form .btn-submit').removeAttr('disabled');
    }
});

(function() {
    $('#contact-form input:not([type="submit"]), #contact-form textarea').on("change keyup blur mouseenter", function() {
        var empty = false;
        $('#contact-form input:not([type="submit"])').each(function() {
            if ($(this).val().length === 0 && $.trim($(this).val()) === '') {
                empty = true;
            }
        });

        var txt = $('#contact-form textarea').val();
        if (txt.length === 0 && $.trim(txt) === '') {
	    empty = true;
        }

        if (empty) {
            $('#contact-form .btn-submit').attr('disabled', 'disabled');
        } else {
            $('#contact-form .btn-submit').removeAttr('disabled');
        }
    });
})()

