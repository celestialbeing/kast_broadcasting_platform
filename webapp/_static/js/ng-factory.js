app.factory('dataFactory', ['$http', function($http){

	var dataFactory = {};

	dataFactory.getContactGroups = function(){

		return $http.get('/static/js/sample_data.json');
	}

	return dataFactory;

}]);