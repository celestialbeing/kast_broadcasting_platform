$("#header-navigation").sticky({ topSpacing: 0 });

$(document).ready(function() {
    $("ul.trackAccordion").accordion({
        collapsible: true,
        active: false,
        heightStyle: "content",
     });

     $("ul.trackAccordion").accordion("option","icons",
        { 'header': 'ui-icon-plusthick', 'activeHeader': 'ui-icon-minusthick' });

     var status = $("#account-status").html();

     if (status == "live") {
	//$("#live-nav").removeClass("hidden");
     } else {
	//$("#trial-nav").removeClass("hidden");
     }
});

function searchText() {
    var input, filter, ul, li, a, i, el;
    input = document.getElementById("inputString");
    filter = input.value.toUpperCase();
    ul = document.getElementById("myUL");
    a = ul.getElementsByTagName("a");

    for (i = 0; i < a.length; i++) {
        el = a[i];
        if (el.innerHTML.toUpperCase().indexOf(filter) > -1) {
            a[i].style.display = "block";
        } else {
            a[i].style.display = "none";
        }
    }
}
