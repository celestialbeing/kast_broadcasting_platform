/**
 * @file JS for Kast Campaign Dashboard
 * @author Aike Chan <achan@igateway.ph>
 */

/** 
 * Sticky Navigation Bar 
 */
$("#header-navigation").sticky({ topSpacing: 0 });

/** 
 * Initialize dashboard datatable plugin 
 */

var table = $('#dashboard-table').DataTable({
    "scrollX": true,
    "pageLength": 10,
    "paging": true,
    scrollCollapse: true,
    conditionalPaging: {
        style: 'fade',
        speed: 50 // optional
    },
    "processing": true,
    "serverSide": true,
    //"ajax": "/dashboard/datatable/",
    "ajax": {
        "url": "/dashboard/datatable/",
        "contentType": "application/json",
        "type": "POST",
        "data": function ( d ) {

            var param_date = $("#date").val();
            var param_text = $("#textSearch").val();
            var filter_by = $("#filter_by").val();

            /** 
             * Change columns searchable status based on filter by
             *
             * d.columns[1] - campaign name column
             * d.columns[2] - campaign schedule column
             * d.columns[3] - Date created column
             * d.columns[8] - campaign base column
             */ 

            for(var i = 1; i <= 16; i++){

                d.columns[i]['searchable'] = false;

                if(filter_by == "campaign_name" && i == 1){
                    d.columns[1]['searchable'] = true;
                }

                if(filter_by == "campaign_schedule" && i == 2){
                    d.columns[2]['searchable'] = true;
                }

                if(filter_by == "created" && i == 3){
                    d.columns[3]['searchable'] = true;
                }

                if(filter_by == "campaign_base" && i == 8){
                    d.columns[8]['searchable'] = true;
                }

                if(filter_by == "all"){
                    d.columns[i]['searchable'] = true;
                    d.columns[9]['searchable'] = false;
                    d.columns[11]['searchable'] = false;
                }
            }

            /** 
             * Pass value from search input to the d.search['value'] parameter 
             */

            if(param_date){
                d.search['value'] = $("#date").val();
            }
            else{
                d.search['value'] = $("#textSearch").val();    
            }
            
          return JSON.stringify( d );
        }
    },
    "order": [[ 0, "desc" ]],
    dom: "r<'row'<'col-sm-6 custom-filters'f><'col-sm-6 filter-rows'l>>tp",
    "pagingType": "full_numbers",
    "language": {
        "lengthMenu": "Rows<br> _MENU_",
        "infoEmpty": "No records available",
        "search": "",
        "paginate": {
            "previous": "Previous",
            "next": "Next",
            "first": "First",
            "last": "Last"
        },
        "processing": '<div id="loadercontainer"><div id="customloader"></div><div id="campaignloader"><img src="/static/images/ajax-loader2.gif"></div></div>'
    },
    fixedColumns:   {
            leftColumns: 2,
            rightColumns: 6
    },
    "columnDefs": [
        {
            "targets": [0],
            "visible": false,
            "searchable": false,
            "orderable": true,
        },
        {
            "targets": [11],
            "orderable": false,
            "searchable": false
        },
        {
            "targets": [12, 13, 14, 15, 16],
            "visible": false,
            "orderable": false,
            "searchable": false
        }
    ],
    createdRow: function( row, data, dataIndex ) {

        //console.log(data);
        var broadcast_id = data['DT_RowId']; 
        var sched_date =data[2];
        var sender_id = data[13] || "-"; 
        var sender_name = data[14] || "-";
        var cmessage = data[12] || "-"; 
        var campaign_name = data[1];
        var campaign_id = data[0];
        var is_draft = data[16]; 
        var audience = data[8] || "-";
        var campaign_uid = data[15];
        var on_failed = data[7];
        var on_forwarded = data[5] || "-";
        var on_queue = data[4] || "-";
        var on_sent = data[6] || "-";
        var ref_code = data[10] || "-";

        /**
        * Set the campaign name attributes
        * 
        */

        $('td', row).eq(0).attr("id", "cname-"+broadcast_id);
        $('td', row).eq(0).attr("sched", sched_date);
        $('td', row).eq(0).attr("is_draft", is_draft);
        $('td', row).eq(0).attr("campaign_id", campaign_id);
        $('td', row).eq(0).attr("campaign_name", campaign_name);
        $('td', row).eq(0).attr("cmessage", cmessage);
        $('td', row).eq(0).attr("sender_id", sender_id);
        $('td', row).eq(0).attr("campaign_uid", campaign_uid);
        $('td', row).eq(0).attr("sender_id_text", sender_name);
        $('td', row).eq(0).attr("campaign_base", audience);

        /**
        * Override table status cell data
        * 
        */

        $('td', row).eq(3).html(on_queue);
        $('td', row).eq(4).html(on_forwarded);
        $('td', row).eq(5).html(on_sent);
        $('td', row).eq(7).html(audience);
        $('td', row).eq(9).html(ref_code);

        /**
        * Set the table status attributes
        * 
        */

        $('td', row).eq(3).attr("id", "oq-"+broadcast_id); //on queue
        $('td', row).eq(4).attr("id", "fw-"+broadcast_id); //on forwarded
        $('td', row).eq(5).attr("id", "sc-"+broadcast_id); //on sent
        $('td', row).eq(6).attr("id", "fl-"+broadcast_id); //on failed

        /**
        * Override failed status data based on the total failed count
        * 
        */

        var failed_text = '-';
        if(on_failed.count > 0){

            var failed_base = ""
            $.each(on_failed.base, function(index, value){
                failed_base = failed_base + value.msisdn + "#" +value.description+ ", "
            });

            var failed_desc = ""
            $.each(on_failed.base, function(index, value){
                failed_desc = failed_desc + value.description + ", "
            });

            failed_desc = failed_desc.substring(0, failed_desc.length - 1);
            failed_base = failed_base.substring(0, failed_base.length - 1);

            failed_text = '<a id="campaign-'+campaign_id+'" href="javascript:void(0)" data-campaign-name="'+campaign_name+'" data-sender-id="'+sender_id+'" data-message="'+cmessage+'" data-schedule="'+sched_date+'" data-broadcast-id="'+broadcast_id+'" data-failed="'+failed_base+'" data-campaign-id="'+campaign_id+'" data-failed-desc="'+failed_desc+'" style="color: #cf6162;text-decoration: underline;" class="btn-failed"><span>'+on_failed.count+'</span></a>';
        }

        $('td', row).eq(6).html(failed_text);

        /**
        * Override table buttons based on status
        * 
        */

        var btn_edit = "btn-disabled";
        var btn_delete = "btn-disabled";
        var btn_report = "btn-disabled";

        if(is_draft !== true && broadcast_id != null && on_forwarded != "-"){

            btn_report = "";
        }

        if(is_draft === true && broadcast_id == null){

            btn_edit = "";
            btn_delete = "";
        }

        /** ACTION ICONS AVAILABILITY BASED ON STATUS VALUE **/
        $('td', row).eq(10).html('<a data-toggle="modal" href="javascript:void(0)" class="btn-dashboard btn-summary" data-row-campaign="'+campaign_name+'" data-row-audience="'+audience+'" data-row-message="'+cmessage+'" data-row-senderid="'+sender_name+'" data-row-schedule="'+sched_date+'" title="View Summary"><span class="fa fa-eye"></span></a>&nbsp;&nbsp;<a href="/broadcasts/live/?q='+campaign_uid+'" class="btn-dashboard btn-edit '+btn_edit+'" title="Edit"><span class="fa fa-edit"></span></a>&nbsp;&nbsp;<a data-toggle="modal" href="javascript:void(0)" class="btn-dashboard btn-delete '+btn_delete+'" data-row-id="'+campaign_uid+'" data-row-campaign="'+campaign_name+'" title="Delete"><span class="fa fa-trash-o"></span></a>&nbsp;&nbsp;<a href="/broadcasts/get_report/'+broadcast_id+'" class="btn-dashboard btn-request '+btn_report+'" title="Request for a Report"><span class="fa fa-bar-chart"></span></a>');
    }
});

/**
* starting websocket connection
* Iterate websocket for retry connection 
*/
var ws_connected = false;

while(ws_connected==false){
    try{
    /** CREATE WEBSOCKET CONNECTION **/
        var socket = new WebSocket('ws://' + window.location.host + '/statuses/');
        ws_connected = true;
    }
    catch(error){
        console.log("reconnecting to reports");
        ws_connected=false;
    }
}

socket.onopen = function open() {
    console.log('WebSockets connection created.');
};

/**
* Override table row statuses(sent, failed, forwarded, on queue) coming from websocket api
* @param {json} event - return data of websocket api containing campaign information
*/
function message_rcvr(event) {

    var socket_data = JSON.parse(event.data);

    var bcast_id = socket_data.broadcast_id;
    //console.log(bcast_id);
    if(bcast_id != "null"){

        var onqueue = socket_data.onqueue;
        var forwarded = socket_data.forwarded;
        var sent = socket_data.sent;
        var failed = socket_data.failed;

        var rowIdx = table.row("#"+bcast_id).index();

        var sender_id = $("#cname-"+bcast_id).attr('sender_id');
        var sender_id_text = $("#cname-"+bcast_id).attr('sender_id_text');
        var cmessage = $("#cname-"+bcast_id).attr('cmessage');
        var campaign_name = $("#cname-"+bcast_id).attr('campaign_name');
        var campaign_id = $("#cname-"+bcast_id).attr('campaign_id');
        var is_draft = $("#cname-"+bcast_id).attr('is_draft');
        var campaign_uid = $("#cname-"+bcast_id).attr('campaign_uid');
        var sched = new Date($("#cname-"+bcast_id).attr('sched'));

        var schedtime = sched.getTime() / 1000;
        var sched_date_text = $("#cname-"+bcast_id).attr('sched');

        audience = $("#cname-"+bcast_id).attr('campaign_base'); //table.cell( rowIdx, 8).data(); 

        var count_onqueue = $("#oq-"+bcast_id).html(); //table.cell( rowIdx, 4).data(); 
        
        if(onqueue.count != null && onqueue.count > 0){

            count_onqueue = onqueue.count;
            $("#oq-"+bcast_id).html(count_onqueue);
            //table.cell( rowIdx, 4).data(count_onqueue);
        }

        var count_forwarded = $("#fw-"+bcast_id).html(); //table.cell( rowIdx, 5).data();  
        if(forwarded.count != null && forwarded.count > 0){

            count_forwarded = forwarded.count;
            $("#fw-"+bcast_id).html(count_forwarded); 
            //table.cell( rowIdx, 5).data(count_forwarded);
        }

        var count_sent = $("#sc-"+bcast_id).html(); //table.cell( rowIdx, 6).data();  
        if(sent.count != null && sent.count > 0){

            count_sent = sent.count;
            $("#sc-"+bcast_id).html(count_sent); 
            //table.cell( rowIdx, 6).data(count_sent);
        }

        var failed_text = $("#fl-"+bcast_id).html(); //table.cell( rowIdx, 7).data(); 
        console.log(failed.count);
        if(failed.count > 0){

            var failed_base = ""
            $.each(failed.base, function(index, value){
                failed_base = failed_base + value.msisdn + "#" +value.description+ ", "
            });

            var failed_desc = ""
            $.each(failed.base, function(index, value){
                failed_desc = failed_desc + value.description + ", "
            });

            failed_desc = failed_desc.substring(0, failed_desc.length - 1);
            failed_base = failed_base.substring(0, failed_base.length - 1);

            failed_text = '<a href="javascript:void(0)" style="color: #cf6162;text-decoration: underline;" class="btn-failed" id="campaign-'+campaign_id+'" data-campaign-id="'+campaign_id+'" data-campaign-name="'+campaign_name+'" data-sender-id="'+sender_id+'" data-message="'+cmessage+'" data-schedule="'+schedtime+'" data-broadcast-id="'+bcast_id+'" data-failed="'+failed_base+'" data-failed-desc="'+failed_desc+'"><span>'+failed.count+'</span></a>';
            $("#fl-"+bcast_id).html(failed_text); 
            //table.cell( rowIdx, 7).data(failed_text);
        }
        else{

            $("#fl-"+bcast_id).html("-");
        }

        if(count_forwarded > 0){

            table.cell( rowIdx, 11).data('<a data-toggle="modal" href="javascript:void(0)" class="btn-dashboard btn-summary" data-row-campaign="'+campaign_name+'" data-row-audience="'+audience+'" data-row-message="'+cmessage+'" data-row-senderid="'+sender_id_text+'" data-row-schedule="'+sched_date_text+'" title="View Summary"><span class="fa fa-eye"></span></a>&nbsp;&nbsp;<a href="/broadcasts/live/?q='+campaign_uid+'" class="btn-dashboard btn-edit btn-disabled" title="Edit"><span class="fa fa-edit"></span></a>&nbsp;&nbsp;<a data-toggle="modal" href="javascript:void(0)" class="btn-dashboard btn-delete btn-disabled" data-row-id="'+campaign_uid+'" data-row-campaign="'+campaign_name+'" title="Delete"><span class="fa fa-trash-o"></span></a>&nbsp;&nbsp;<a href="/broadcasts/get_report/'+bcast_id+'" class="btn-dashboard btn-request" title="Request for a Report"><span class="fa fa-bar-chart"></span></a>');
        }

        table.fixedColumns().update();
    }
}

/**
* Set 30 seconds interval to update all successfully scheduled broadcasts statuses
* 
*/

setInterval(function(){

    table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
        var data = this.data();

        setTimeout(function(){

            //console.log(data);
            var broadcast_id = data['DT_RowId'];
            var sched_date =data[2];

            var draft = data[16];

            if(sched_date != "-" && draft !== true){
                var schedule = Date.parse(sched_date);

                //console.log(sched_date);

                var sched_epoch = schedule.getTime() / 1000;

                var date_today = Date.now().setTimezone("PHT").getTime() / 1000;

                var date_minus = Date.now().setTimezone("PHT").add(-3).days().getTime() / 1000;

                if(date_minus < sched_epoch && sched_epoch <= date_today){

                    //console.log(broadcast_id);
                    /** send message to websocket
                    param : broadcast_id
                    type : int **/
                    socket.send(broadcast_id);
                    
                }
                else if(
                    sched_epoch <= date_today &&
                    (
                        data[4] == "-" || data[5] == "-" ||
                        (
                            data[6]=="-" && data[7]=="-"
                        )
                    )
                ){
                    /** send message to websocket
                    param : broadcast_id
                    type : int **/
                    socket.send(broadcast_id);
                }
            }

        }, 1000);
        socket.onmessage = function(event){
            message_rcvr(event);
        }
    });

}, 30000);


if (socket.readyState == WebSocket.OPEN) {
    socket.onopen();
}

/**
* Override datatable filters
* 
*/

$("#dashboard-table_filter").addClass("form-inline");
$("#dashboard-table_filter").find("label").remove();
$("#dashboard-table_filter").append('<div class="input-group"><input type="text" id="textSearch" class="form-control" placeholder="Search for..."><span class="input-group-btn"><button class="btn btn-default btn-blue btn-search-text" type="button" style="padding:.5em 1em;background:#62A6AE;">Go!</button></span></div>');

var filter_field = '<div class="filter-container form-inline"><div class="form-group"><label for="filter" class="filter-label">Filter by&nbsp;&nbsp; </label><select class="filter form-control" id="filter_by"><option value="all">All</option><option value="campaign_name">Campaign Name</option><option value="campaign_schedule">Schedule</option><option value="created">Date Created</option><option value="campaign_base">Campaign Base</option></select></div></div>';
var date_field = '<div id="date-container" class="dataTables_filter form-inline" style="margin-top:20px;"><div class="input-group"><input type="text" id="date" class="form-control datepicker" style="width: 205px;padding:6px 12px;" placeholder="Search for..."><span class="input-group-btn"><button class="btn btn-default btn-blue btn-search-text" type="button" style="padding:.5em 1em;background:#62A6AE;">Go!</button></span></div></div>';


$(".custom-filters #dashboard-table_filter").before($(filter_field));
$(".custom-filters #dashboard-table_filter").before($(date_field));

$("select[name='dashboard-table_length']").addClass('form-control');

$("#status-container, #date-container").hide();

$("#dashboard-table_wrapper").on("click", '.btn-summary', function() {
    var rowCampaignName         = $(this).data('row-campaign');
    var rowAudience         = $(this).data('row-audience');
    var rowMessage          = $(this).data('row-message');
    var rowSID          = $(this).data('row-senderid');
    var rowScheduleDate         = $(this).data('row-schedule');

    if(rowScheduleDate != "-"){

        var schedule = rowScheduleDate.split(" ");
        var date = schedule[0];
        var time = schedule[1];

        $(".modal-summary #cdate").html(date);
        $(".modal-summary #ctime").html(time);
    }

    $(".modal-summary #cname").html(rowCampaignName);
    $(".modal-summary #caudience").html(rowAudience);
    $(".modal-summary #cmessage").html(rowMessage);
    $(".modal-summary #csenderid").html(rowSID);

    $("#broadcast-summary").modal("show");

});

$("#dashboard-table_wrapper").on('click', '.btn-delete', function(){
    var rowCampaignID   = $(this).data('row-id');
    var rowCampaignName     = $(this).data('row-campaign');

    $("#deleteModal #modal-campaign-name").html(rowCampaignName);
    $("#deleteModal #delete-confirm").val(rowCampaignID);

    $("#deleteModal").modal({backdrop: 'static'});
    $("#deleteModal").modal("show"); 

    localStorage.setItem('successDelete', true);
});

/*check for successful delete*/
if(localStorage.getItem('successDelete')){

    $("#successModalDashboard").modal({backdrop: 'static'});
    $("#successModalDashboard").modal("show"); 

    localStorage.removeItem('successDelete');
}

$('#successModalDashboard').on('hidden.bs.modal', function (e) {

  localStorage.removeItem('successDelete');
  var successDelete = localStorage.getItem('successDelete');

  window.location = '/dashboard';

});

$('#deleteModal').on('hidden.bs.modal', function (e) {

  localStorage.removeItem('successDelete');

});

/** SEARCH BY FILTER **/
$(document.body).on('change', '.filter, #date', function(){

    var filter = $(".filter").val();

    $("#date").val("");
    $("#dashboard-table_filter").find("input").val("");

    if(filter == "campaign_schedule" || filter == "created") {
      $("#date-container").show();
      $("#dashboard-table_filter").hide();
    }
    else if(filter == "all" || filter == "campaign_base" || filter == "campaign_name"){
      $("#date-container").hide();
      $("#dashboard-table_filter").show();
    }

    //table.ajax.url( '/dashboard/datatable/' ).load();
});

$('.datepicker').datepicker({ 
    dateFormat: 'yy-mm-dd',
    onSelect: function(){

    }
});


/**
* Override Modal Failed mobile numbers container and failed description dropdown
* 
*/
$("#dashboard-table_wrapper").on('click', '.btn-failed', function(){

    var data_failed = $(this).data("failed");
    var broadcast_id = $(this).data("broadcast-id");
    var sender_id = $(this).data("sender-id");
    var message = $(this).data("message");
    var schedule = $(this).data("schedule");
    var campaign_name = $(this).data("campaign-name");
    var campaign_id = $(this).data("campaign-id");
    var data_failed_desc = $(this).data("failed-desc");

    var numbers = data_failed.split(",");

    var cfailed_desc = data_failed_desc.substring(0, data_failed_desc.length - 1);

    var failed_desc = cfailed_desc.split(", ");

    $(".list-failed").find("li").remove();

    $(".sub-list").parent().remove();

    var new_failed_desc = Array();
    var arr = ['x'];

    failed_lists = "";
    for(var i = 0; i < numbers.length; i++){
        details=numbers[i].trim().split('#');
        $(".list-failed").append('<li description="'+details[1]+'">'+details[0]+'</li>');

        failed_lists = failed_lists + details[0] + ",";
    }

    var cfailed_lists = failed_lists.substring(0, failed_lists.length - 2);

    $("#select_resend").find("option").remove();
    for(var i = 0; i < failed_desc.length; i++){

        new_failed_desc.push(failed_desc[i].trim());
    }

    var filteredArray = new_failed_desc.filter(function(item, pos){
      return new_failed_desc.indexOf(item)== pos; 
    });
    
    $("#select_resend").append('<option value="all">All</option>');
    for(var i = 0; i < filteredArray.length; i++){

        $("#select_resend").append('<option value="'+filteredArray[i]+'">'+filteredArray[i]+'</option>');
    }

    var $bigList = $('#modalFailed .list-failed'), group;

    while((group = $bigList.find('li:lt(5)').remove()).length){
        ul = $('<ul/>').addClass('sub-list list-group').append(group);
        $("<div/>").addClass("col-md-3 col-sm-3").append(ul).appendTo("#modalFailed .panel-body");
        //$('<ul/>').addClass('sub-list list-group').append(group).appendTo('.right');
    }

    $(".list-failed").hide();

    $("#broadcast_id").val(broadcast_id);
    $("#contact_list").val(cfailed_lists);
    $("#message").val(message);
    $("#sender_id").val(sender_id);
    $("#schedule").val(schedule);
    $("#campaign_name").val(campaign_name);
    $("#campaign_id").val(campaign_id);
    $("#modalFailed").modal("show");

});


/**
* Override Modal Failed mobile numbers container based on failed description dropdown
* 
*/
$("#modalFailed").on('change', '#select_resend', function(){
    var campaign_id = $("#campaign_id").val();
    var failed = $("#campaign-"+campaign_id).attr("data-failed").split(',');
    
    var li_text = $(this).val();
    var new_arr = Array();
    $.each(failed, function(index,value){
        var value_arr = value.split('#');
        var desc = value_arr[1];

        if(li_text == desc){
            new_arr.push(value.trim());
        }
        else if(li_text == "all"){
            new_arr.push(value.trim());
        }
    });

    $(".list-failed").find("li").remove();
    $(".sub-list").parent().remove();

    new_lists = "";
    for(var i = 0; i < new_arr.length; i++){
        details=new_arr[i].trim().split('#');
        $(".list-failed").append('<li description="'+details[1]+'">'+details[0]+'</li>');

        new_lists = new_lists + details[0] + ",";
    }

    var cfailed = new_lists.substring(0, new_lists.length - 1);

    $("#contact_list").val(cfailed);

    var $bigList = $('#modalFailed .list-failed'), group;

    while((group = $bigList.find('li:lt(5)').remove()).length){
        ul = $('<ul/>').addClass('sub-list list-group').append(group);
        $("<div/>").addClass("col-md-3 col-sm-3").append(ul).appendTo("#modalFailed .panel-body");
        //$('<ul/>').addClass('sub-list list-group').append(group).appendTo('.right');
    }

    $(".list-failed").hide();
});

/**
* Load overlay on delete campaign
* 
*/
$("#delete-form").submit(function(){

    $.LoadingOverlay("show", {
        color: "rgba(255, 255, 255, 0.42)"
    });
});


$(".btn-resend-failed").click(function(){

    $.LoadingOverlay("show", {
        color: "rgba(255, 255, 255, 0.42)"
    });

    var data = {
        contact_list: $("#contact_list").val(),
        campaign_name: $("#campaign_name").val(),
        campaign_id: parseInt($("#campaign_id").val()),
        message: $("#message").val(),
        broadcast_id: parseInt($("#broadcast_id").val()),
        sender_id: parseInt($("#sender_id").val()),
        schedule: $("#schedule").val(),
        select_resend: $("#select_resend").val()
    }

    $("#modalFailed").modal('hide');
    $("#successResendBroadcast").find("button").attr("data-broadcast-id", $("#broadcast_id").val());
    $("#successResendBroadcast").modal('show');

    $.ajax({
        method: "POST",
        url: "/broadcasts/do_resend/",
        data: JSON.stringify(data),
        contentType: "application/json",
        headers: {'X-CsrfToken': csrftoken}
    }).done(function(msg) {

        var ref_id = msg.ref_id;
        $.LoadingOverlay("hide");
        $("#modalFailed").modal('hide');
        $("#successResendBroadcast").find("button").attr("data-broadcast-id", $("#broadcast_id").val());
        $("#successResendBroadcast").modal('show');

    }).fail(function(jqXHR, textStatus){
        
        $.LoadingOverlay("hide");  

        try{

            var error = jqXHR.responseText;
            var data_error = $.parseJSON(error);
            
            var error_text = "";

            error_text = data_error.error;

            $(".error-broadcast").html(error_text);
        }
        catch(error){

            $(".error-broadcast").html("Campaign broadcast failed. Please contact Rising Tide");
        }

        $(".alert-danger").show(0).delay(5000).hide(0);
        
    });
});


/**
* Disable failed button on selected broadcast from resend modal
*
*/
$('#successResendBroadcast').on('hidden.bs.modal', function (e) {

  var broadcast_id = $('#successResendBroadcast').find("button").data("broadcast-id");

  $("#fl-"+broadcast_id).find("a").css({'pointer-events': 'none', 'cursor': 'default', 'opacity': '0.5'});

});

/**
* Reload datatable from search button
* 
*/
$("#dashboard-table_wrapper").on('click', '.btn-search-text', function(){

    table.ajax.url( '/dashboard/datatable/' ).load();
});

$('#textSearch').keyup(function(event){

    if(event.keyCode == 13){

        $(this).blur();
        table.ajax.url( '/dashboard/datatable/' ).load();
    }

});

/** KILL DEBUGGER
$(document).on("contextmenu",function(e){        
   e.preventDefault();
});

$(document).keydown(function(event){
    if(event.keyCode==123) {
        return false;
    } else if(event.ctrlKey && event.shiftKey && event.keyCode==73) {        
      return false;  //Prevent from ctrl+shift+i
    }
});
**/
