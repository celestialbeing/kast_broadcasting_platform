//var unicodeWord = XRegExp('\\p{InBasic_Latin}+$');
var unicodeTest = /^([\x00-\x7F]|([\xC2-\xDF]|\xE0[\xA0-\xBF]|\xED[\x80-\x9F]|(|[\xE1-\xEC]|[\xEE-\xEF]|\xF0[\x90-\xBF]|\xF4[\x80-\x8F]|[\xF1-\xF3][\x80-\xBF])[\x80-\xBF])[\x80-\xBF])*$/;
var mobile_check = /^\+?(09|639|9)\d{9}$/;
var alpha_check = /^(?![0-9]*$)[a-zA-Z0-9\-\s\'\.\_]+$/;
var html_tags_check = /^<(.|\n)*?>/g;
var hours, minutes, seconds, interval_hours;

/**
* Sort nested array strings alphabetically needed for javascript sort function
* 
*/
function stringSort(string_a, string_b){
    if(string_a['name'] < string_b['name']){
        return -1;
    }
    if(string_a['name'] > string_b['name']){
        return 1;
    }
    return 0;
}

/**
* Format whole numbers with comma delimited
* @param {integer} val - return formatted numbers
*/
function commaSeparateNumber(val){

    while (/(\d+)(\d{3})/.test(val.toString())){
      val = val.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
    }
 
    return val;
}

/**
* Get current datetime needed for timepicker disabled time
* @param {string} strTime - return time value
*@param {string} interval_hours - converted military time from current datetime
*
*/
function getTime(){

    var currentdate = Date.now().setTimezone("PHT").addMinutes(15);
    
    hours = currentdate.getHours();
    minutes = currentdate.getMinutes();
    seconds = currentdate.getSeconds();

    var ampm = hours >= 12 ? 'pm' : 'am';
    
    hours = (hours % 12);
    hours = hours;
    hours = (hours) ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;

    var time = hours+":"+minutes+":"+seconds;

    interval_hours = currentdate.toString("HH:mm");

    var strTime = time + ampm;

    return strTime;

}
  

$("#menu-toggler").click(function(e) {
    e.preventDefault();
    $(".wrapper").toggleClass("toggled");
});


/**
* Toggle complete title
* 
*/
$(".btn-tooltip").click(function(){

    var title = $(this).attr('title');
    var orig_title = $(this).html();
    $(this).attr('title', orig_title);
    $(this).html(title);
});

/**
* Function for disabling next button of jquery steps wizard
* 
*/
function disableWizardNext(){

    $('.actions > ul > li:nth-child(2) > a').attr('class', 'btn btn-default');
    $('.actions > ul > li:nth-child(2) > a').attr('disabled', 'disabled');
    $('.actions > ul > li:nth-child(2) > a').attr('href', '#');
    $('.actions > ul > li:nth-child(2)').addClass('disabled');
}

/**
* Function for enabling next button of jquery steps wizard
* 
*/
function enableWizardNext(){

    $('.actions > ul > li:nth-child(2) > a').removeAttr('class');
    $('.actions > ul > li:nth-child(2) > a').removeAttr('disabled');
    $('.actions > ul > li:nth-child(2) > a').attr('href', '#next'); 
    $('.actions > ul > li:nth-child(2)').removeClass('disabled');
}

/**
* Function for enabling finish button of jquery steps wizard
* 
*/
function enableFinishWizard(){

    $('.actions > ul > li:nth-child(3)').css('display', 'block');
    $('.actions > ul > li:nth-child(3) > a').attr('class', 'btn btn-default');
    $('.actions > ul > li:nth-child(3) > a').removeAttr('disabled');
    $('.actions > ul > li:nth-child(3) > a').attr('href', '#finish');
    $('.actions > ul > li:nth-child(3)').removeClass('disabled');
}

/**
* Function for disabling finish button of jquery steps wizard
* 
*/
function disableFinishWizard(){

    $('.actions > ul > li:nth-child(3)').css('display', 'block');
    $('.actions > ul > li:nth-child(3) > a').attr('class', 'btn btn-default');
    $('.actions > ul > li:nth-child(3) > a').attr('disabled', 'disabled');
    $('.actions > ul > li:nth-child(3) > a').attr('href', '#');
    $('.actions > ul > li:nth-child(3)').addClass('disabled');
}

// using jQuery to get document cookie
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

/*set up tooltip for clipboard plugin*/
function setTooltip(btn, message) {
  $(btn).tooltip('hide')
    .attr('data-original-title', message)
    .tooltip('show');
}

/*hide tooltip for clipboard plugin*/
function hideTooltip(btn) {
  setTimeout(function() {
    $(btn).tooltip('hide');
  }, 1000);
}
