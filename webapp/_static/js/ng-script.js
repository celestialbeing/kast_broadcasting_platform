var app = angular.module('app', ['ngRoute', 'ngSanitize']);

app.config(function($interpolateProvider) {
  $interpolateProvider.startSymbol('{[{');
  $interpolateProvider.endSymbol('}]}');
});

app.value('HTMLIZE_CONVERSIONS', [
    { expr: /\n+?/g, value: '<br>' }
  ]);

app.filter('htmlize', function(HTMLIZE_CONVERSIONS) {
    return function(string) {
      return HTMLIZE_CONVERSIONS.reduce(function(result, conversion) {
        return result.replace(conversion.expr, conversion.value);
      }, string || '');
    };
});

