app.directive('wizard', [function() {
    return {
        restrict: 'EA',
        scope: {
            stepChanging: '='
        },
        compile: function(element, attr) {
            element.steps({
                bodyTag: attr.bodyTag,
                transitionEffect: "slideLeft",
                autoFocus: true,
                titleTemplate: '<div style="width:100%;"><span class="steps-index">#index#</span></div><span class="steps-text">#title#</span>',
                labels: {
                    next: "Next",
                    previous: "Back",
                    finish: 'Next',
                    loading: "Loading ..."
                },
                autoFocus: true,
                enableContentCache: false,
                preloadContent: true,
                saveState: true,
                startIndex: 0,
                onInit: function (event, currentIndex) {

                    $("#date_from, #date_to").datepicker();

                    $("#feedback-dashboard").show();

                    $(".btn-back").click(function(){
                         parent.$.fancybox.close();
                    });


                    return true;
                },
                onFinishing: function (event, currentIndex) 
                { 

                    $.fancybox( '#promos-summary',{
                        width: 500,
                        height: 500,
                        autoHeight: false,
                        autoResize: false,
                        fitToView: false,
                        autoSize: false, 
                        closeBtn: false
                    });

                    return false; 
                }
            });

            return {
                //pre-link
                pre:function() {},
                //post-link
                post: function(scope, element) {
                    element.on('stepChanging', scope.stepChanging);
                }
            }
        }
    };
}]);

app.controller('Ctrl', ['$scope', function($scope) {
    $scope.stepChanging = function() {
  
    };

    $scope.save = function(){

        console.log($scope.model);
    }
}]);