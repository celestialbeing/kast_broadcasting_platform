var form = $("#contacts-container");
var file = "";
var contactGroups = [];
var strContact = "";
var error_num = false, i  = 0, stopAnimate = false, originalText = $("#loading-text").text();

/** VALIDATION RULES **/
$.validator.addMethod("utfonly", function(value, element) {
    return unicodeTest.test(value);
}, "We noticed you used unsupported characters");

$.validator.addMethod("NoWhiteSpaceAtBeginn", function(value, element) {
    var newVal = value.trim();
    return /^[^\s].*/.test(newVal);
  });

form.validate({
    errorPlacement: function errorPlacement(error, element) {
        //element.before(error);
        if(element.attr("name") == "contacts_name"){
            error.insertAfter(".contact-group-wrapper");
        }
    },
    rules: {
        contacts_name: {
            required: true,
            maxlength: 50,
            utfonly: true,
            NoWhiteSpaceAtBeginn: true
        }
    },
    messages: {
        contacts_name: {
            required: "Contact group name is required",
            NoWhiteSpaceAtBeginn: "Contact group name must not begin with a whitespace",
            maxlength: 'Please enter no more than 50 characters'
        }
    }
});

app.directive('wizard', [function() {
    return {
        restrict: 'EA',
        scope: {
            stepChanging: '='
        },
        compile: function(element, attr) {
            element.steps({
                bodyTag: attr.bodyTag,
                transitionEffect: "slideLeft",
                autoFocus: true,
                titleTemplate: '<div style="width:100%;"><span class="steps-index">#index#</span></div><span class="steps-text">#title#</span>',
                labels: {
                    next: "NEXT",
                    previous: "PREVIOUS",
                    finish: 'SAVE GROUP',
                    loading: "Loading ..."
                },
                autoFocus: true,
                enableContentCache: false,
                preloadContent: true,
                saveState: true,
                startIndex: 0,
                onInit: function (event, currentIndex) {

                    $("#contacts-container").show();

                    $("#contacts-container .content").css('min-height', '10em');

                    $('.actions > ul > li:first-child').attr('style', 'display:none');
                    disableWizardNext();

                    $("#contacts_name").keyup(function(event){

                        var group_name = $(this).val();

                        var len = group_name.trim().length;

                        if(len == 0){

                            disableWizardNext();
                        }
                        else{

                            enableWizardNext();

                        }

                        if(event.keyCode == 8){

                            $("#contacts_name-error").remove();
                            $("#contacts_name").removeClass('error');
                        }

                    });

                    $('#contacts-container').on('change', '#upload_file', function(){
                        file = this.files[0];
                        var ext = file.name.split('.').pop();

                        if(ext != 'xlsx' && ext != 'xls' && ext != 'csv'){

                            disableFinishWizard();
                            $(".file-label").show();
                            $(".file-text").html(file.name);
                            $(".file-error").html("Invalid file extension");
                        }
                        else{

                            $(".file-label").show();
                            $(".file-text").html(file.name);
                            $(".file-error").html("");

                            enableFinishWizard();
                        }
                    });

                    $(".remove-upload").click(function(){

                        $(".file-label").hide();
                        $(".file-text").html("");
                        $("#upload_file").val("");
                        $(".file-error").html("");
                        file = "";
                        disableFinishWizard();
                    });

                    $(".btn-download-invalid").click(function(){

                        var download_url = $(this).data('download');

                        window.location = download_url;

                    });

                    $("input[name=contact_number]").keyup(function(event){

                        var contact = $(this).val().trim();

                        if(event.keyCode == 8){

                            $(".contact-group-error").html("");
                        }

                        if(event.keyCode == 13){

                            $(".btn-add-contact").click();
                        }

                    });

                    $(".btn-add-contact").click(function(){

                        var contact = $("input[name=contact_number]").val();

                        $(".btn-add-contact").removeAttr('disabled');
                        $(".contact-group-error").html("");

                        if(contact.match(mobile_check) != null){

                            $(".contact-group-error").html("");

                            $.ajax({
                                method: "POST",
                                    url: "/broadcasts/get_franchise/",
                                data: '{"phonenumber":"'+contact+'"}',
                                    contentType: "application/json",
                                headers: {'X-CsrfToken': csrftoken}
                                }).done(function(msg) {

                                    console.log(msg);

                                    if(jQuery.isEmptyObject(msg)){
                                        $(".contact-group-error").html("");
                                        $(".contact-group-error").append('<label class="error">Mobile number is not yet franchised</label>');
                                    }
                                    else{
                                        $(".contact-group-error").html("");
                                        var mobile = msg.number.substr(msg.number.length - 12);
                                        addtoGroup(mobile);
                                    }

                                });
                        }
                        else{
                            error_num = true;
                            $(".contact-group-error").html("");
                            $(".contact-group-error").append('<label class="error">Invalid contact number</label>');
                        }

                    });

                    $("#contacts-container").on('click', '.btn-remove', function(){
                        var contact = $(this).data('mobile');
                        $(".btn-add-contact").removeAttr('disabled');
                        $(".contact-group-error").html("");
                        for (var i = 0; i < contactGroups.length; i++)
                        {
                            if (contactGroups[i] == contact)
                            {
                                contactGroups.splice(i, 1);
                                break;
                            }
                        }
                        populateContactGroups();
                    });

                    function addtoGroup(contact){

                        if(contactGroups.includes(contact)){
                            error_num = true;
                            $(".contact-group-error").html("");
                            $(".contact-group-error").append('<label class="error">You must enter unique mobile number.</label>');
                        }
                        else{
                            contactGroups.push(contact);
                            populateContactGroups();
                        }

                    }
                    function populateContactGroups(){
                        $(".no-contacts").hide();
                        $(".with-contacts").show();
                        $(".with-contacts .list-group").find("li").remove();
                        $("#contact_number").val("");
                        strContact = "";
                        for(var i in contactGroups){
                            var lists = '<li class="list-group-item">'+
                                        '<span class="contact-text">'+contactGroups[i]+'</span>'+
                                        '<button type="button" class="btn-remove" data-mobile="'+contactGroups[i]+'"><img src="/static/images/close-btn.png"></button>'+
                                        '</li>';
                            $(".with-contacts .list-group").append(lists);
                            strContact += contactGroups[i]+', ';
                        }

                        if(contactGroups.length > 0){

                            enableFinishWizard();
                        }
                        else{

                            disableFinishWizard();
                        }

                        strContact = strContact.substr(0, strContact.length - 2);
                        $("#contacts_form").val(strContact);
                        $(".total-counter .counter").html(contactGroups.length);
                    }

                    $("input[name=choose_method]").click(function(){

                        if($(this).is(":checked")){

                            element.steps('next');
                            enableWizardNext();
                            $('.actions > ul > li:nth-child(2)').css('display', 'none');
                        }
                    });

                    return true;
                },
                onStepChanging: function(event, currentIndex, newIndex){

                    if (newIndex < currentIndex) {

                        enableWizardNext();
                        $('.actions > ul > li:nth-child(3)').css('display', 'none');
                        return true;
                    }
                    else{

                        form.validate().settings.ignore = ":disabled,:hidden";
                        return form.valid();
                    }

                },
                onStepChanged: function (event, currentIndex, priorIndex){
                    if (currentIndex > 0) {
                        $('.actions > ul > li:first-child').attr('style', '');

                        if(currentIndex == 2){

                            if($("input[name=choose_method]:checked").val() == "upload_method"){

                                $("#upload_container").show();
                                $("#type_container").hide();
                            }
                            else{
                                $("#upload_container").hide();
                                $("#type_container").show();
                            }
                        }


                    } else {

                        $('.actions > ul > li:first-child').attr('style', 'display:none');
                    }

                    //for step1
                    if(currentIndex == 0){

                        $("#contacts-container .content").css('min-height', '10em');
                    }
                    else if(currentIndex == 1){

                        $("#contacts-container .content").css('min-height', '20em');
                    }
                    else{

                        if($("input[name=choose_method]:checked").val() == "upload_method"){

                            $("#contacts-container .content").css('min-height', '26em');
                        }
                        else{

                            $("#contacts-container .content").css('min-height', '25em');
                        }

                    }

                    if(currentIndex == 1){

                        if($("input[name=choose_method]").is(":checked")){

                            enableWizardNext();
                            return true;
                        }
                        else{
                            disableWizardNext();
                            return false;
                        }
                    }

                    if(currentIndex == 2){

                        if($("input[name=choose_method]:checked").val() == "upload_method"){

                            $("#contacts_form").val("");

                            if(file == ""){

                                disableFinishWizard();
                            }
                            else{
                                enableFinishWizard();
                            }
                        }
                        else if($("input[name=choose_method]:checked").val() == "type_method"){

                            file = "";

                            if($("#contacts_form").val() == ""){

                                disableFinishWizard();
                            }
                            else{
                                enableFinishWizard();
                            }
                        }
                    }
                },
                onFinishing: function (event, currentIndex)
                {
                    //if(form.validate().numberOfInvalids() > 0 || $("input[name=schedule_date]").val() == "" || $("input[name=schedule_time]").val() == ""){
                        return true;
                },
                onFinished: function (event, currentIndex){

                    var fd = new FormData();
                    var error = false;
                    var error_type = "";

                    if($("input[name=choose_method]:checked").val() == "upload_method"){

                        if(file == ""){

                            error = true;
                            error_type = "upload";
                        }
                        else{
                            error = false;
                        }
                    }
                    else if($("input[name=choose_method]:checked").val() == "type_method"){

                        if($("#contacts_form").val() == ""){

                            error = true;
                            error_type = "type";
                        }
                        else{
                            error = false;
                        }
                    }

                    $(".type-error").html("");
                    $(".file-error").html("");

                    if(error){

                        if(error_type == "type"){

                            $(".type-error").html("Please add your contact(s) manually");
                            return false;
                        }
                        else{
                            $(".file-error").html("Please upload your contacts");
                            return false;
                        }
                    }
                    else{
                        var count = 10;
                        var customElement = $("<div>", {
                            id      : "countdown",
                            text    : count+'%'
                        });

                        $.LoadingOverlay("show", {
                            image   : "",
                            color   : "rgba(0, 0, 0, 0.42)",
                            custom  : customElement
                        });

                        var interval = setInterval(function(){
                            count += 5;
                            customElement.html(count+'%<p id="loading-text">Please wait</p>');
                            $("#loading-text").append("..");
                            if (count > 90) {
                                clearInterval(interval);
                                customElement.html(count+'%<p id="loading-text">Please wait</p>');
                                return;
                            }

                        }, 500);

                        setInterval(function() {

                            if(stopAnimate === false){

                                $("#loading-text").append(".");
                            }
                            else{

                                $("#loading-text").append("");
                            }
                        
                            i++;
                            
                            if(i == 4)
                            {
                                $("#loading-text").html("Please wait");
                                i = 0;
                            }

                        }, 500);

                        fd.append('contacts_file', file);

                        var other_data = $('#contacts-container').serializeArray();
                        $.each(other_data, function(key, input){
                            fd.append(input.name, input.value);
                        });

                        $.ajax({
                            url: '/contacts/add_contacts/',
                            data: fd,
                            contentType: false,
                            processData: false,
                            type: 'POST'
                        }).done(function(data) {
                            clearInterval(interval);
                            customElement.text('100%');
                            $.LoadingOverlay("hide");

                            //remove session storage data
                            sessionStorage.removeItem('valid_contacts');

                            if(data.code){
                                //error code 008 invalid entries
                                if(data.code == '008'){

                                    $("#contact-group-warning").find("#num-invalid").html(data.invalid_count);
                                    $("#contact-group-warning").find("#num-valid").html(data.valid_count);
                                    $(".btn-download-invalid").attr('href', data.invalid_contacts);

                                    //save to web session storage
                                    sessionStorage.setItem('valid_contacts', data.valid_contacts); //, JSON.stringify(data.valid_contacts));
                                    sessionStorage.setItem('valid_count', data.valid_count);

                                    $("#contact-group-warning").modal({backdrop: 'static',
                                                keyboard: false});
                                    return false;
                                }
                            }
                            else{

                                $("#save-success").find('.base-file').html($('.file-text').html());
                                $("#save-success").find('.contact-group').html($("#contacts_name").val());
                                $("#save-success").find('.contact-base-count').html(data.valid_count);

                                if($("input[name=choose_method]:checked").val() == "type_method"){

                                    $(".base-type").hide();
                                }
                                else{

                                    $(".base-type").show();
                                }

                                $("#save-success").modal({backdrop: 'static',
                                                keyboard: false});
                                    return false;
                            }
                        }).fail(function(jqXHR, textStatus){
                            var error = jqXHR.responseText;

                            clearInterval(interval);
                            customElement.text('100%');
                            $.LoadingOverlay("hide");
                            try{
                                var data_error = $.parseJSON(error);


                                if(data_error.code == '009'){
                                    //file size exceeded
                                    $(".file-error").html(data_error.error);
                                }
                            }
                            catch(err){
                                $(".file-error").html("Upload failed. Please contact Risingtide");
                            }

                        });

                        return true;
                    }
                }
            });
            return {
                //pre-link
                pre:function() {},
                //post-link
                post: function(scope, element) {
                    element.on('stepChanging', scope.stepChanging);
                }
            }
        }
    };
}]);

$(".btn-proceed-upload").click(function(){

    $("#contact-group-warning").hide();

    var count = 10;
    var customElement = $("<div>", {
        id      : "countdown",
        text    : count+'%'
    });

    $.LoadingOverlay("show", {
        image   : "",
        color   : "rgba(0, 0, 0, 0.42)",
        custom  : customElement
    });

    var interval = setInterval(function(){
        count += 5;
        customElement.html(count+'%<p id="loading-text">Please wait</p>');
        $("#loading-text").append("..");
        if (count > 90) {
            clearInterval(interval);
            customElement.html(count+'%<p id="loading-text">Please wait</p>');
            return;
        }

    }, 500);

    setInterval(function() {

        if(stopAnimate === false){

            $("#loading-text").append(".");
        }
        else{

            $("#loading-text").append("");
        }
    
        i++;
        
        if(i == 4)
        {
            $("#loading-text").html("Please wait");
            i = 0;
        }

    }, 500);

    setInterval(function() {

        if(stopAnimate === false){

            $("#loading-text").append(".");
        }
        else{

            $("#loading-text").append("");
        }
    
        i++;
        
        if(i == 4)
        {
            $("#loading-text").html("Please wait");
            i = 0;
        }

    }, 500);

    var data ={
        contacts_name: $("#contacts_name").val(),
        valid_contacts: sessionStorage.valid_contacts, //JSON.parse(sessionStorage.valid_contacts),
        valid_count: sessionStorage.valid_count
    }

    $.ajax({
        url: '/contacts/proceed_upload/',
        method: "POST",
        data: JSON.stringify(data),
        contentType: "application/json",
        headers: {'X-CsrfToken': csrftoken},
    }).done(function(data) {
        clearInterval(interval);
        customElement.text('100%');
        $.LoadingOverlay("hide");

        $("#save-success").find('.base-file').html($('.file-text').html());
        $("#save-success").find('.contact-group').html($("#contacts_name").val());
        $("#save-success").find('.contact-base-count').html(data.valid_count);

        if($("input[name=choose_method]:checked").val() == "type_method"){

            $(".base-type").hide();
        }
        else{

            $(".base-type").show();
        }

        $("#save-success").modal({backdrop: 'static',
                        keyboard: false});

        //remove session storage data
        sessionStorage.removeItem('valid_contacts');
        sessionStorage.removeItem('valid_count');
        return false;

    }).fail(function(jqXHR, textStatus){

        clearInterval(interval);
        customElement.text('100%');
        $.LoadingOverlay("hide");

        try{
            var data_error = $.parseJSON(error);

            $("#upload-errors").html(data_error.error);
            $("#upload-errors").show(0).delay(3000).hide(0);
        }
        catch(err){
            $("#upload-errors").html("Upload failed. Please contact Risingtide");
            $("#upload-errors").show(0).delay(3000).hide(0);
        }

    });
});

app.controller('Ctrl', ['$scope', 'dataFactory', function($scope, dataFactory) {
    $scope.validLength = 160;
    $scope.increment = 1;
    $scope.contact_groups;
    $scope.status;
    $scope.sender_ids = [{"id": 1, "name": "VERIFY"}];
    $scope.default_sender_id = $scope.sender_ids[0];


}]);

/*counter=5;
size_li = $(".contacts-main li").size();

//load more
$('.contacts-main li:lt('+counter+')').show();
if(size_li <= 5){

    $('#loadMore').css('display', 'none');
}
else{
    $('#loadMore').css('display', 'block');
}*/

$('#loadMore').click(function () {
    /*counter= (counter+5 <= size_li) ? counter+5 : size_li;
    $('.contacts-main li:lt('+counter+')').show();*/

    $.LoadingOverlay("show", {
        color: "rgba(255, 255, 255, 0.42)"
    });

    var last_id = $(".contacts-main li").last().find("h4").data("id");
    var inputString = $("#inputString").val();

    var data = {
        last_id: last_id,
        search_string: inputString
    }

    $.ajax({
        url: "/contacts/load_more/",
        type: "post",
        contentType: "application/json",
        headers: {'X-CsrfToken': csrftoken},
        data: JSON.stringify(data),
        success: function(result){

            $.LoadingOverlay("hide");

            var data = result.contact_groups;

            for(val in data){
                
                $(".contacts-main li").last().after('<li class="list-group-item"><h4 title="'+data[val].name+'" data-id="'+data[val].id+'" class="btn-tooltip" style="cursor: pointer;">'+
                        data[val].name+'</h4><span class="content-body" style="font-size: 14px;">Base: '+commaSeparateNumber(data[val].base_count)+'</span></li>');
            }
        }   
    });
});

$(".contacts-wrapper").on("click", ".btn-search-groups", function(){

    var inputString = $("#inputString").val();

    $.LoadingOverlay("show", {
        color: "rgba(255, 255, 255, 0.42)"
    });

    var data = {
        search_string: inputString
    }

    $.ajax({
        url: "/contacts/search/",
        type: "post",
        contentType: "application/json",
        headers: {'X-CsrfToken': csrftoken},
        data: JSON.stringify(data),
        success: function(result){

            $.LoadingOverlay("hide");

            var data = result.contact_groups;

            $(".contacts-main ul").find("li").remove();

            for(val in data){
                
                $(".contacts-main ul").append('<li class="list-group-item"><h4 title="'+data[val].name+'" data-id="'+data[val].id+'" class="btn-tooltip" style="cursor: pointer;">'+
                        data[val].name+'</h4><span class="content-body" style="font-size: 14px;">Base: '+commaSeparateNumber(data[val].base_count)+'</span></li>');
            }
        }   
    });
});

$("#inputString").keyup(function(ev){

    var len = $(this).val().length;

    if(len <= 0 || ev.keyCode == 13){

        $(".btn-search-groups").click();
    }
});

function searchText() {
    var input, filter, ul, li, a, i;
    input = document.getElementById("inputString");
    filter = input.value.toUpperCase();
    ul = document.getElementById("myUL");
    li = ul.getElementsByTagName("li");

    if(filter.length == 0){
       $('.contact-groups li').hide();
       $('.contact-groups li:lt(5)').show();

       if(size_li > 5){

            $('#loadMore').css('display', 'block');
       }

        $(".search-result").html("");
    }

    var newVal = filter.trim();

    if(!/^[^\s].*/.test(newVal)){

        filter = null;
    }

    if(filter != null){

        var count = 0;
        for (i = 0; i < li.length; i++) {
            a = li[i].getElementsByTagName("h4")[0];
            if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "block";
                count++;
            } else {
                b = li[i].getElementsByClassName('content-body')[0];
                if(b.innerHTML.toUpperCase().indexOf(filter) > -1){
                    li[i].style.display = "block";
                    count++;
                }
                else{

                    li[i].style.display = "none";
                }
            }
        }

        var result_text = " result";
        if(count > 1){
            result_text = " results";
        }

        $(".search-result").html("<em>"+count + result_text+" found</em>");

        $('#loadMore').css('display', 'none');
    }
}
