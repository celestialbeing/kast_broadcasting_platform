$("#header-navigation").sticky({ topSpacing: 0 });

var form = $("#account-form");

/** COMPANY NAME CONVERT TO UPPERCASE **/
$(function() {
    $('#account-form #company, #account-form #name').keyup(function() {
        this.value = this.value.toLocaleUpperCase();
    });
});

/** VALIDATION RULES **/
$.validator.addMethod("lettersonly", function(value, element) {
    return this.optional(element) || /^[a-zA-Z ]+$/i.test(value);
}, "Please input a valid name");

$.validator.addMethod("alphanumeric", function(value, element) {
    return this.optional(element) || /^[a-zA-Z0-9 ]+$/i.test(value);
}, "Please input a valid company name");

$.validator.addMethod("franchiseformat", function(value, element) {
    return this.optional(element) || /^\+?(09|639)\d{9}$/i.test(value);
}, "Please input a valid mobile number format");

form.validate({
    highlight: function(element, errorClass, validClass) {
	$(element).addClass(errorClass).removeClass(validClass);
	$(element).siblings("span").removeClass("fa-check-circle").addClass("fa-times-circle");
    },
    unhighlight: function(element, errorClass, validClass) {
	$(element).removeClass(errorClass).addClass(validClass);
	if ($(element).val() !== '') {
	    $(element).siblings("span").removeClass("fa-times-circle").addClass("fa-check-circle");
        } else {
	    $(element).siblings("span").removeClass("fa-check-circle");
	}
    },
    errorPlacement: function errorPlacement(error, element) { element.before(error); },
    rules: {
        name: {
            lettersonly: true
        },
	company: {
	    alphanumeric: true
	},
        mobile: {
            number: true,
            minlength: 11,
	    maxlength: 12,
	    franchiseformat: true
        },
        email: {
            email: true
        },
        opassword: {
            minlength: 5
        },
        npassword: {
            minlength: 5
        },
        cpassword: {
            minlength: 5,
            equalTo: "#npassword"
        }
    },
    messages: {
        cpassword: {
            equalTo: "Your passwords do not match"
        }
    },
    submitHandler: function(form) {
	 $.ajax({
            type: "POST",
            data: $("#account-form").serialize(),
            success: function (data) {

                var res = $.parseJSON(data);
                
                if(res.status == "error"){

                    $('#errorModal').find(".details").html(res.message);

                    $('#errorModal').modal({backdrop: "static"});
                }
                else{
                    $('#successModal').modal({backdrop: "static"});
                }
            }
        });
    }
});

$(".btn-edit").click(function() {
    var btnLabel = $(this).text();

    if(!$(this).hasClass("link-disabled")) {

    if(btnLabel.indexOf("CHANGE PASSWORD") >= 0) {
        $(this).text("CANCEL").addClass("pull-right");
        $("#opassword").removeClass("hidden");
        $(this).parent().removeClass('col-sm-8').addClass('col-md-2 col-md-offset-1');
    } else {
        $(this).text("CHANGE PASSWORD").removeClass("pull-right");
        $("#opassword").addClass("hidden");
        $(this).parent().removeClass('col-md-2 col-md-offset-1').addClass('col-sm-8');
    }

    $(".passwords").slideToggle('fast', function(){

        $("#opassword-error").remove();
        $("#opassword").removeClass('error');
        $("#opassword").val("");

        $("#npassword-error").remove();
        $("#npassword").removeClass('error');
        $("#npassword").val("");

        $("#cpassword-error").remove();
        $("#cpassword").removeClass('error');
        $("#cpassword").val("");

    });
    }
});

$(".btn-ok").click(function() {
    setTimeout(location.reload.bind(location), 1000);
});

/** DISABLE SUBMIT BUTTON IF INPUT IS INCOMPLETE **/
$(document).ready(function() {
    var empty = false;
    $('input:not([type="submit"]):not([type="password"])').each(function() {
        if ($(this).val().length === 0 && $.trim($(this).val()) === '') {
            empty = true;
        }
    });

    if (empty) {
        $('.btn-submit').attr('disabled', 'disabled');
    } else {
        $('.btn-submit').removeAttr('disabled');
    }    
});

(function() {
    $('input:not([type="submit"]):not([type="password"])').on("change keyup blur mouseenter", function() {

        var empty = false;
        $('input:not([type="submit"]):not([type="password"])').each(function() {
            if ($(this).val().length === 0 && $.trim($(this).val()) === '') {
                empty = true;
            }
        });

        if (empty) {
            $('.btn-submit').attr('disabled', 'disabled');
        } else {
            $('.btn-submit').removeAttr('disabled');
        }
    });
})()

/** INTEGRATE PROFILE PAGE FRONT-END LOGIC **/
$(document).ready(function(){
    var status = $("#account-status").val();

    if (status == "live") {
    } else if (status == "processing") {
	$(".processing-message").removeClass('hidden');
	$(".registration-link").removeClass('hidden').addClass("link-disabled").attr("href", "javascript:void(0)");
	$(".btn-edit").removeClass("btn-edit").addClass("link-disabled");
	$("#account-form .btn-submit").addClass("hidden");
	
	$('input:not([type="submit"])').each(function(){
	    $(this).attr("disabled", true);
	});
    } else if (status == "trial" || status.length < 0) {
	$(".registration-link").removeClass("hidden");
    }
});
