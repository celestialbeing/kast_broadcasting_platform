var form = $("#broadcast-container");
var contactGroups = [];
var strContact = "";
var originalText = $("#loading").text(),i  = 0, stopAnimate = false;
var error_num = false;

/** VALIDATION RULES **/
$.validator.addMethod("utfonly", function(value, element) {

    return unicodeTest.test(value);

}, "We noticed you used unsupported characters"); 

$.validator.addMethod("NoWhiteSpaceAtBeginn", function(value, element) {

    var newVal = value.trim();

    return /^[^\s].*/.test(newVal);

  }, "Broadcast message must not begin with a whitespace");


form.validate({
    errorClass: 'error-text',
    errorPlacement: function errorPlacement(error, element) { 
        //element.before(error); 
        if(element.attr("name") == "broadcast_message"){
            error.insertAfter(".message-character-count");
        }
    },
    rules: {
        broadcast_message: {
            required: true,
            utfonly: true,
            NoWhiteSpaceAtBeginn: true
        }
    },
    messages: {
        broadcast_message: {
            required: "Broadcast message is required"
        }
    }
});
app.directive('wizard', [function() {
    return {
        restrict: 'EA',
        scope: {
            stepChanging: '='
        },
        compile: function(element, attr) {
            element.steps({
                bodyTag: attr.bodyTag,
                transitionEffect: "slideLeft",
                autoFocus: true,
                titleTemplate: '<div style="width:100%;"><span class="steps-index">#index#</span></div><span class="steps-text">#title#</span>',
                labels: {
                    next: "NEXT",
                    previous: "PREVIOUS",
                    finish: 'NEXT',
                    loading: "Loading ..."
                },
                autoFocus: true,
                enableContentCache: false,
                preloadContent: true,
                saveState: true,
                startIndex: 0,
                onInit: function (event, currentIndex) {

                    $(".broadcast-container .content").css('min-height', '26em');

                    $("#broadcast-container").show();

                    $('.actions > ul > li:first-child').attr('style', 'display:none');
                    disableWizardNext();

                    $('#broadcast-summary').on('hidden.bs.modal', function (e) {

                        if($(".btn-submit-broadcast").data('trial')){

                            window.location = '/dashboard';
                        }
                        else{

                            element.steps('previous');
                            enableWizardNext();
                        }
                    });

                    // Tooltip
                    $('.copyclip').tooltip({
                      trigger: 'click',
                      placement: 'bottom'
                    });

                    //copy to clipboard
                    var clipboard = new Clipboard('.copyclip');

                    clipboard.on('success', function(e) {
                      setTooltip(e.trigger, 'Copied!');
                      hideTooltip(e.trigger);
                    });

                    clipboard.on('error', function(e) {
                      setTooltip(e.trigger, 'Failed!');
                      hideTooltip(e.trigger);
                    });

                    $('#broadcast_message').keyup(function (evt) {
                        var max = 160;
                        var len = $(this).val().length;


                        var post = $(this).val().replace(/</g, "&lt;").replace(/>/g, "&gt;");
                        //var post = $(this).val();

                        $(".sms-text span").html(post).text();
                        $("#broadcast-summary").find('.message-body').html(post).text();
                        if(len == 0){
                            $("#charCounter").text("0");
                            disableWizardNext();
                        }
                        else{

                            $("#charCounter").text("1");
                            enableWizardNext();
                        }

                        if (len >= max) {
                            $('#characterLeft').text('0');            
                        } 
                        else {
                            var ch = max - len;
                            $('#characterLeft').text(ch);          
                        }
                    });

                    $("input[name=contact_number]").keyup(function(event){

                        var contact = $(this).val().trim();

                        if(event.keyCode == 8){

                            $(".contact-group-error").html("");
                        }

                        if(event.keyCode == 13){

                            $(".btn-add-contact").click();
                        }

                    });

                    $(".btn-add-contact").click(function(){

                        var contact = $("input[name=contact_number]").val();
                        
                        if(contactGroups.length >= 5){
                            $(".btn-add-contact").attr('disabled', 'disabled');
                            $(".contact-group-error").html("");
                            $(".contact-group-error").append('<label class="error">You have reached the maximum limit for mobile numbers</label>');
                        }
                        else{
                            $(".btn-add-contact").removeAttr('disabled');
                            $(".contact-group-error").html("");

                            contact = contact.trim();

                            if(contact.match(mobile_check) != null){
                                
                                $(".contact-group-error").html("");

                                $.ajax({
                                    method: "POST",
                                        url: "/broadcasts/get_franchise/",
                                    data: '{"phonenumber":"'+contact+'"}',    
                                        contentType: "application/json",
                                    headers: {'X-CsrfToken': csrftoken}
                                    }).done(function(msg) {
                                    
                                        if(jQuery.isEmptyObject(msg)){
                                            $(".contact-group-error").html("");
                                            $(".contact-group-error").append('<label class="error">Mobile number is not yet franchised</label>');
                                        }
                                        else{
                                            $(".contact-group-error").html("");
                                            var mobile = msg.number.substr(msg.number.length - 12);
                                            addtoGroup(mobile);
                                        }
                                        
                                    });
                            }
                            else{   

				                error_num = true;
                                $(".contact-group-error").html("");
                                $(".contact-group-error").append('<label class="error">Invalid contact number</label>');
                            }
                        }
                        
                    });

                    $("#broadcast-container").on('click', '.btn-remove', function(){
                        var contact = $(this).data('mobile');
                        $(".btn-add-contact").removeAttr('disabled');
                        $(".contact-group-error").html("");
                        for (var i = 0; i < contactGroups.length; i++)
                        {
                            if (contactGroups[i] == contact)
                            {
                                contactGroups.splice(i, 1);
                                break;
                            }
                        }
                        populateContactGroups();
                    });

                    function addtoGroup(contact){
                        if(contactGroups.length >= 5){
				            error_num = true;
                            $(".btn-add-contact").attr('disabled', 'disabled');
                            $(".contact-group-error").html("");
                            $(".contact-group-error").append('<label class="error">You have reached the maximum limit for mobile numbers</label>');
                        }
                        else{
                            
                             if(contactGroups.includes(contact)){
				                error_num = true;
                                $(".contact-group-error").html("");
                                $(".contact-group-error").append('<label class="error">You must enter unique mobile number</label>');
                            }
                            else{
                                contactGroups.push(contact);
                                populateContactGroups();
                            }
                        }
                    }
                    function populateContactGroups(){
                        $(".with-contacts").find("li").remove();
                        $("#contact_number").val("");
                        strContact = "";
                        for(var i in contactGroups){
                            var lists = '<li class="list-group-item">'+
                                        '<span class="contact-text">'+contactGroups[i]+'</span>'+
                                        '<button type="button" class="btn-remove" data-mobile="'+contactGroups[i]+'"><img src="/static/images/close-btn.png"></button>'+
                                        '</li>';
                            $(".with-contacts").append(lists);
                            strContact += contactGroups[i]+', ';
                        }

                        if(contactGroups.length == 0){
                            $(".no-contacts").show();
                            $(".with-contacts").hide();
                            disableWizardNext();
                        }
                        else{

                            $(".no-contacts").hide();
                            $(".with-contacts").show();

                            enableWizardNext();
                        }

                        strContact = strContact.substr(0, strContact.length - 2);
                        $("input[name=form_contact_group]").val(strContact);
                        $(".total_audience").html(strContact);
                        $(".total-counter .counter").html(contactGroups.length);
                    }
                    return true;
                },
                onStepChanging: function(event, currentIndex, newIndex){

                        if($("input[name=form_contact_group]").val() == ""){
                            $(".contact-group-error").html("");
                            $(".contact-group-error").append('<label class="error">You must enter at least one contact number</label>');
                            return false;
                        }
                        else{

                            // If user click on "Previous" button, we just normally let he/she goes
                            if (newIndex < currentIndex) {

                                enableWizardNext();
                                return true;
                            }
                            else{

                                if($("#broadcast_message").val() != ""){

                                    enableWizardNext();
                                }
                                else{

                                    disableWizardNext();
                                }

                                form.validate().settings.ignore = ":disabled,:hidden";
                                return form.valid();
                            }
                        }
                    
                },
                onStepChanged: function (event, currentIndex, priorIndex){ 
                    if (currentIndex > 0) {
                        $('.actions > ul > li:first-child').attr('style', '');
                        if(currentIndex == 2){
                            $('.actions > ul > li:nth-child(2)').css('display', 'none');
                            $("#broadcast-summary").modal({backdrop: 'static',
                                                    keyboard: false});
                            return false;
                        }

                    } else {
                        $('.actions > ul > li:nth-child(2)').css('display', 'block');
                        $('.actions > ul > li:first-child').attr('style', 'display:none');
                    }

                    if(currentIndex == 0){

                        $("#broadcast-container .content").css('min-height', '26em');
                    }
                    else if(currentIndex == 1 || currentIndex == 2){

                        $("#broadcast-container .content").css('min-height', '30em');
                    }

                },
                onFinishing: function (event, currentIndex) 
                { 
                    //if(form.validate().numberOfInvalids() > 0 || $("input[name=schedule_date]").val() == "" || $("input[name=schedule_time]").val() == ""){
                        form.validate().settings.ignore = ":disabled,:hidden";
                        return form.valid();
                },
                onFinished: function (event, currentIndex){
                    /*$(".modal").modal("show");
                    return false;*/
                }
            });
            return {
                //pre-link
                pre:function() {},
                //post-link
                post: function(scope, element) {
                    element.on('stepChanging', scope.stepChanging);
                }
            }
        }
    };
}]);
  
app.controller('Ctrl', ['$scope', 'dataFactory', function($scope, dataFactory) {
    $scope.validLength = 160;
    $scope.increment = 1;
    $scope.contact_groups;
    $scope.status;
    $scope.sender_ids = [{"id": 1, "name": "VERIFY"}];
    $scope.default_sender_id = $scope.sender_ids[0];


}]);

$(".btn-submit-broadcast").click(function() {

    var data = {
        phonenumbers: contactGroups,
        message: $("#broadcast_message").val()
    }

    $("#loading").css('display', 'block');
    /*setInterval(function() {

        if(stopAnimate === false){

            $("#loading").append(".");
        }
        else{

            $("#loading").append("");
        }
    
        i++;
        
        if(i == 4)
        {
            $("#loading").html(originalText);
            i = 0;
        }

    }, 500);*/

    $(".btn-submit-broadcast").attr('disabled', 'disabled');
    $(".btn-back").attr('disabled', 'disabled');

    $.ajax({
    method: "POST",
    url: "/broadcasts/do_trial/",
    data: JSON.stringify(data),   
    contentType: "application/json",
    headers: {'X-CsrfToken': csrftoken},
    }).done(function(msg) {
        $(".error-broadcast").html("");
        var ref_id = msg.ref_id;
        $("#broadcast-summary").modal("hide");
        $("#reference_number").val(ref_id);
        $("#broadcast-controller").hide();
        $(".modal-backdrop").hide();
        $("#broadcast-result").show();
        $('body').removeClass('modal-open');
    }).fail(function(jqXHR, textStatus){
        var error = jqXHR.responseText;
        var data_error = $.parseJSON(error);

         i = 4;
        stopAnimate = true;
        $("#loading").html("");

        $(".btn-submit-broadcast").removeAttr('disabled');
        $(".btn-back").removeAttr('disabled');
        
        if(data_error.code == '004'){
            //trial already consumed
            $(".btn-submit-broadcast").attr('data-trial', true);
            $(".error-broadcast").html("Free trial is already consumed");
            //window.location = "/dashboard";
        }
        else if(data_error.code == '005'){

            //outside schedule
            window.location = '/broadcasts/trial';
        }
        else if(data_error.code == '007' || data_error.code == '003'){
            //call to sender api/broadcast failed
            $(".btn-submit-broadcast").removeAttr('disabled');
            $(".btn-back").removeAttr('disabled');
            //$(".btn-submit-broadcast").attr('data-error', 'invalid_schedule');
            $(".error-broadcast").html("Trial broadcast failed. Please try again");
        }
        else if(data_error.code == '013'){

            //more than 5 contacts error
            $(".btn-submit-broadcast").attr('data-trial', true);
            $(".error-broadcast").html(data_error.error);
        }
        
    });
});
