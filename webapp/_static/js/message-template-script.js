form = $("#msg-template-form");
var maxBroadcastLength = 459;

$(".message-template-container").on('click', '.btn-edit', function(){
    var id = $(this).data('id');
    $.ajax({
        type: "POST",
        dataType: 'json',
        data: {'id': id, '_method': 'edit', 'csrfmiddlewaretoken': csrftoken},
        success: function(response){
            var obj = $.parseJSON(JSON.stringify(response.data));
            var data = $.parseJSON(JSON.stringify(obj.data));

            $(".btn-save-template").removeAttr('disabled');
            $("input[name=template_name]").val(data.name);
            $("#broadcast_message").val(data.message);
            $("input[name=id]").val(data.pk);
            $("input[name=_method]").val('update');

            var ch = data.message.length - 2;
            $('#characterLeft').text(maxBroadcastLength - ch);
            getCounter(ch);
            $(".template-error").html("");
            
        }
    });
});
$(".message-template-container").on('click', '.btn-remove', function(){
    var id = $(this).data('id');
    $("#delete-record").find(".btn-confirm-delete").attr('data-id', id);
    $("#delete-record").modal("show");
});

$(".btn-confirm-delete").click(function(){
    var id = $(this).data('id');
    $.ajax({
        type: "POST",
        dataType: 'json',
        data: {'id': id, '_method': 'delete', 'csrfmiddlewaretoken': csrftoken},
        success: function(response){
            if(response.status){
                location.reload(true);
            }
        }
    });
});

/** VALIDATION RULES **/
$.validator.addMethod("utfonly", function(value, element) {

    return unicodeTest.test(value);

}, "We noticed you used unsupported characters");

$.validator.addMethod("NoWhiteSpaceAtBeginn", function(value, element) {
    var newVal = value.trim();
    return /^[^\s].*/.test(newVal);
  });

form.validate({
    errorPlacement: function errorPlacement(error, element) { 
        //element.before(error); 
        if(element.attr("name") == "broadcast_message"){
            error.insertAfter(".textarea-wrapper");
        }
        if(element.attr("name") == "template_name"){
            error.insertAfter("input[name=template_name]");
        }
    },
    rules: {
        template_name: {
            required: true,
            utfonly: true,
            NoWhiteSpaceAtBeginn: true,
            maxlength: 50
        },
        broadcast_message: {
            required: true,
            utfonly: true,
            NoWhiteSpaceAtBeginn: true
        }
    },
    messages: {
        template_name: {
            required: "Template name is required",
            NoWhiteSpaceAtBeginn: "Template name must not begin with a whitespace",
            maxlength: 'Please enter no more than 50 characters'
        },
        broadcast_message: {
            required: "Broadcast message is required",
            NoWhiteSpaceAtBeginn: "Message must not begin with a whitespace"
        }
    },
    success: function(){ 
        
        $(".btn-save-template").removeAttr('disabled'); 
    }, 
    submitHandler: function(form) {

        $.LoadingOverlay("show", {
            color: "rgba(255, 255, 255, 0.42)"
        });

        $(".btn-save-template").attr('disabled', 'disabled');

        $.ajax({
            type: "POST",
            dataType: 'json',
            data: $("#msg-template-form").serialize(),
            success: function(response){
                
                $.LoadingOverlay("hide");

                $(".btn-save-template").removeAttr('disabled');

                if(response.status === false){
                    $(".template-error").html("");
                    $(".template-error").append('<label class="error">'+response.message+'</label>');
                }
                else{
                    //location.reload(true);
                    var template_name = $("#template_name").val().replace(/</g, "&lt;").replace(/>/g, "&gt;");
                    var message = $("#broadcast_message").val().replace(/</g, "&lt;").replace(/>/g, "&gt;");

                    $("#save-success").find(".template-name").html(template_name).text();
                    $("#save-success").find(".message-body").html(message).text();
                    $("#save-success").modal("show");
                }
            }
        });
    }
});

counter=5;
size_li = $(".template-groups li").size();

//load more
$('.template-groups li:lt('+counter+')').show();
if(size_li <= 5){

    $('#loadMore').css('display', 'none');
}
else{
    $('#loadMore').css('display', 'block');
}

$('#loadMore').click(function () {
    counter= (counter+5 <= size_li) ? counter+5 : size_li;
    $('.template-groups li:lt('+counter+')').show();
});

$('#save-success').on('hidden.bs.modal', function (e) {
    location.reload(true);
});

$('#broadcast_message').keyup(function () {

    var len = $(this).val().length;

    getCounter(len);

    var ch = maxBroadcastLength - len;
    $('#characterLeft').text(ch);
    
});

function searchText() {
    var input, filter, ul, li, a, i;
    input = document.getElementById("inputString");
    filter = input.value.toUpperCase();
    ul = document.getElementById("myUL");
    li = ul.getElementsByTagName("li");

    if(filter.length == 0){
       $('.template-groups li').hide();
       $('.template-groups li:lt(5)').show();

       if(size_li > 5){

            $('#loadMore').css('display', 'block');
       }

        $(".search-result").html("");
    }

    var newVal = filter.trim();

    if(!/^[^\s].*/.test(newVal)){

        filter = null;
    }

    if(filter != null){

        var count = 0;
        for (i = 0; i < li.length; i++) {
            a = li[i].getElementsByTagName("h4")[0];
            if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
                li[i].style.display = "block";
                count++;
            } else {
                b = li[i].getElementsByClassName('content-body')[0];
                if(b.innerHTML.toUpperCase().indexOf(filter) > -1){
                    li[i].style.display = "block";
                    count++;
                }
                else{

                    li[i].style.display = "none";
                }
            }
        }

        var result_text = " result";
        if(count > 1){
            result_text = " results";
        }

        $(".search-result").html("<em>"+count + result_text+" found</em>");

        $('#loadMore').css('display', 'none');
    }
}

function getCounter(len){

    if(len == 0){

        $("#charCounter").text("0");
    }
    else if (len >= 1 && len <= 160){

        $("#charCounter").text("1");
    
    }else if(len >= 161 && len <= 306){
        
        $("#charCounter").text("2");
    }
    else{
        
        $("#charCounter").text("3");
    }
}

app.controller('Ctrl', ['$scope', 'dataFactory', function($scope, dataFactory) {
    $scope.validLength = 459;
    $scope.increment = 1;
    $scope.templates = [];

}]);

app.filter('cut', function () {
        return function (value, wordwise, max, tail) {
            if (!value) return '';
            max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;
            value = value.substr(0, max);
            if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace != -1) {
                  //Also remove . and , so its gives a cleaner result.
                  if (value.charAt(lastspace-1) == '.' || value.charAt(lastspace-1) == ',') {
                    lastspace = lastspace - 1;
                  }
                  value = value.substr(0, lastspace);
                }
            }
            return value + (tail || ' …');
        };
    });
