form = $("#sender-id-form");

$(".btn-remove").click(function(){
    var id = $(this).data('id');
    
    $("#delete-record .btn-confirm-delete").attr('data-id', id);

    $("#delete-record").modal("show");
});

$(".btn-confirm-delete").click(function(){
    var id = $(this).data('id');
    $.ajax({
        type: "POST",
        dataType: 'json',
        data: {'id': id, '_method': 'delete', 'csrfmiddlewaretoken': csrftoken},
        success: function(response){
            if(response.status){
                location.reload(true);
            }
        }
    });
});

$("#sender_id").keyup(function(){

    var len = $(this).val().length;

    if(len == 0){

        $(".btn-save-template").attr('disabled', 'disabled');
    }
    else{

        $(".btn-save-template").removeAttr('disabled'); 
    }

});

/** VALIDATION RULES **/
$.validator.addMethod("alphanumeric", function(value, element) {
    return alpha_check.test(value);
}, "Sender ID must be alphanumeric characters"); 

$.validator.addMethod("NoWhiteSpaceAtBeginn", function(value, element) {
    var newVal = value.trim();
    return /^[^\s].*/.test(newVal);
});

form.validate({
    errorPlacement: function errorPlacement(error, element) { 
        //element.before(error); 

        if(element.attr("name") == "sender_id"){
            error.insertAfter("input[name=sender_id]");
        }
    },
    rules: {
        sender_id: {
            required: true,
            alphanumeric: true,
            NoWhiteSpaceAtBeginn: true,
            maxlength: 11
        }
    },
    messages: {
        sender_id: {
            required: "Sender ID is required",
            NoWhiteSpaceAtBeginn: "Message must not begin with a whitespace",
            maxlength: 'Please enter no more than 11 alphanumeric characters'
        }
    },
    success: function(){ 
        
        $(".btn-save-template").removeAttr('disabled'); 
    }, 
    submitHandler: function(form) {

        $.LoadingOverlay("show", {
            color: "rgba(255, 255, 255, 0.42)"
        });

	$("#sender_id-error").remove();

        $.ajax({
            method: "POST",
            dataType: 'json',
            data: $("#sender-id-form").serialize(), 
        }).done(function(msg) {

            $.LoadingOverlay("hide");

            var status = msg.status;
            
            if(status === false){

                var data_error = msg.data;

                $(".template-error").html("");
                $(".template-error").html('<label class="error">'+data_error.error+'</label>');
            }
            else{
                $("#save-success").modal("show");
            }

        }).fail(function(jqXHR, textStatus){

            var error = jqXHR.responseText;

            var data_error = $.parseJSON(error);

            $(".template-error").html("");
            $(".template-error").html('<label class="error">'+data_error.error+'</label>');
        });
    }
});


counter=5;

size_li_pending = $("#pending .list-group li").size();
size_li_approved = $("#approved .list-group li").size();
size_li_rejected = $("#rejected .list-group li").size();

//load more pending
$('#pending .list-group li:lt('+counter+')').show();
if(size_li_pending <= 5){

    $('#loadMorePending').css('display', 'none');
}
else{
    $('#loadMorePending').css('display', 'block');
}

$('#loadMorePending').click(function () {
    counter= (counter+5 <= size_li_pending) ? counter+5 : size_li_pending;
    $('#pending .list-group li:lt('+counter+')').show();
});
/*load more pending end*/

//load more approved
$('#approved .list-group li:lt('+counter+')').show();
if(size_li_approved <= 5){

    $('#loadMoreApproved').css('display', 'none');
}
else{
    $('#loadMoreApproved').css('display', 'block');
}

$('#loadMoreApproved').click(function () {
    counter= (counter+5 <= size_li_approved) ? counter+5 : size_li_approved;
    $('#approved .list-group li:lt('+counter+')').show();
});
/*load more approved end*/

//load more rejected
$('#rejected .list-group li:lt('+counter+')').show();
if(size_li_rejected <= 5){

    $('#loadMoreRejected').css('display', 'none');
}
else{
    $('#loadMoreRejected').css('display', 'block');
}

$('#loadMoreRejected').click(function () {
    counter= (counter+5 <= size_li_rejected) ? counter+5 : size_li_rejected;
    $('#rejected .list-group li:lt('+counter+')').show();
});
/*load more rejected end*/


//search function
var string,i,j;

$('#search-senderid').on('input', function() {

    string = $(this).val().toLowerCase();
    var ul, li, a, i;
    ul = $(".list-group");
    li = ul.find("li");

    if(string.length == 0){

        $('.nav-search > li').removeClass('active');
        $('.nav-search > li').css('display', 'block');
        $('.nav-search > li:first-child').addClass('active');
        $('.tab-content > div').removeClass('active');
        $('.tab-content > div:first-child').addClass('active');
        /*$('.list-group li').hide();
        $('.list-group li:lt(5)').show();*/

        $("#pending .list-group li").hide();
        $("#pending .list-group li:lt(5)").show();

        if(size_li_pending > 5){

            $("#loadMorePending").css('display', 'block');
        }

        $("#approved .list-group li").hide();
        $("#approved .list-group li:lt(5)").show();

        if(size_li_approved > 5){

            $("#loadMoreApproved").css('display', 'block');
        }

        $("#rejected .list-group li").hide();
        $("#rejected .list-group li:lt(5)").show();

        if(size_li_rejected > 5){

            $("#loadMoreRejected").css('display', 'block');
        }

        $(".search-result").html("");
    }
    else{

        $(".act_load_more").css('display', 'none');
    
        var newVal = string.trim();
        if(!/^[^\s].*/.test(newVal)){
            string = null;
        }

        if(string != null){

            var count = 0;

            //for (j = 0; j < tabsContent.length; j++) {
                //console.log(tabContent[j].indexOf(string));
                /*if (tabContent[j].indexOf(string) > -1) {
                    li[j].style.display = "block";
                    count++;
                    
                }*/

                for (i = 0; i < li.length; i++) {
                    a = li[i].getElementsByTagName("span")[0];
                    if (a.innerHTML.toLowerCase().indexOf(string) > -1) {
                        li[i].style.display = "block";
                        count++;
                      
                    } else {
                        li[i].style.display = "none";
                    }
                }

                var result_text = " result";
                if(count > 1){
                    result_text = " results";
                }

                $(".search-result").html("<em>"+count + result_text+" found</em>");
            }
        //}
    }
  
});