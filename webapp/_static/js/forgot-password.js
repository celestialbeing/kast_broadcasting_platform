$("#header-navigation").sticky({ topSpacing: 0 });   
                            
var form = $("#forgot-password-form");

/** VALIDATION RULES **/
form.validate({         
    highlight: function(element, errorClass, validClass) {
        $(element).addClass(errorClass).removeClass(validClass);
        $(element).siblings("span").removeClass("fa-check-circle").addClass("fa-times-circle");
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass(errorClass).addClass(validClass);
        if ($(element).val() !== '') {
            $(element).siblings("span").removeClass("fa-times-circle").addClass("fa-check-circle");
        } else {
            $(element).siblings("span").removeClass("fa-check-circle fa-times-circle");
        }
    },
    errorPlacement: function errorPlacement(error, element) { element.before(error); },
    rules: {                
        email: {                
            email: true         
        }
    },
    submitHandler: function(form) {
	$.ajax({
	    type: "POST",
	    data: $("#forgot-password-form").serialize(),
	    dataType: 'json',
	    success: function (data) {
		//if (data.status !== "ok") {
                //   var label = '<label class="error" style="margin-top:1em">Email does not exist</label>';
                //   $("#forgot-password-form .form-group:first-of-type").append(label);
                //   $("#forgot-password-form #email").addClass("error");
                //   $("#forgot-password-form .form-group:first-of-type span").removeClass("fa-check-circle").addClass("fa-times-circle");
		//} else {
		//}
	    }
        });
        $('#successModal').modal({backdrop: 'static'});
	$('#successModal').modal('show');    
    }                               
});         

/** DISABLE SUBMIT BUTTON IF INPUT IS INCOMPLETE **/
$(document).ready(function() {
    var empty = false;
    $('input:not([type="submit"])').each(function() {
        if ($(this).val().length === 0 && $.trim($(this).val()) === '') {
            empty = true;
        }
    });

    if (empty) {
        $('.btn-submit').attr('disabled', 'disabled');
    } else {
        $('.btn-submit').removeAttr('disabled');
    }    
});

(function() {
    $('input:not([type="submit"])').on("change keyup blur mouseenter", function() {

        var empty = false;
        $('input:not([type="submit"])').each(function() {
            if ($(this).val().length === 0 && $.trim($(this).val()) === '') {
                empty = true;
            }
        });

        if (empty) {
            $('.btn-submit').attr('disabled', 'disabled');
        } else {
            $('.btn-submit').removeAttr('disabled');
        }
    });
})()

/** STEPS WIZARD 
form.children("div").steps({        
    headerTag: "h2",            
    bodyTag: "section",     
    transitionEffect: "slideLeft",
    onStepChanging: function (event, currentIndex, newIndex)
    {
        form.validate().settings.ignore = ":disabled,:hidden";
        return form.valid();
    },
    onFinishing: function (event, currentIndex)
    {
        form.validate().settings.ignore = ":disabled";
        return form.valid();
    },
    onFinished: function (event, currentIndex)
    {
        alert("Submitted!");
    }  
});
**/
