$('#first-main-link').click(function() {
    $('html, body').animate({
        scrollTop: $('#first-main').offset().top
    }, 1000);
    $(this).addClass('active');
    $('#second-main-link').removeClass('active');
    $('#third-main-link').removeClass('active');
});

$('#second-main-link').click(function() {
    $('html, body').animate({
         scrollTop: $('#second-main').offset().top
    }, 1000);
    $(this).addClass('active');
    $('#first-main-link').removeClass('active');
    $('#third-main-link').removeClass('active');
});

$('#third-main-link').click(function() {
    $('html, body').animate({
        scrollTop: $('#third-main').offset().top
    }, 1000);
    $(this).addClass('active');
    $('#second-main-link').removeClass('active');
    $('#first-main-link').removeClass('active');
});

$("#header-navigation").sticky({ topSpacing: 0 });

var form = $("#login-form");

form.validate({
    errorPlacement: function errorPlacement(error, element) { element.before(error); },
    rules: {
	email: {
	    email: true
	}
    },
    messages: {
	email: {
	    email: "Please input a valid email address"
	}
    },
    submitHandler: function(form) {
	$.ajax({
	    type: "POST",
            data: $("#login-form").serialize(),
            dataType: 'json',
	    success: function (data) {
                if(data.status == "ok"){
		    window.location.replace(data.url);
                } else {
                   var label = '<label class="error" style="margin-top:1em">Invalid email or password</label>';
                   $("#login-form .form-group:first-of-type").append(label);
                   $("#login-form #email").addClass("error");
                   $("#login-form #password").addClass("error");
                }
	    }
	});
    }
});

/** DISABLE SUBMIT BUTTON IF INPUT IS INCOMPLETE **/
$(document).ready(function() {
    var empty = false;
    $('input:not([type="submit"])').each(function() {
        if ($(this).val().length === 0 && $.trim($(this).val()) === '') {
            empty = true;
        }
    });

    if (empty) {
        $('.btn-submit').attr('disabled', 'disabled');
    } else {
        $('.btn-submit').removeAttr('disabled');
    }    
});

(function() {
    $('input:not([type="submit"])').on("change keyup blur mouseenter", function() {

        var empty = false;
        $('input:not([type="submit"])').each(function() {
            if ($(this).val().length === 0 && $.trim($(this).val()) === '') {
                empty = true;
            }
        });

        if (empty) {
            $('.btn-submit').attr('disabled', 'disabled');
        } else {
            $('.btn-submit').removeAttr('disabled');
        }
    });
})()
