$("#header-navigation").sticky({ topSpacing: 0 });
var form = $("#login-form");
/** VALIDATION RULES **/
form.validate({
    highlight: function(element, errorClass, validClass) {
        $(element).addClass(errorClass).removeClass(validClass);
        $(element).siblings("span").removeClass("fa-check-circle").addClass("fa-times-circle");
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).removeClass(errorClass).addClass(validClass);
        if ($(element).val() !== '') {
            $(element).siblings("span").removeClass("fa-times-circle").addClass("fa-check-circle");
        } else {
            $(element).siblings("span").removeClass("fa-check-circle fa-times-circle");
        }
    },
    errorPlacement: function errorPlacement(error, element) { element.before(error); },
    rules: {
        email: {
            email: true
        },
    },
    submitHandler: function(form) {
        $('#liveAccountModal').modal('show');
	$.ajax({
            type: "POST",
            data: $("#login-form").serialize(),
            dataType: 'json',
            success: function (data) {
               if(data.status == "ok"){
                   window.location.replace(data.url);
               } else {
		   var label = '<label class="error" style="margin-top:1em">Invalid email or password</label>';
		   $("#login-form .form-group:first-of-type").append(label);
		   $("#login-form #email").addClass("error");
		   $("#login-form .form-group:first-of-type span").removeClass("fa-check-circle").addClass("fa-times-circle");
		   $("#login-form #password").addClass("error");
		   $("#login-form .form-group:nth-of-type(2) span").removeClass("fa-check-circle").addClass("fa-times-circle");
               }
            }
        });
    }
});
/** DISABLE SUBMIT BUTTON IF INPUT IS INCOMPLETE **/
$(document).ready(function() {
    var empty = false;
    $('input:not([type="submit"])').each(function() {
        if ($(this).val().length === 0 && $.trim($(this).val()) === '') {
            empty = true;
        }
    });
    if (empty) {
        $('.btn-submit').attr('disabled', 'disabled');
    } else {
        $('.btn-submit').removeAttr('disabled');
    }    
});
(function() {
    $('input:not([type="submit"])').on("change keyup blur mouseenter", function() {
        var empty = false;
        $('input:not([type="submit"])').each(function() {
            if ($(this).val().length === 0 && $.trim($(this).val()) === '') {
                empty = true;
            }
        });
        if (empty) {
            $('.btn-submit').attr('disabled', 'disabled');
        } else {
            $('.btn-submit').removeAttr('disabled');
        }
    });
})()
