var table = $('#dashboard-table').DataTable({
    "scrollX": true,
    "pageLength": 10,
    "bInfo" : true,
    "order": [[ 0, "desc" ]],
    dom: "<'row'<'col-sm-12 custom-filters'f>>t<'row'<'col-sm-12 bottom-filters'i>>p",
    "pagingType": "full_numbers",
    "language": {
        "lengthMenu": "Rows:<br> _MENU_",
        "infoEmpty": "No records available",
        "search": "Search: ",
        "paginate": {
            "previous": "Previous",
            "next": "Next",
            "first": "First",
            "last": "Last"
        }
    },
    scrollCollapse: false,
    fixedColumns:   {
            leftColumns: 3,
            rightColumns: 8
    },
    "columnDefs": [
            {
                "targets": [0],
                "visible": false
            },
            {
                "targets": [1, 13],
                "visible": true,
                "orderable": false
            }, 
            {
                "targets": [14, 15, 16, 17, 18, 19, 20],
                "visible": false,
                "orderable": false
            }
        ]
});

// starting websocket connection
var socket = new WebSocket('ws://' + window.location.host + '/statuses/');


socket.onopen = function open() {
    //console.log('WebSockets connection created.');
};

socket.onmessage = function message(event) {
    var data = JSON.parse(event.data);

    setInterval(function(){

        table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {
            var data = this.data();
            
            //console.log(data);
            var broadcast_id = data[14];
            var sched_date =data[18];
            var sender_id = data[16];
            var cmessage = data[15];
            var campaign_name = data[2];
            var campaign_id = data[0];
            var is_draft = data[17];

            var audience = "-";
            if(data[9] > 0){
                audience = data[9];
            }

            if(sched_date != "-" && is_draft != "True"){
                var schedule = Date.parse(sched_date);

                //console.log(sched_date);

                var sched_epoch = schedule.getTime() / 1000;

                var date_today = Date.today().getTime() / 1000;

                var date_minus = Date.today().add(-3).days().getTime() / 1000;
                if(date_minus < sched_epoch && sched_epoch <= date_today){

                    var idx = table.row( $(this) ).index();

                    socket.send(broadcast_id);
                    socket.onmessage = function message(event) {
                        var data = JSON.parse(event.data);

                        var onqueue = JSON.parse(data['onqueue']);
                        var forwarded = JSON.parse(data['forwarded']);
                        var sent = JSON.parse(data['sent']);
                        var failed = JSON.parse(data['failed']);

                        var count_onqueue = "-";
                        if(onqueue.count > 0 || onqueue.count != null){

                            count_onqueue = onqueue.count;
                        }

                        var count_forwarded = "-";
                        if(forwarded.count > 0 || forwarded.count != null){

                            count_forwarded = forwarded.count;
                        }

                        var count_sent = "-";
                        if(sent.count > 0 || sent.count != null){

                            count_sent = sent.count;
                        }

                        var failed_text = "-";
                        if(failed.count > 0 || failed.count != null){

                            var failed_base = ""
                            $.each(failed.base, function(index, value){
                                failed_base = failed_base + value.msisdn + "#" +value.description+ ", "
                            });

                            var failed_desc = ""
                            $.each(failed.base, function(index, value){
                                failed_desc = failed_desc + value.description + ", "
                            });

                            failed_desc = failed_desc.substring(0, failed_desc.length - 1);
                            failed_base = failed_base.substring(0, failed_base.length - 1);

                            failed_text = '<a href="javascript:void(0)" style="color: #cf6162;text-decoration: underline;" class="btn-failed" id="campaign-'+campaign_id+'" data-campaign-id="'+campaign_id+'" data-campaign-name="'+campaign_name+'" data-sender-id="'+sender_id+'" data-message="'+cmessage+'" data-schedule="'+sched_epoch+'" data-broadcast-id="'+broadcast_id+'" data-failed="'+failed_base+'" data-failed-desc="'+failed_desc+'"><span>'+failed.count+'</span></a>';
                        }

                        table.cell( rowIdx, 6).data(count_onqueue);
                        var is_forwarded = table.cell( rowIdx, 7).data(count_forwarded);
                        table.cell( rowIdx, 8).data(count_sent);
                        table.cell( rowIdx, 9 ).data(failed_text);

                        if(is_forwarded.data() > 0 && is_forwarded.data() != "-"){

                            table.cell( rowIdx, 13).data('<a data-toggle="modal" href="javascript:void(0)" class="btn-dashboard btn-summary" data-row-campaign="'+campaign_name+'" data-row-audience="'+audience+'" data-row-message="'+cmessage+'" data-row-senderid="'+data[20]+'" data-row-schedule="'+sched_date+'" title="View Summary"><span class="fa fa-eye"></span></a>&nbsp;&nbsp;<a href="/broadcasts/live/?q='+data[19]+'" class="btn-dashboard btn-edit btn-disabled" title="Edit"><span class="fa fa-edit"></span></a>&nbsp;&nbsp;<a data-toggle="modal" href="javascript:void(0)" class="btn-dashboard btn-delete btn-disabled" data-row-id="'+data[19]+'" data-row-campaign="'+data[2]+'" title="Delete"><span class="fa fa-trash-o"></span></a>&nbsp;&nbsp;<a href="/broadcasts/get_report/'+broadcast_id+'" class="btn-dashboard btn-request" title="Request for a Report"><span class="fa fa-bar-chart"></span></a>');
                        }

                        table.fixedColumns().update();
                    }
                }
            }

        });

    }, 30000);
};

if (socket.readyState == WebSocket.OPEN) {
    socket.onopen();
}

$('#dashboard-table_filter').addClass('form-inline');
$('#dashboard-table_filter input').css('margin-left', '5px');
$('#dashboard-table_filter input').addClass('form-control');
/*
$('#dashboard-table_filter input').attr('placeholder', 'Search...');

$('#dashboard-table_filter input').removeClass("input-sm");
$('#dashboard-table_filter input').attr('aria-describedby','basic-addon2');
$('#dashboard-table_filter input').before('<span class="input-group-addon" id="basic-addon2"><span class="glyphicon glyphicon-search"></span></span>');*/

var filter_field = '<div class="filter-container form-inline"><div class="form-group"><label for="filter" class="filter-label">Filter by&nbsp;&nbsp; </label><select class="filter form-control"><option value="all">All</option><option value="4">Campaign Name</option><option value="5">Schedule</option><option value="6">Date Created</option><option value="8">Campaign Base</option></select></div></div>';
var date_field = '<div id="date-container" class="filter-container form-inline" style="margin-left:5px;"><div class="form-group"><label for="filter" class="filter-label">Date&nbsp;&nbsp; </label><input id="date" class="datepicker form-control" /></div></div>';

var delete_field = '<form class="form-inline" style="margin-top:10px;"><div class="checkbox"><label> <input type="checkbox" value=""> <span style="margin-top: 3px;float: right;margin-left: 5px;margin-right: 15px;">Select All</span> </label></div>&nbsp;&nbsp;<button type="button" class="btn btn-gray btn-sm"><span class="glyphicon glyphicon-trash"></span>&nbsp;Delete</button></form>';

$(".dataTables_info").before($(delete_field));
$(".custom-filters #dashboard-table_filter").before($(filter_field));
$(".custom-filters #dashboard-table_filter").before($(date_field));

$("select[name='dashboard-table_length']").addClass('form-control');

$("#status-container, #date-container").hide();

$(".dataTables_wrapper").on("click", '.btn-summary', function() {

    var rowCampaignName         = $(this).data('row-campaign');
    var rowAudience         = $(this).data('row-audience');
    var rowMessage          = $(this).data('row-message');
    var rowSID          = $(this).data('row-senderid');
    var rowScheduleDate         = $(this).data('row-schedule');

    var schedule = rowScheduleDate.split(" ");
    var date = schedule[0];
    var time = schedule[1];

    $(".modal-summary #cname").html(rowCampaignName);
    $(".modal-summary #caudience").html(rowAudience);
    $(".modal-summary #cmessage").html(rowMessage);
    $(".modal-summary #csenderid").html(rowSID);
    $(".modal-summary #cdate").html(date);
    $(".modal-summary #ctime").html(time);

    $("#broadcast-summary").modal("show");

});

$(".dataTables_wrapper").on('click', '.btn-delete', function(){
    var rowCampaignID   = $(this).data('row-id');
    var rowCampaignName     = $(this).data('row-campaign');

    $("#deleteModal #modal-campaign-name").html(rowCampaignName);
    $("#deleteModal #delete-confirm").val(rowCampaignID);

    $("#deleteModal").modal({backdrop: 'static'});
    $("#deleteModal").modal("show"); 

    localStorage.setItem('successDelete', true);
});

/*check for successful delete*/
if(localStorage.getItem('successDelete')){

    $("#successModalDashboard").modal({backdrop: 'static'});
    $("#successModalDashboard").modal("show"); 

    localStorage.removeItem('successDelete');
}

$('#successModalDashboard').on('hidden.bs.modal', function (e) {

  localStorage.removeItem('successDelete');
  var successDelete = localStorage.getItem('successDelete');

  window.location = '/broadcasts/lists/';

});

$('#deleteModal').on('hidden.bs.modal', function (e) {

  localStorage.removeItem('successDelete');

});

/** ACTION ICONS AVAILABILITY BASED ON STATUS VALUE **/
table.rows().every( function ( rowIdx, tableLoop, rowLoop ) {

    var data = this.data();
            
    //console.log(data);
    var broadcast_id = data[14];
    var sched_date =data[18];
    var sender_id = data[16];
    var cmessage = data[15];
    var campaign_name = data[2];
    var campaign_id = data[0];
    var is_draft = data[17];

    var audience = "-";
    if(data[9] > 0){
        audience = data[9];
    }

    var on_forwarded = table.cell( rowIdx, 7).data();
    
    if(on_forwarded != "-" && on_forwarded != "" && sched_date != "-" && is_draft != "True"){

        table.cell( rowIdx, 13).data('<a data-toggle="modal" href="javascript:void(0)" class="btn-dashboard btn-summary" data-row-campaign="'+campaign_name+'" data-row-audience="'+audience+'" data-row-message="'+cmessage+'" data-row-senderid="'+data[20]+'" data-row-schedule="'+sched_date+'" title="View Summary"><span class="fa fa-eye"></span></a>&nbsp;&nbsp;<a href="/broadcasts/live/?q='+data[19]+'" class="btn-dashboard btn-edit btn-disabled" title="Edit"><span class="fa fa-edit"></span></a>&nbsp;&nbsp;<a data-toggle="modal" href="javascript:void(0)" class="btn-dashboard btn-delete btn-disabled" data-row-id="'+data[19]+'" data-row-campaign="'+data[2]+'" title="Delete"><span class="fa fa-trash-o"></span></a>&nbsp;&nbsp;<a href="/broadcasts/get_report/'+broadcast_id+'" class="btn-dashboard btn-request" title="Request for a Report"><span class="fa fa-bar-chart"></span></a>');
    }
    else{

        table.cell( rowIdx, 13).data('<a data-toggle="modal" href="javascript:void(0)" class="btn-dashboard btn-summary" data-row-campaign="'+campaign_name+'" data-row-audience="'+audience+'" data-row-message="'+cmessage+'" data-row-senderid="'+data[20]+'" data-row-schedule="'+sched_date+'" title="View Summary"><span class="fa fa-eye"></span></a>&nbsp;&nbsp;<a href="/broadcasts/live/?q='+data[19]+'" class="btn-dashboard btn-edit" title="Edit"><span class="fa fa-edit"></span></a>&nbsp;&nbsp;<a data-toggle="modal" href="javascript:void(0)" class="btn-dashboard btn-delete" data-row-id="'+data[19]+'" data-row-campaign="'+data[2]+'" title="Delete"><span class="fa fa-trash-o"></span></a>&nbsp;&nbsp;<a href="/broadcasts/get_report/'+broadcast_id+'" class="btn-dashboard btn-request btn-disabled" title="Request for a Report"><span class="fa fa-bar-chart"></span></a>');
    }

    table.fixedColumns().update();

});

/** HIDE PAGINATION ITEMS BASED ON ROW COUNT **/
$(".dataTables_paginate").hide();
var total = table.rows().count();
var countDD = 10; //$("select[name='dashboard-table_length']").val();

if (countDD < total) {
    $(".dataTables_paginate").show();
}

/*trigger hide pagination on select rows*/
/*$("select[name='dashboard-table_length']").change(function(){

    countDD = $(this).val();
    total = table.rows().count();

    hidePagination(countDD, total);
});*/

/** SEARCH BY FILTER **/
$(document.body).on('change', '.filter, #date', function(){

    var filter = $(".filter").val();

    $("#date").val("");
    $("#dashboard-table_filter").find("input").val("");

    if(filter == 5 || filter == 6) {
      $("#date-container").show();
      $("#dashboard-table_filter").hide();
    }
    else if(filter == "all" || filter == 4 || filter == 8){
      $("#date-container").hide();
      $("#dashboard-table_filter").show();
    }

    table.search( '' ).columns().search( '' ).draw();

    total = table.rows().count();

    hidePagination(countDD, total);
});

$('.datepicker').datepicker({ 
    dateFormat: 'mm-dd-yy',
    onSelect: function(date) {
        
        var filter = $(".filter").val();

        table.search( '' ).columns().search( '' ).draw();
        
        if(filter == 5) {

          table.columns( 2 ).search( date ).draw();
        }

        if( filter == 6){

            table.columns( 3 ).search( date ).draw();
        }

        total = table.rows().count();

        hidePagination(countDD, total);

    },
});

$(".dataTables_wrapper").on('click', '.btn-failed', function(){

    var data_failed = $(this).data("failed");
    var broadcast_id = $(this).data("broadcast-id");
    var sender_id = $(this).data("sender-id");
    var message = $(this).data("message");
    var schedule = $(this).data("schedule");
    var campaign_name = $(this).data("campaign-name");
    var campaign_id = $(this).data("campaign-id");
    var data_failed_desc = $(this).data("failed-desc");

    var numbers = data_failed.split(",");

    var cfailed_desc = data_failed_desc.substring(0, data_failed_desc.length - 1);

    var failed_desc = cfailed_desc.split(", ");

    //$(".list-failed").find("li").remove();

    //$(".sub-list").parent().remove();

    var new_failed_desc = Array();
    var arr = ['x'];

    failed_lists = "";
    var tTable = '<table cellspacing="0" width="100%" class="table table-condensed table-failed" border="0">';
    var newTr = "";
    for(var i = 0; i < numbers.length; i++){

        if(i % 4 == 0){
            newTr += (i > 0) ? "</tr><tr>" : "<tr>";
        }

        details=numbers[i].trim().split('#');
        if(details[1] != undefined){

            newTr += '<td description="'+details[1]+'">' + details[0] + '</td>';

            //$(".list-failed").append('<li description="'+details[1]+'" class="list-group-item">'+details[0]+'</li>');

            failed_lists = failed_lists + details[0] + ",";
        }  
    }

    newTr+="</tr>";
    tTable += newTr;
    $("#modalFailed .list-numbers").append(tTable);

    var cfailed_lists = failed_lists.substring(0, failed_lists.length - 2);

    $("#select_resend").find("option").remove();
    for(var i = 0; i < failed_desc.length; i++){

        new_failed_desc.push(failed_desc[i].trim());
    }

    var filteredArray = new_failed_desc.filter(function(item, pos){
      return new_failed_desc.indexOf(item)== pos; 
    });
    
    $("#select_resend").append('<option value="all">All</option>');
    for(var i = 0; i < filteredArray.length; i++){

        $("#select_resend").append('<option value="'+filteredArray[i]+'">'+filteredArray[i]+'</option>');
    }

    //var $bigList = $('#modalFailed .list-failed'), group;

    //while((group = $bigList.find('li:lt(2)').remove()).length){
        //ul = $('<ul/>').addClass('sub-list list-group').append(group);
        //$("<div/>").addClass("col-md-4 col-sm-4 nopaddingLR box-list").append(ul).appendTo("#modalFailed .list-numbers");
        //$('<ul/>').addClass('sub-list list-group').append(group).appendTo('.right');
    ///}

    //$(".list-failed").hide();

    $("#broadcast_id").val(broadcast_id);
    $("#contact_list").val(cfailed_lists);
    $("#message").val(message);
    $("#sender_id").val(sender_id);
    $("#schedule").val(schedule);
    $("#campaign_name").val(campaign_name);
    $("#campaign_id").val(campaign_id);
    $("#modalFailed").modal("show");

});

$("#modalFailed").on('change', '#select_resend', function(){
    var campaign_id = $("#campaign_id").val();
    var failed = $("#campaign-"+campaign_id).attr("data-failed").split(',');
    
    var li_text = $(this).val();
    var new_arr = Array();
    $.each(failed, function(index,value){
        var value_arr = value.split('#');
        var desc = value_arr[1];

        if(li_text == desc){
            new_arr.push(value.trim());
        }
        else if(li_text == "all"){
            new_arr.push(value.trim());
        }
    });

    //$(".list-failed").find("li").remove();
    //$(".sub-list").parent().remove();

    $(".table-failed tbody").find("tr").remove();

    var newTr = "";

    new_lists = "";
    for(var i = 0; i < new_arr.length; i++){

        if(i % 4 == 0){
            newTr += (i > 0) ? "</tr><tr>" : "<tr>";
        }

        details=new_arr[i].trim().split('#');

        if(details[1] != undefined){

            newTr += '<td description="'+details[1]+'">' + details[0] + '</td>';
            //$(".list-failed").append('<li description="'+details[1]+'">'+details[0]+'</li>');

            new_lists = new_lists + details[0] + ",";
        }
    }

    newTr+="</tr>";

    $("#modalFailed .table-failed").find("tbody").append(newTr);

    var cfailed = new_lists.substring(0, new_lists.length - 1);

    $("#contact_list").val(cfailed);

    //var $bigList = $('#modalFailed .list-failed'), group;

    //while((group = $bigList.find('li:lt(5)').remove()).length){
        //ul = $('<ul/>').addClass('sub-list list-group').append(group);
        //$("<div/>").addClass("col-md-3 col-sm-3").append(ul).appendTo("#modalFailed .panel-body");
        //$('<ul/>').addClass('sub-list list-group').append(group).appendTo('.right');
    //}

    //$(".list-failed").hide();
});

$('#dashboard-table_filter input').unbind().on('keyup', function(){

    var searchTerm = this.value;

    var filter = $(".filter").val();

    if(filter == 4) {

      table.columns( 1 ).search( searchTerm ).draw();
    }

    if(filter == 8) {

      table.columns( 8 ).search( date ).draw();
    }

    if(filter == "all"){

        table.search( searchTerm ).draw();
    }

    total = table.rows().count();

    hidePagination(countDD, total);

});

$(".btn-resend-failed").click(function(){

    $.LoadingOverlay("show", {
        color: "rgba(255, 255, 255, 0.42)"
    });

    var data = {
        contact_list: $("#contact_list").val(),
        campaign_name: $("#campaign_name").val(),
        campaign_id: parseInt($("#campaign_id").val()),
        message: $("#message").val(),
        broadcast_id: parseInt($("#broadcast_id").val()),
        sender_id: parseInt($("#sender_id").val()),
        schedule: $("#schedule").val(),
        select_resend: $("#select_resend").val()
    }

    $.ajax({
        method: "POST",
        url: "/broadcasts/do_resend/",
        data: JSON.stringify(data),
        contentType: "application/json",
        headers: {'X-CsrfToken': csrftoken}
    }).done(function(msg) {
        // Debug.
        var ref_id = msg.ref_id;
        $(".btn-save-campaign-draft").hide();
        $("#broadcast-summary").modal("hide");
        $("#reference_number").val(ref_id);
        $("#result-campaign-name").html($("#campaign_name").val());
        $("#broadcast-controller").remove();
        $(".modal-backdrop").remove();
        $("#broadcast-result").show();
    }).fail(function(jqXHR, textStatus){
        
        $.LoadingOverlay("hide");  

        try{

            var error = jqXHR.responseText;
            var data_error = $.parseJSON(error);
            
            var error_text = "";

            //console.log(data_error.code);
            /*if(data_error.code == '003'){

                error_text = "We noticed you have invalid data submitted";
            }

            if(data_error.code == '010'){

                error_text = data_error.error;
            }*/

            error_text = data_error.error;

            $(".error-broadcast").html(error_text);
        }
        catch(error){

            $(".error-broadcast").html("Campaign broadcast failed. Please contact Rising Tide");
        }

        $(".error-broadcast").show(0).delay(5000).hide(0);
        
    });
});

function hidePagination(countDD, total){

    if (countDD < total) {
        $(".dataTables_paginate").show();
    }
    else{
        $(".dataTables_paginate").hide();
    }
}

/** KILL DEBUGGER
$(document).on("contextmenu",function(e){        
   e.preventDefault();
});

$(document).keydown(function(event){
    if(event.keyCode==123) {
        return false;
    } else if(event.ctrlKey && event.shiftKey && event.keyCode==73) {        
      return false;  //Prevent from ctrl+shift+i
    }
});
**/