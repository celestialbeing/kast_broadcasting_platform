$(function(){

        var incomingData = {
            labels: [1, 2, 3, 4, 5, 6, 7],
            datasets: [

                {
                    label: "Dt 1",
                    backgroundColor: "rgba(98,203,49,0.5)",
                    data: [33, 48, 40, 19, 54, 27, 54],
                    lineTension: 0,
                    pointBorderWidth: 1,
                    pointBackgroundColor: "rgba(98,203,49,1)",
                    pointRadius: 3,
                    pointBorderColor: '#ffffff',
                    borderWidth: 1
                }
            ]
        };

        var outgoingData = {
            labels: [1, 2, 3, 4, 5, 6, 7],
            datasets: [

                {
                    label: "Dt 1",
                    backgroundColor: "rgba(98,203,49,0.5)",
                    data: [20, 48, 60, 19, 34, 27, 55],
                    lineTension: 0,
                    pointBorderWidth: 1,
                    pointBackgroundColor: "rgba(98,203,49,1)",
                    pointRadius: 3,
                    pointBorderColor: '#ffffff',
                    borderWidth: 1
                }
            ]
        };

        var broadcastsData = {
            labels: [1, 2, 3, 4, 5, 6, 7, 8, 9],
            datasets: [

                {
                    label: "Dt 1",
                    backgroundColor: "rgba(72,131,221,0.5)",
                    data: [202, 484, 60, 199, 334, 427, 555, 200, 100],
                    pointBorderWidth: 1,
                    pointBackgroundColor: "rgba(72,131,221,1)",
                    pointRadius: 3,
                    pointBorderColor: '#ffffff',
                    borderWidth: 1
                }
            ]
        };

        var lineOptions = {
            responsive: true,
            legend: {
                display: false
            },
            scales: {
                xAxes: [{
                    display: true,
                    scaleLabel: {
                        display: true,
                        labelString: 'October 2017',
                        fontSize:14
                    },
                    ticks: {
                        fontSize: 12
                    }
                }],
                yAxes: [{
                    display: true,
                    scaleLabel: {
                        display: false,
                        labelString: 'Value'
                    },
                    ticks: {
                        fontSize: 12
                    }
                }]
            },
        };

        var senderidsData = {
            labels: [
                "SENDERID",
                "SENDERID2",
                "SENDERID3"
            ],
            datasets: [
                {
                    data: [200, 50, 130],
                    backgroundColor: [
                        "#62cb31",
                        "#77aae9",
                        "#a3e186"
                    ],
                    hoverBackgroundColor: [
                        "#57b32c",
                        "#4984de",
                        "#57b32c"
                    ]
                }]
        }


        var doughnutOptions = {
            responsive: true,
            legend: {
                display: true,
                position: 'bottom'
            },
            animation: {
                animateScale: true,
                animateRotate: true
            },
            maintainAspectRatio : false
        };

        var ctx = document.getElementById("sender_ids_breakdown").getContext("2d");
        new Chart(ctx, {type: 'doughnut', data: senderidsData, options:doughnutOptions});


        var incoming = document.getElementById("incoming").getContext("2d");
        var outgoing = document.getElementById("outgoing").getContext("2d");
        var broadcasts = document.getElementById("broadcasts").getContext("2d");
        new Chart(incoming, {type: 'line', data: incomingData, options:lineOptions});
        new Chart(outgoing, {type: 'line', data: outgoingData, options:lineOptions});
        new Chart(broadcasts, {type: 'line', data: broadcastsData, options:lineOptions});
});