//var unicodeWord = XRegExp('\\p{InBasic_Latin}+$');
var unicodeTest = /^([\x00-\x7F]|([\xC2-\xDF]|\xE0[\xA0-\xBF]|\xED[\x80-\x9F]|(|[\xE1-\xEC]|[\xEE-\xEF]|\xF0[\x90-\xBF]|\xF4[\x80-\x8F]|[\xF1-\xF3][\x80-\xBF])[\x80-\xBF])[\x80-\xBF])*$/;
var mobile_check = /^\+?(09|639)\d{9}$/;
var alpha_check = /^(?![0-9]*$)[a-zA-Z0-9\-\s\'\.\_]+$/;
var html_tags_check = /^<(.|\n)*?>/g;
var hours, minutes, seconds, interval_hours;

function stringSort(string_a, string_b){
    if(string_a['name'] < string_b['name']){
        return -1;
    }
    if(string_a['name'] > string_b['name']){
        return 1;
    }
    return 0;
}

function getTime(){

    var currentdate = new Date();
    hours = currentdate.getHours();
    minutes = currentdate.getMinutes();
    seconds = currentdate.getSeconds();
    var ampm = hours >= 12 ? 'pm' : 'am';
    hours = (hours % 12);
    hours = hours;
    hours = (hours) ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;

    var time = hours+":"+minutes+":"+seconds;
    var time2 = addMinutes(time, 15);

    var strtime = time2 + " " + ampm;
    interval_hours = convertTime(strtime);
    
    var strTime = time2 + ampm;

    return strTime;

}

function convertTime(time) {

    var hours = Number(time.match(/^(\d+)/)[1]);
    var minutes = Number(time.match(/:(\d+)/)[1]);
    var AMPM = time.match(/\s(.*)$/)[1];

    if (AMPM == "pm" && hours < 12) hours = hours + 12;
    if (AMPM == "am" && hours == 12) hours = hours - 12;
    var sHours = hours.toString();
    var sMinutes = minutes.toString();
    if (hours < 10) sHours = "0" + sHours;
    if (minutes < 10) sMinutes = "0" + sMinutes;
    var stime = sHours + ":" + sMinutes;

    return stime;
}

function addMinutes(time, minsToAdd) {
  function D(J){ return (J<10? '0':'') + J};
  
  var piece = time.split(':');
  
  var mins = piece[0]*60 + +piece[1] + +minsToAdd;

  return D(mins%(24*60)/60 | 0) + ':' + D(mins%60);  
}  

$("#menu-toggler").click(function(e) {
    e.preventDefault();
    $(".wrapper").toggleClass("toggled");
});

$(".btn-tooltip").click(function(){

    var title = $(this).attr('title');
    var orig_title = $(this).html();
    $(this).attr('title', orig_title);
    $(this).html(title);
});

function disableWizardNext(){

    $('.actions > ul > li:nth-child(2) > a').attr('class', 'btn btn-default');
    $('.actions > ul > li:nth-child(2) > a').attr('disabled', 'disabled');
    $('.actions > ul > li:nth-child(2) > a').attr('href', '#');
    $('.actions > ul > li:nth-child(2)').addClass('disabled');
}

function enableWizardNext(){

    //enable wizard next button
    $('.actions > ul > li:nth-child(2) > a').removeAttr('class');
    $('.actions > ul > li:nth-child(2) > a').removeAttr('disabled');
    $('.actions > ul > li:nth-child(2) > a').attr('href', '#next'); 
    $('.actions > ul > li:nth-child(2)').removeClass('disabled');
}

function enableFinishWizard(){

    $('.actions > ul > li:nth-child(3)').css('display', 'block');
    $('.actions > ul > li:nth-child(3) > a').attr('class', 'btn btn-default');
    $('.actions > ul > li:nth-child(3) > a').removeAttr('disabled');
    $('.actions > ul > li:nth-child(3) > a').attr('href', '#finish');
    $('.actions > ul > li:nth-child(3)').removeClass('disabled');
}

function disableFinishWizard(){

    $('.actions > ul > li:nth-child(3)').css('display', 'block');
    $('.actions > ul > li:nth-child(3) > a').attr('class', 'btn btn-default');
    $('.actions > ul > li:nth-child(3) > a').attr('disabled', 'disabled');
    $('.actions > ul > li:nth-child(3) > a').attr('href', '#');
    $('.actions > ul > li:nth-child(3)').addClass('disabled');
}

// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function setTooltip(btn, message) {
  $(btn).tooltip('hide')
    .attr('data-original-title', message)
    .tooltip('show');
}

function hideTooltip(btn) {
  setTimeout(function() {
    $(btn).tooltip('hide');
  }, 1000);
}
