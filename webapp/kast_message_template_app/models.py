from django.db import models

from kast_account_app.models import KastTenants

class KastMessageTemplates(models.Model):

    tenant   = models.ForeignKey(KastTenants, on_delete=models.CASCADE)
    name     = models.CharField(null=True, max_length=50)
    message  = models.TextField(null=True)
    created  = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = "kast_message_templates"
        unique_together = ('name', 'tenant')

    def __unicode__(self):
        return self.name or u''