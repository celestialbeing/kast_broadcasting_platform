"""

This module is responsible for rendering message template application pages

Models used:
   KastTenants
   KastMessageTemplates

"""

from django.shortcuts import render
from django.shortcuts import redirect
from django.utils import timezone
from django.db import IntegrityError
from django.core import serializers
from django.http import HttpResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from kast_account_app.models import KastTenants

from .models import KastMessageTemplates

import json

@login_required
def message_template(request):

   """

   Renders the message template or displays errors
   Process message template page depending on the chosen 
      action(edit, delete, update)

   :type request: obj 
   :param request: Request object

   :rtype: obj (HttpResponse)
   :return: Rendered message template page

   """

   response_dict = {}

   #get user tenant
   user = User.objects.get(email = request.user.email)

   if(user.kastusers.status == 'trial'):
      return redirect('/dashboard')

   tenant = user.kastusers.tenant
   #get tenant id
   tenant_obj = KastTenants.objects.get(name = tenant)
   tenant_id = tenant_obj.id

   if (request.method == "POST"):
      #get message template id
      tmp_id = request.POST.get('id')

      #get and return message template for edit form
      if(request.POST.get('_method') == "edit"):
         search_template = KastMessageTemplates.objects.get(pk = tmp_id)
         response_dict['data'] = {'data': {'name': search_template.name, 'message': search_template.message, 'pk': search_template.pk}}
      else:
         template_name = request.POST.get('template_name')
         message = request.POST.get('broadcast_message')
         
         if(request.POST.get('_method') == "delete"):
            KastMessageTemplates.objects.filter(pk = tmp_id).delete()
            response_dict['status'] = True
         else:
            try:
               template_name = template_name.upper().strip()
               message = message.strip()
               if(request.POST.get('_method') == "update"):
                  template_obj = KastMessageTemplates.objects.filter(pk = tmp_id).update(name=template_name, message = message)
                  response_dict['status'] = True
               else:
                  template_obj = KastMessageTemplates(name = template_name, message = message, tenant_id = tenant_id)
                  template_obj.save()
                  response_dict['status'] = True
            except IntegrityError as e:
               response_dict['status'] = False
               response_dict['message'] = 'Template name is already exist'

      return HttpResponse(json.dumps(response_dict))
   else:
      templates = KastMessageTemplates.objects.filter(tenant_id = tenant_id).order_by('-created')
      return render(request, 'kast_message_template_app_templates/home.html', {'templates': templates})
