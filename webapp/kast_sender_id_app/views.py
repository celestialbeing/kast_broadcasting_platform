"""

This module is responsible for rendering Kast sender ID application pages

Models used:
    KastTenants
    KastUsers
    KastSIDs

"""

import logging
import requests
from hashids import Hashids

import arrow

from django.shortcuts import render
from django.shortcuts import redirect
from django.views.decorators.csrf import csrf_exempt
from django.db import transaction
from django.db.models import Q

from django.utils import timezone
from django.db import IntegrityError
from django.core import serializers
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.mail import send_mail, EmailMessage
from kast_account_app.models import KastTenants, KastUsers

from django.template.loader import render_to_string

from .models import KastSIDs

import json

@login_required
def sender_id(request):

    """

    Handles Kast sender ID page rendering
    Checks approved sender IDs
    Hits broadcast API for sender ID checking
    Sends an email to both Kast admin and users about sender ID status
    
    .. note::
        Code can be disected into function like on broadcast app

        Hardcoded stuffs
            IP to check sender ID status
            Email subjects and messages
            Kast admin email

    :type request: obj
    :param request: Request object

    :rtype: obj (HttpResponse)
    :return: Response object containing pending, rejected and approved sender IDs

    """

    logger = logging.getLogger('django.server')
    #get user tenant
    user = User.objects.get(email = request.user.email)
    if(user.kastusers.status == 'trial'):
            return redirect('/dashboard')
    tenant = user.kastusers.tenant
    #get tenant id
    tenant_obj = KastTenants.objects.get(name = tenant)
    tenant_id = tenant_obj.id
    response_dict = {}
    if(request.method == "POST"):
        try:
            requested_by = request.user.email
            if(request.POST.get('_method') == "delete"):
                pk_id = request.POST.get('id')

                try:
                    sid = KastSIDs.objects.get(
                        ~Q(status=KastSIDs.DELETED),
                        pk=pk_id,
                        tenant=tenant_id)
                except KastSIDs.DoesNotExist:
                    response_dict['status'] = False
                    response_dict['data'] = {
                        'error': 'No sender id: %s found.' % pk_id}
                    return JsonResponse(response_dict)

                with transaction.atomic():
                    # Mark sender id as deleted.
                    KastSIDs.objects\
                        .filter(pk=pk_id, tenant=tenant_id)\
                        .update(
                            status=KastSIDs.DELETED,
                            requested_by=requested_by)

                    # Notify tenants sender id is deleted.
                    subject = 'KAST Sender ID Deleted'
                    message = 'Your sender id: "%s" has been deleted by %s.'\
                        % (sid.name, requested_by)

                    html_message = render_to_string(
                    'kast_sender_id_templates/emails/delete_sender_id.html',
                    {'sender_id': str(sid.name),'requested_by': str(requested_by)})

                    from_ = 'KAST'
                    tenant_emails = KastUsers.objects\
                        .filter(tenant=tenant)\
                        .values_list('user__email', flat=True)
                    send_mail(subject, message, from_,
                        tenant_emails, fail_silently=False, html_message=html_message)

                response_dict['status'] = True;
            else:
                sender_id = request.POST.get('sender_id')
                #check sender id is existed and approved
                do_request = requests.get('http://172.30.0.44/senderid/'+sender_id)
                logger.info('Call to broadcast api returned: %s' % do_request.text)
                res = do_request.json()

                if('code' in res):
                    if(res['code'] == "003"): #sender id not yet existed
                        do_request  = requests.post('http://172.30.0.44/senderid', json={"requested_by": requested_by,"name": sender_id})
                        logger.info('Call to broadcast api returned: %s' % do_request.text)

                # MySQL default collation is case-insensitive in matching
                # columns. Get all matching rows and filter each name if
                # same with requested sender id.
                # Ref: https://docs.djangoproject.com/en/1.8/ref/databases/#mysql-collation
                search_id = KastSIDs.objects.filter(
                    ~Q(status=KastSIDs.DELETED),
                    name=sender_id,
                    tenant_id=tenant_id)
                if filter(lambda x: x.name == sender_id, search_id):
                    response_dict['status'] = False
                    response_dict['data'] = {'error': 'Sender ID: '+sender_id+' is already existed.'}
                else:
                    senderid = KastSIDs.objects.get_sender_id(
                        sender_id, tenant_id)
                    if senderid:
                        # Update sender id.
                        senderid.status = 0
                        senderid.requested_by = requested_by
                        senderid.save()
                    else:
                        # Create new sender id.
                        senderid = KastSIDs.objects.create(
                            name=sender_id,
                            requested_by=requested_by,
                            status=0,
                            tenant_id=tenant_id)

                    response_dict['status'] = True;

                    #get sender id uid and date created
                    uid = senderid.uid
                    date_created = senderid.created

                    # Send acknowledgement email.
                    # Email sending fails silently.
                    subject = 'Receipt of Sender ID Application'
                    message = ('You have successfully requested for '
                        'Sender ID: %s') % sender_id

                    html_message = render_to_string(
                    'kast_sender_id_templates/emails/apply_sender_id.html',
                    {'sender_id': str(sender_id)})

                    from_ = 'KAST'
                    recipient_list = [
                        #request.user.email, 'teamkast@risingtide.ph']
                        request.user.email]
                    send_mail(subject, message, from_, recipient_list, html_message=html_message)

                    #send email to admin
                    subject2 = 'Sender ID Request'
                    message2 = ''

                    html_message = render_to_string(
                    'kast_sender_id_templates/emails/apply_sender_id_admin.html',
                    {'sender_id': str(sender_id), 'requested_by': str(requested_by),
                    'company': str(tenant), 'date_created': arrow.utcnow().to('Asia/Manila').format('YYYY-MM-DD HH:mm:ss'),
                    'approve_uri': request.build_absolute_uri()+'action?method=approve&q='+uid,
                    'reject_uri': request.build_absolute_uri()+'action?method=reject&q='+uid})

                    from_2 = 'KAST'
                    recipient_list2 = ['customercare@kast.ph']
                    bcccccccccccccc = [
                        "products@risingtide.ph", "operations@risingtide.ph",
                        "sales@kast.ph", "echosales@risingtide.ph"]

                    mail=EmailMessage(
                        subject2,
                        html_message,
                        from_2,
                        recipient_list2,
                        bcccccccccccccc
                    )
                    mail.content_subtype = "html"
                    mail.send()
                    #send_mail(subject2, message2, from_2, recipient_list2+bcccccccccccccc, html_message = html_message)

                do_request.raise_for_status()
        except (requests.exceptions.HTTPError, KeyError, ValueError) as e:
            logger.exception(e)
            response_dict['status'] = False
        return HttpResponse(json.dumps(response_dict))
    else:
        pending_sender_ids = KastSIDs.objects.filter(tenant_id = tenant_id, status = 0).order_by('name')
        approved_sender_ids = KastSIDs.objects.filter(tenant_id = tenant_id, status = 1).order_by('name')
        rejected_sender_ids = KastSIDs.objects.filter(tenant_id = tenant_id, status = 2).order_by('name')
        return render(request, 'kast_sender_id_templates/home.html',  {'pending_sender_ids': pending_sender_ids,
                                                                    'approved_sender_ids': approved_sender_ids,
                                                                    'rejected_sender_ids': rejected_sender_ids,
                                                                    'user_email': request.user.email})

@csrf_exempt
def sender_action(request):

    """

    Handles Kast sender ID action page rendering
    Hits broadcast API for sender ID checking
    Sends an email to users about sender ID status
    
    .. note::
        Again, this function can be disected into function like on broadcast app

        Hardcoded stuffs
            IP to check sender ID status
            Email subjects and messages
            Kast admin email

    :type request: obj
    :param request: Request object

    :rtype: obj (HttpResponse)
    :return: Http response object

    """

    # Check if get has a method to approve or reject parameter
    if (request.method == 'GET' and 'method' in request.GET):
        #check if get has q parameter included
        if(request.method == "GET" and 'q' in request.GET):
            link_uid  = request.GET.get('q')
            try:
                sids = KastSIDs.objects.get(uid = link_uid, status=KastSIDs.PENDING)
                sender_id = sids.name
                user_email = sids.requested_by
            except:
                sids = None

            if(sids is not None):

                logger = logging.getLogger('django.server')

                #check if sender id is already approved in sendoh db
                try:
                    do_request = requests.get('http://172.30.0.44/senderid/'+sender_id)
                    logger.info('Call to broadcast api returned: %s' % do_request.text)
                    res = do_request.json()

                    if('status' in res):
                        if(res['status'] == 0):
                            # pending status 0
                            return HttpResponse('Sender ID '+sender_id+' is not yet approved in EGG RTS.')
                        elif(res['status'] == 1):
                            # approved status 1
                            method = request.GET.get('method')
                            if(method == 'approve'):
                                status = 1
                                disp_stat = ' approved'
                                subject = 'Your Sender ID has been Approved!'
                                html_message = render_to_string(
                                    'kast_sender_id_templates/emails/apply_sender_id_approved.html',
                                    {'sender_id': str(sender_id)})
                            else:
                                status = 2
                                disp_stat = ' rejected'
                                subject = 'Your Sender ID Request has been Declined'
                                html_message = render_to_string(
                                    'kast_sender_id_templates/emails/apply_sender_id_rejected.html',
                                    {'sender_id': str(sender_id)})

                            KastSIDs.objects.filter(uid = link_uid).update(status = status)

                            # Send acknowledgement email.
                            # Email sending fails silently.
                            message = 'Your application for Sender ID: '+sender_id+' has been '+disp_stat+'.'

                            from_ = 'KAST'
                            recipient_list = [user_email.strip('kast-')]
                            send_mail(subject, message, from_, recipient_list, html_message=html_message)

                            return HttpResponse('Sender ID '+ sender_id+disp_stat+".")
                        else:
                            # rejected status 2
                            return HttpResponse('Sender ID '+sender_id+' has been rejected in EGG RTS.')
                    else:
                        return HttpResponse(do_request.text)

                    do_request.raise_for_status()

                except (requests.exceptions.HTTPError) as e:
                    return HttpResponse(e)
            else:
                return HttpResponse('method is already done')
        else:
            return HttpResponse('param not found')
    else:
        return HttpResponse('method not found')
