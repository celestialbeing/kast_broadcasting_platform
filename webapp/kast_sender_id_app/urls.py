from django.conf.urls import url
from . import views

urlpatterns = [
    # Route to sender id template landing page
    url(r'^sender-id/$', views.sender_id, name='sender-id'),

    # Route to approve/reject sender id
   	url(r'^sender-id/action/', views.sender_action, name='sender-action'),
]
