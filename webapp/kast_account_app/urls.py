from django.conf.urls import url
from . import views

urlpatterns = [

    # Route to kast registration pages
    url(r'^register$', views.register, name='register'),
    url(r'^register/trial/$', views.register_trial, name='register-trial'),
    url(r'^processing/$', views.processing, name='processing'),
    # Route to forget password page
    url(r'^identify/', views.forgot_password, name='identify'),
    # Route to user profile page (both trial and live user accounts)
    url(r'^profile/$', views.user_profile, name='profile'),
    # Route to session destroy (logout)
    url(r'^logout/$', 'django.contrib.auth.views.logout', name='logout', kwargs={'next_page': '/home'}),
]
