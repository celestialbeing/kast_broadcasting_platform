"""

Views of the Kast account application. Handles mostly accounts page rendering and redirection.

Models:
    User
    KastUsers
    KastTenants

"""

from django.shortcuts import render
from django.shortcuts import redirect

from django.http import HttpResponse

from django.contrib.auth import login
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.contrib.auth.hashers import check_password
from django.contrib.auth.hashers import make_password
from django.contrib.sites.shortcuts import get_current_site

from django.template.loader import render_to_string

from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.csrf import csrf_protect

from django.core.mail import send_mail, EmailMessage

from email_normalize import normalize

from .models import KastTenants
from .models import KastUsers

import json
import uuid


# Go Live Registration
@csrf_exempt
def register(request):

    """
    
    Process user registration from trial to live account.
    Sends an email to Kast administrator and user about the go live request.

    :type request: obj
    :param request: Return object

    :rtype: obj (HttpResponse)
    :return: Rendered template for redirection or HttpResponse status

    .. warning::
        Email subject is hardcoded
        Kast admin email is hardcoded
        Email message for user is hardcoded

    """

    if (request.method == "POST"):

        tenant   = request.POST.get('company')
        user     = request.user
        name     = user.kastusers.name
        mobile   = user.kastusers.contact
        email    = user.email

        # Check if tenant was already present on the system to avoid duplicates
        try:
            check_tenant = KastTenants.objects.get(name=tenant)
        except:
            check_tenant = None

        if (check_tenant is None):
            tenant_obj = KastTenants(name=tenant)
            tenant_obj.save()

        # Tie up the user to the tenant
        # Go live process will never be automated
        user.kastusers.tenant  = tenant
        user.save()

        # Send an email to kast_admin about the go live request
        message = ("A new go live request was received. \n"
                   "NAME: " + str(name) + "\n"
                   "EMAIL: " + str(email) + "\n"
                   "TENANT: " + str(tenant))

        html_message = render_to_string(
        'kast_account_app_templates/emails/go_live_admin_email.html',
        {'name': str(name),
        'email': str(email),
        'tenant': str(tenant)})
        
        bcc = ["products@risingtide.ph", "operations@risingtide.ph", "sales@kast.ph", "echosales@risingtide.ph"]

        bcc = ["products@risingtide.ph", "operations@risingtide.ph", "sales@kast.ph", "echosales@risingtide.ph"]

        # send_mail(
        #     'KAST User Go Live Request',
        #     message,
        #     'customercare@kast.ph',
        #     ['customercare@kast.ph']+bcc,
        #     fail_silently=False,
        #     html_message=html_message,
        # )

        mail = EmailMessage(
            'KAST User Go Live Request',
            html_message,
            'customercare@kast.ph',
            ['customercare@kast.ph'],
            bcc
        )
        mail.content_subtype = "html"
        mail.send()

        # Send an email to the user about the go live request
        message = ("Thank you for choosing KAST. Your live account registration is being processed."
                   "This usually takes 3-7 days. Thank you for your patience.")
        html_message = render_to_string(
        'kast_account_app_templates/emails/go_live.html',
        {'message': message})

        send_mail(
            'KAST User Go Live Request',
            message,
            'customercare@kast.ph',
            [email],
            fail_silently=False,
            html_message=html_message,
        )

        # User is still logged in on this part. Exit to dashboard
        response_dict = {}
        response_dict['status'] = "ok"
        return HttpResponse(json.dumps(response_dict))

    else:
        if (request.user.is_authenticated() and request.session['login_type'] == 'live'):
            return redirect('/dashboard')
        elif(not request.user.is_authenticated()):
            return redirect('/home')
        else:
            # Identify the current logged in user then identify placeholders
            user       = request.user
            p_name     = user.kastusers.name
            p_mobile   = user.kastusers.contact
            p_email    = user.email
            return render(request, 'kast_account_app_templates/registration.html',
                         {'p_name': p_name, 'p_mobile': p_mobile, 'p_email': p_email })

# Trial Registration
@csrf_exempt
def register_trial(request):

    """
    
    Process user registration to create/update a trial acount.
    Checks emails for duplicates.
    Validates verification code input.
    Sends an email to the user with a code confirming their ownership.

    :type request: obj
    :param request: Return object

    :rtype: obj (HttpResponse)
    :return: Rendered template for redirection or HttpResponse status

    .. warning::
        Email subject is hardcoded
        Kast admin email is hardcoded
        Email message is hardcoded, verification code interpollated

    """

    if (request.method == "POST"):
        request_type = request.POST.get('request_type')

        if (request_type == "registration"):

            # Trial registration form handler
            name     = request.POST.get('name')
            mobile   = request.POST.get('mobile')
            email    = request.POST.get('email')
            password = request.POST.get('password')
            akey     = None

            # Verify if email was used already
            try:
                user = User.objects.get(email=email)
            except:
                user = None

            if (user is not None):
                if (user.is_active == True):
                    # email was used already
                    response_dict = {}
                    response_dict['status']  = "error"
                    response_dict['message'] = "The email address was used already"
                    return HttpResponse(json.dumps(response_dict))
                else:
                    # user might have accidentally closed the browser before activation code input
                    akey = user.kastusers.activation_key
                    user.password = make_password(password)
                    user.kastusers.contact = mobile
                    user.kastusers.name = name
                    user.save(force_update=True)

                    # Send activation key to email
                    message = ("Welcome to the KAST Community! <br><br>"
                               "To start using KAST for your SMS promos, broadcast, community building, and more, please verify"
                               "this email address by inputting the code in the email verification page <br><br>"
                               "CODE: " + str(akey) + "<br><br>"
                               "Thank you for your interest in using KAST! Should you have any questions, feel free to visit our FAQs page"
                               "or email our customer support (switch@risingtide.ph) <br><br>"
                               "Thank you! <br><br>"
                               "Kast Support Team"
                              )
                    html_message = render_to_string(
                    'kast_account_app_templates/emails/register_trial.html',
                    {'code_key': str(akey)})
                    send_mail(
                        'KAST Verify Email Account', 
                        message, 
                        'customercare@kast.ph', 
                        [email],
                        fail_silently=False,
                        html_message=html_message,
                    )

                    # Formulate response
                    response_dict = {}
                    response_dict['status'] = "ok"
                    return HttpResponse(json.dumps(response_dict))
            else:
                # Normal registration
                akey = str(uuid.uuid4().hex[:6].upper())
                uid  = str(uuid.uuid4())[:20]
                user = User.objects.create_user(uid, email, password)
                user.is_active = False
                user.kastusers.name = name
                user.kastusers.contact = mobile
                user.kastusers.activation_key = akey
                user.save()

                # Send activation key to email
                message = ("Welcome to the KAST Community! <br><br>"
                           "To start using KAST for your SMS promos, broadcast, community building, and more, please verify"
                           "this email address by inputting the code in the email verification page <br><br>"
                           "CODE: " + str(akey) + "<br><br>"
                           "Thank you for your interest in using KAST! Should you have any questions, feel free to visit our FAQs page"
                           "or email our customer support (switch@risingtide.ph) <br><br>"
                           "Thank you! <br><br>"
                           "Kast Support Team"
                          )

                html_message = render_to_string(
                    'kast_account_app_templates/emails/register_trial.html',
                    {'code_key': str(akey)})

                send_mail(
                    'KAST Verify Email Account', 
                    message, 
                    'customercare@kast.ph', 
                    [email],
                    fail_silently=False,
                    html_message=html_message,
                )

                # Formulate response
                response_dict = {}
                response_dict['status'] = "ok"
                return HttpResponse(json.dumps(response_dict))

        elif(request_type == "registration_check_email"):

            # Handle google email bug
            email = normalize(request.POST.get('email'))

            # Actual email checking logic
            try:
                user = User.objects.get(email=email)
                if (user.is_active == True):
                    response_dict = {}
                    response_dict['status']  = "error"
                    response_dict['message'] = "The email address was used already"
                    return HttpResponse(json.dumps(response_dict))
                    #return HttpResponse(False)
                else:
                    response_dict = {}
                    response_dict['status'] = "ok"
                    return HttpResponse(json.dumps(response_dict))
                    #return HttpResponse(True)
            except:
                response_dict = {}
                response_dict['status'] = "ok"
                return HttpResponse(json.dumps(response_dict))
                #return HttpResponse(True)

        elif(request_type == "registration_verif_resend"):

            # Resend verification code (no capping)
            email = request.POST.get('email')
            user  = User.objects.get(email=email)
            akey  = user.kastusers.activation_key
            name  = user.kastusers.name

            message = ("We have received a request to resend the verification code. Please verify your email address by "
                       "inputting the code in the email verification page <br><br>"
                       "CODE: " + str(akey) + "<br><br>"
                       "Thank you for your interest in using KAST! Should you have any questions, feel free to visit our FAQs page"
                       "or email our customer support (switch@risingtide.ph) <br><br>"
                       "Thank you! <br><br>"
                       "Kast Support Team"
                      )
            html_message = render_to_string(
                    'kast_account_app_templates/emails/register_trial_resend_verification.html',
                    {'code_key': str(akey)})
            send_mail(
                'KAST Resend Verification Code',
                message, 
                'customercare@kast.ph', 
                [email],
                fail_silently=False,
                html_message=html_message,
            )

            # Formulate response
            response_dict = {}
            response_dict['status'] = "ok"
            return HttpResponse(json.dumps(response_dict))

        elif(request_type == "registration_validation"):

            # Trial registration verif form handler
            code  = request.POST.get('code')
            try:
                kast_user = KastUsers.objects.get(activation_key=code)
            except:
                kast_user = None
            if (kast_user == None):
                response_dict = {}
                response_dict['status']  = "error"
                response_dict['message'] = "You have put an invalid code"
                return HttpResponse(json.dumps(response_dict))
            else:
                # Activation key is valid
                email = kast_user.user.email
                user  = User.objects.get(email=email, is_active=False)
                user.is_active = True
                user.save()

                # Auto login user
                auth = authenticate(username=email)
                login(request, auth)
                request.session['login_type'] = 'trial'
                response_dict = {}
                response_dict['status'] = "ok"
                return HttpResponse(json.dumps(response_dict))

    else:
        if (request.user.is_authenticated()):
            return redirect('/dashboard')
        else:
            return render(request, 'kast_account_app_templates/registration-trial.html')


# forgot password
@csrf_exempt
def forgot_password(request):

    """

    Process both change and forgot password pages.

    :type request: obj
    :param request: Request object

    :rtype: obj (HttpResponse)
    :return: HttpResponse, page redirection or rendered page of forgot and change password

    """

    if (request.method == "POST"):
        request_type = request.POST.get('request_type')

        if (request_type == "fpemail"):
            email = request.POST.get('email')
            try:
                user  = User.objects.get(email=email)
            except:
                user = None

            if (user is not None):
                # email is a registered kast user
                uid = user.kastusers.uid
                message = "Click this link to reset your password: " + request.build_absolute_uri() + "?q=" + str(uid)

                html_message = render_to_string(
                    'kast_account_app_templates/emails/forgot_password.html',
                    {'confirmation_code': request.build_absolute_uri() + "?q=" + str(uid)})

                send_mail(
                    'KAST Forget Password', 
                    message, 
                    'customercare@kast.ph', 
                    [email],
                    fail_silently=False,
                    html_message=html_message,
                )

                response_dict = {}
                response_dict['status'] = "ok"
                response_dict['url']    = str("http://") + str(get_current_site(request).domain) + str("/home")
                return HttpResponse(json.dumps(response_dict))

        elif (request_type == "fpupdate"):

            password      = request.POST.get('npassword')
            uid           = request.POST.get('uid')
            kast_user     = KastUsers.objects.get(uid=uid)
            email         = kast_user.user.email
            user          = User.objects.get(email=email)
            user.password = make_password(str(password))
            user.save()

            response_dict = {}
            response_dict['status'] = "ok"
            response_dict['url']    = str("http://") + str(get_current_site(request).domain) + str("/home")
            return HttpResponse(json.dumps(response_dict))

    else:
        if (request.user.is_authenticated()):
            return redirect('/dashboard')
        else:
            # For change password link
            if (request.method == 'GET' and 'q' in request.GET):
                link_uid  = request.GET.get('q')
                try:
                    kast_user = KastUsers.objects.get(uid=link_uid)
                except:
                    kast_user = None

                if (kast_user is not None):
                    return render(request, 'kast_account_app_templates/change-password.html', {'p_uid': link_uid})
                else:
                    return redirect('/home')
            else:
                return render(request, 'kast_account_app_templates/forgot-password.html')

# User Profile
@csrf_exempt
def user_profile(request):

    """

    Handles changing of account information, both for trial and live user accounts

    :type request: obj
    :param request: Request object

    :rtype: obj (HttpResponse)
    :return: HttpResponse object or user profile rendered template

    .. note:: Site behavior changes depending on the status of a user account

    """

    if (request.method == 'POST'):

        # Handle information update
        # Only name, mobile and password can be updated
        name      = request.POST.get('name')
        mobile    = request.POST.get('mobile')
        opassword = request.POST.get('opassword')
        npassword = request.POST.get('npassword')

        user = request.user
        user.kastusers.name = name
        user.kastusers.contact = mobile

        # Check if user wants to modify his/her password
        if (opassword == ''):
            user.save()
            response_dict = {}
            response_dict['status'] = "ok"
            return HttpResponse(json.dumps(response_dict))
        else:
            if check_password(opassword, user.password):
                user.password = make_password(npassword)
                user.save()
                response_dict = {}
                response_dict['status'] = "ok"
                return HttpResponse(json.dumps(response_dict))
            else:
                response_dict = {}
                response_dict['status']  = "error"
                response_dict['message'] = "Your password is incorrect"
                return HttpResponse(json.dumps(response_dict))
    else:
        if (request.user.is_authenticated()):
            if (request.session['login_type'] == 'trial'):
                user     = request.user
                p_name   = user.kastusers.name
                p_mobile = user.kastusers.contact
                p_email  = user.email

                if (user.kastusers.tenant):
                    return render(request, 'kast_account_app_templates/user-profile.html',
                                 {'p_name': p_name, 'p_mobile': p_mobile, 'p_email': p_email, 'p_company': None, 'p_status': 'processing' })
                else:
                    return render(request, 'kast_account_app_templates/user-profile.html',
                                 {'p_name': p_name, 'p_mobile': p_mobile, 'p_email': p_email, 'p_company': None, 'p_status': 'trial' })

            elif (request.session['login_type'] == 'live'):
                user      = request.user
                p_name    = user.kastusers.name
                p_mobile  = user.kastusers.contact
                p_email   = user.email
                p_company = user.kastusers.tenant
                return render(request, 'kast_account_app_templates/user-profile.html',
                             {'p_name': p_name, 'p_mobile': p_mobile, 'p_email': p_email, 'p_company': p_company, 'p_status': 'live' })
        else:
            return redirect('/home')

# Processing page
@csrf_exempt
def processing(request):

    """

    Handles page display when user account is being processed from trial to live account

    :type request: obj 
    :param request: Request object

    :rtype: obj (HttpResponse)
    :return: Rendered template or page redirection

    """

    if (request.user.is_authenticated()):
        # Need to check if user status always (even if user is still logged in)
        if (request.user.kastusers.status == 'trial'):
            return render(request, 'kast_account_app_templates/processing.html')
        else:
            request.session['login_type'] = 'live'
            return redirect('/dashboard')
    else:
        return redirect('/home')
