import logging
import requests
from hashids import Hashids

import arrow
import json

from django.shortcuts import render
from django.shortcuts import redirect
from django.views.decorators.csrf import csrf_exempt
from django.db import transaction
from django.db.models import Q

from django.utils import timezone
from django.db import IntegrityError
from django.core import serializers
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from kast_account_app.models import KastTenants, KastUsers

@login_required
def home(request):
    context = {}
    context['title'] = 'Users'
    return render(request, 'kast_user_management_templates/view_users.html', {'context': context})

def form(request):
    context = {}
    context['title'] = 'Users'
    return render(request, 'kast_user_management_templates/users_form.html', {'context': context})